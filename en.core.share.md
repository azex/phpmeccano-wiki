**[>>> Contents <<<](en.index)**

## Module share.php ##

### General description ###

Includes class ```Share``` that is purposed to exchange messages and files like in simple social network.

### Methods of class ```Share``` *(class ```Share``` extends ```Discuss```)* ###

**```__construct(\mysqli $dbLink)```**

**Description**  
Sets a connection to database.

**Parameters**  
*```dbLink```* - object ```mysqli```.

** **

**```string createCircle(int $userId, string $name)```**

***Description***  
Creates new circle of the user contacts.

**Parameters**  
*```userId```* - identifier of the user for whom a circle is created.  
*```name```* - name of the circle.

**Return values**  
In case of success returns identifier (GUID) of the created circle. Otherwise returns ```FALSE```.

** **

**```string / DOMDocument / bool userCircles(int $userId)```**

**Description**  
Returns list of the user circles.

**Parameters**  
*```userId```* - user identifier.

**Return values**  
In case of success returns list of the user circles as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "string",
                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
            },
            "name": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            }
        },
        "required": ["id", "name"]
    }
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="circles">
            <zeroOrMore>
                <element name="circle"> <!-- node of contact circle -->
                    <attribute name="id"> <!-- identifier of contact circle -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </attribute>
                    <attribute name="name"> <!-- name of contact circle -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </attribute>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    array(
        'id' => 'identifier of contact circle',
        'name' => 'name of contact circle'
    ),
    array(...),
    ...
)
```

** **

**```bool renameCircle(int $userId, string $circleId, string $newName)```**

**Description**  
Renames circles of user's contacts.

**Parameters**  
*```userId```* - user identifier.  
*```circleId```* - identifier (GUID) of the circle.  
*```newName```* - new name of the circle.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool addToCircle(int $contactId, string $circleId, int $userId)```**

**Description**  
Adds new user to the circle of contacts.

**Parameters**  
*```contactId```* - user identifier which is added to the circle as new contact.  
*```circleId```* - identifier of the circle to which the contact is added.  
*```userId```* - identifier of the user to a circle of which the contact is added.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```string / DOMDocument / bool circleContacts(int $userId, string $circleId)```**

**Description**  
Returns list of users from circle of contacts.

**Parameters**  
*```userId```* - identifier of the user who owns the circle.  
*```circleId```* - identifier (GUID) of the circle.

**Return values**  
In case of success returns list of users from circle of contacts as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "cid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "cname": {
            "type": "string",
            "minlength": 1,
            "maxlength": 50
        },
        "contacts": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1,
                        "exclusiveMinimum": false
                    },
                    "username": {
                        "type": "string",
                        "pattern": "[a-zA-Z0-9_]{3,20}"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    }
                },
                "required": ["id", "username", "fullname"]
            }
        }
    },
    "required": ["cid", "cname", "contacts"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="contacts">
            <attribute name="cid"> <!-- identifier of the circle -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <attribute name="cname"> <!-- name of the circle -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">50</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="contact"> <!-- node of the contact -->
                    <element name="id"> <!-- identifier of the contact -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="username"> <!-- name of the contact -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- full name of the contact -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'cid' => 'identifier of the circle',
    'cname' => 'name of the circle',
    'contacts' => array(
        array(
            'id' => 'identifier of the contact',
            'username' => 'name of the contact',
            'fullname' => 'full name of the contact'
        ),
        array(...),
        ...
    )
)
```

** **

**```bool rmFromCircle(int $userId, string $circleId, int $contactId)```**

**Description**  
Removes user from the circle of contacts.

**Parameters**  
*```userId```* - identifier of the user from circle of which a contact is removed.  
*```circleId```* - identifier (GUID) of the circle.  
*```contactId```* - identifier of the user which is removed from the circle.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool delCircle(int $userId, string $circleId)```**

**Description**  
Deletes circle of user's contacts.

**Parameters**  
*```userId```* - user identifier.  
*```circleId```* - identifier of the circle.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```string createMsg(int $userId, string $title, string $text)```**

**Description**  
Creates message of the user.

**Parameters**  
*```userId```* - identifier of the user from name of which a message is created.  
*```title```* - title of the message.  
*```text```* - text of the message.

**Return values**  
Identifier (GUID) of created message. Otherwise returns ```FALSE```.

** **

**```string stageFile(string $file, string $filename, int $userid, string $title = '', string $comment = '')```**

**Description**  
Moves uploaded to server file from temporary directory and stages it from name of defined user.

**Parameters**  
*```file```* - path to uploaded file inside of temporary directory.  
*```filename```* - name of the file with which file will be given at downloading.  
*```userId```* - identifier of the user from name of which the file is staged.  
*```title```* - short title of the file.  
*```comment```* - short comment of the file.

**Return values**  
Identifier (GUID) of staged message. Otherwise returns ```FALSE```.

** **

**```bool shareFile(string $fileId, int $userId, array $circles)```**

**Description**  
Sets access rights to the file within user's contact circles to share it with another users.

**Parameters**  
*```fileId```* - file identifier (GUID).  
*```userId```* - identifier of the user who owns a file.  
*```circles```* - array like ```array('identifier_of_circle' => bool ...)```.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```string / DOMDocument / array / bool getFileInfo($fileId)```**

**Description**  
Returns information about the file.

**Parameters**  
*```fileId```* - file identifier (GUID).  

**Return values**  
In case of success returns information about the file as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "id": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "fullname": {
            "type": ["string", "null"],
            "maxLength": 100
        },
        "title": {
            "type": ["string", "null"],
            "maxLength": 255
        },
        "filename": {
            "type": "string",
            "maxLength": 255
        },
        "comment": {
            "type": ["string", "null"],
            "maxLength": 1024
        },
        "mime": {
            "type": "string",
            "pattern": "^[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})\/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})$"
        },
        "size": {
            "type": "integer",
            "minimum": 1
        },
        "time": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        }
    },
    "required": ["id", "username", "fullname", "title", "filename", "comment", "mime", "size", "time"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="fileinfo">
            <attribute name="id"> <!-- file identifier -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <element name="username"> <!-- username of the user who uploaded the file -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </element>
            <element name="fullname"> <!-- full name of the user who uploaded the file -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="title"> <!-- title of the file -->
                <data type="string">
                    <param name="maxLength">255</param>
                </data>
            </element>
            <element name="filename"> <!-- file name  -->
                <data type="string">
                    <param name="maxLength">255</param>
                </data>
            </element>
            <element name="comment"> <!-- comment of the file -->
                <data type="string">
                    <param name="maxLength">1024</param>
                </data>
            </element>
            <element name="mime"> <!-- mime type of the file -->
                <data type="string">
                    <param name="pattern">[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})</param>
                </data>
            </element>
            <element name="size"> <!-- size of the file in bytes -->
                <data type="positiveInteger" />
            </element>
            <element name="time"> <!-- time when the file was uploaded -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'id' => 'file identifier',
    'username' => 'username of the user',
    'fullname' => 'full name of the user',
    'title' => 'file title',
    'filename' => 'file name',
    'comment' => 'comment of the file',
    'mime' => 'mime type of the file',
    'size' => file_size_in_bytes,
    'time' => 'time when the file was uploaded'
)
```

** **

**```bool getFile(string $fileId, string $disp = 'inline', bool $nocache = FALSE)```**

**Description**  
Checks access rights and returns file by using of *mod_xsendfile* (for *Apache2*), *X-Accel-Redirect* (for *NGINX*) or *X-LIGHTTPD-send-file* (for *lighttpd*).

**Parameters**  
*```fileId```* - file identifier (GUID).  
*```disp```* - may receive values ```'inline'``` and ```'attachment'```. Specifies value of title Content-Disposition.  Value ```'inline'``` means that content should be displayed inside the browser page. Value ```'attachment'``` means that content should be downloaded, most browsers provide 'Save as' dialog for this.  
*```nocache```* - a flag indicating the browser that it shouldn't cache recieved file, if value is ```TRUE```. If value is ```FALSE``` tha browser should to cache recieved file.

**Return values**  
Returns requared file in case of success. Otherwise returns a page with one of the following HTTP statuses:  
```400 Bad Request```, if incoming parameters are incorrect;  
```403 Forbidden```, if the access is denied;  
```404 Not Found```, if the file isn't found;  
```501 Not Implemented```, if the method is executed under unsupported web server;  
```503 Service Unavailable```, if there is some error on the server.  
Returns ```FALSE``` in case of executing not under the web server.

** **

**```bool attachFile( string $fileId, string $msgId, int $userId)```**

**Description**  
Attaches file to a message.

**Parameters**  
*```fileId```* - file identifier (GUID).  
*```msgId```* - message identifier (GUID).  
*```userId```* - identifier of the user who owns file and message.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool unattachFile(string $fileId, string $msgId, int $userId)```**

**Description**  
Unattaches file from a message.

**Parameters**  
*```fileId```* - file identifier (GUID).  
*```msgId```* - message identifier (GUID).  
*```userId```* - identifier of the user who owns file and message.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool delFile(string $fileId, int $userId, bool $force = FALSE)```**

**Description**  
Deletes a staged file.

**Parameters**  
*```fileId```* - file identifier (GUID).  
*```userId```* - identifier of the user who owns file.  
*```force```* - if value is ```FALSE```, a file will be deleted only if it is not attached to a message. If value is ```TRUE```, a file will be deleted even if it is attached to a message.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool shareMsg(string $msgId, int $userId, array $circles)```**

**Description**  
Sets access rights to the message within user's contact circles to share it with another users.

**Parameters**  
*```msgId```* - file identifier (GUID).  
*```userId```* - identifier of the user who owns a message.  
*```circles```* - array like array('identifier_of_circle' => bool ...).

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```string / DOMDocument / bool getMsg(string $msgId)```**

**Description**  
Returns message data.

**Parameters**  
*```msgId```* - message identifier (GUID).

**Return values**
In case of success returns message data as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "id": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "source": {
            "oneOf": [
                {
                    "type": ["string", "null"],
                    "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                },
                {
                    "type": "string",
                    "length": 0
                }
            ]
        },
        "title": {
            "type": "string",
            "maxLength": 250
        },
        "text": {
            "type": "string",
            "maxLength": 65535
        },
        "time": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "username": {
            "type": "string",
            "pattern": "[a-zA-Z0-9_]{3,20}"
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        }
    },
    "required": ["id", "source", "title", "text", "time", "username", "fullname"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="message"> <!-- message node -->
            <attribute name="id"> <!-- message identifier -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <choice> <!-- identifier of the source message -->
                <element name="source">
                    <data type="string">
                        <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                    </data>
                </element>
                <element name="source">
                    <data type="string">
                        <param name="length">0</param>
                    </data>
                </element>
            </choice>
            <element name="title"> <!-- title of a message -->
                <data type="string">
                    <param name="maxLength">250</param>
                </data>
            </element>
            <element name="text"> <!-- text of a message -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="time"> <!-- date and time of the message creation -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="username"> <!-- username who created the message -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </element>
            <element name="fullname"> <!-- full name of the user who created the message -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'id' => 'message identifier',
    'source' => 'identifier of the source message',
    'title' => 'title of a message',
    'text' => 'text of a message',
    'time' => 'time of the message creation',
    'username' => 'username who created the message',
    'fullname' => 'full name of the user who created the message'
)
```

** **

**```string / DOMDocument / bool msgFiles(string $msgId)```**

**Description**  
Returns list of files which are attached to the message.

**Parameters**  
*```msgId```* - message identifier (GUID).

**Return values**  
In case of success returns list of files which are attached to the message as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "msgid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "files": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 255
                    },
                    "filename": {
                        "type": "string",
                        "minLength": 3,
                        "maxLength": 255
                    },
                    "mime": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})\/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})$"
                    }
                },
                "required": ["id", "title", "filename", "mime"]
            }
        }
    },
    "required": ["msgid", "files"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="files">
            <attribute name="msgid"> <!-- message identifier -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="file"> <!-- node of file data -->
                    <element name="id"> <!-- file identifier -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="title"> <!-- file title -->
                        <data type="string">
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="filename"> <!-- file name -->
                        <data type="string">
                            <param name="minLength">3</param>
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="mime"> <!-- file MIME-type -->
                        <data type="string">
                            <param name="pattern">[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'msgid' => '',
    'files' => array(
        array(
            'id' => 'file identifier',
            'title' => 'file title',
            'filename' => 'file name',
            'mime' => 'file MIME-type'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool getFileShares(string $fileId, int $userId)```**

**Description**  
Returns data about access rights to the file.

**Parameters**  
*```fileId```* - file identifier (GUID).  
*```userId```* - identifier of the user who owns the file.

**Return values**  
In case of success returns data about access rights to the file as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "fileId": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "circles": {
            "type": "array",
            "items": {
                "type": "object",
                "oneOf": [
                    {
                        "properties": {
                            "id": {
                                "type": "string",
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            },
                            "name": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 50
                            },
                            "access": {
                                "type": "integer",
                                "minimum": 0,
                                "maximum": 1
                            }
                        }
                    },
                    {
                        "properties": {
                            "id": {
                                "type": "string",
                                "pattern": "^public$"
                            },
                            "name": {
                                "type": "string",
                                "length": 0
                            },
                            "access": {
                                "type": "integer",
                                "minimum": 0,
                                "maximum": 1
                            }
                        }
                    }
                ],
                "required": ["id", "name", "access"]
            }
        }
    },
    "required": ["fileId", "circles"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="shares">
            <attribute name="fileId"> <!-- file identifier -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <choice>
                    <element name="circle"> <!-- first version of user's circle of contacts -->
                        <element name="id"> <!-- identifier of a circle -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="name"> <!-- name of a circle -->
                            <data type="string">
                                <param name="minLength">1</param>
                                <param name="maxLength">50</param>
                            </data>
                        </element>
                        <element name="access"> <!-- value of access to the file -->
                            <choice>
                                <value>0</value>
                                <value>1</value>
                            </choice>
                        </element>
                    </element>
                    <element name="circle"> <!-- second version of user's circle of contacts -->
                        <element name="id"> <!-- identifier of a public access circle -->
                            <data type="string">
                                <param name="pattern">public</param>
                            </data>
                        </element>
                        <element name="name"> <!-- empty name of a circle -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="access"> <!-- value of access to the file -->
                            <choice>
                                <value>0</value>
                                <value>1</value>
                            </choice>
                        </element>
                    </element>
                </choice>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
   'fileId' => 'file identifier',
    'circles' => array(
        array(
            'id' => 'identifier of a circle',
            'name' => 'name of a circle',
            'access' => value_of_access_to_the_file
        ),
        array(...),
        ...
    ) 
)
```

** **

**```string / DOMDocument / bool getMsgShares(string $msgId, int $userId)```**

**Description**  
Returns data about access rights to the message.

**Parameters**  
*```msgId```* - message identifier (GUID).  
*```userId```* - identifier of the user who owns the message.

**Return values**  
In case of success returns data about access rights to the message as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "msgId": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "circles": {
            "type": "array",
            "items": {
                "type": "object",
                "oneOf": [
                    {
                        "properties": {
                            "id": {
                                "type": "string",
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            },
                            "name": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 50
                            },
                            "access": {
                                "type": "integer",
                                "minimum": 0,
                                "maximum": 1
                            }
                        }
                    },
                    {
                        "properties": {
                            "id": {
                                "type": "string",
                                "pattern": "^public$"
                            },
                            "name": {
                                "type": "string",
                                "length": 0
                            },
                            "access": {
                                "type": "integer",
                                "minimum": 0,
                                "maximum": 1
                            }
                        }
                    }
                ],
                "required": ["id", "name", "access"]
            }
        }
    },
    "required": ["msgId", "circles"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="shares">
            <attribute name="msgId"> <!-- message identifier -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <choice>
                    <element name="circle"> <!-- first version of user's circle of contacts -->
                        <element name="id"> <!-- identifier of a circle -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="name"> <!-- name of a circle -->
                            <data type="string">
                                <param name="minLength">1</param>
                                <param name="maxLength">50</param>
                            </data>
                        </element>
                        <element name="access"> <!-- value of access to the message -->
                            <choice>
                                <value>0</value>
                                <value>1</value>
                            </choice>
                        </element>
                    </element>
                    <element name="circle"> <!-- second version of user's circle of contacts -->
                        <element name="id"> <!-- identifier of a public access circle -->
                            <data type="string">
                                <param name="pattern">public</param>
                            </data>
                        </element>
                        <element name="name"> <!-- empty name of a circle -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="access"> <!-- value of access to the message -->
                            <choice>
                                <value>0</value>
                                <value>1</value>
                            </choice>
                        </element>
                    </element>
                </choice>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'msgId' => 'message identifier',
    'circles' => array(
        array(
            'id' => 'identifier of a circle',
            'name' => 'name of a circle',
            'access' => value_of_access_to_the_message
        ),
        array(...),
        ...
    ) 
)
```

** **

**```bool editFile(string $fileId, int $userId, string $title, string $comment)```**

**Description**  
Edits information about the file.

**Parameters**  
*```fileId```* - file identifier.  
*```userId```* - user identifier.  
*```title```* - new short title of the file.  
*```comment```* - new short comment to the file.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```array / bool repostMsg(string $msgId, int $userId, bool $hlink = TRUE)```**

**Description**  
Makes repost of the message with all it's files that are allowed to access.

**Parameters**  
*```msgId```* - identifier of a copied message.  
*```userId```* - identifier of the user who makes repost.  
*```hlink```* - a flag which points whether needed to create hard links to files that are attached to the message (it allows to save disk storage capacity), or whether needed to copy files. If value is ```TRUE``` hard links are created. If value is ```FALSE``` files are copied.

**Return values**  
In case of success repost an array with data about copy of the message like ```array('message' => 'message_identifier', 'files' => array_values('file_identifier_1', 'file_identifier_2' ...))``` is returned.

** **

**```bool editMsg(string $msgId, int $userId, string $title, string $text)```**

**Description**  
Edits the message.

**Parameters**  
*```msgId```* - message identifier.  
*```userId```* - identifier of the user who owns the message.  
*```title```* - message title.  
*```text```* - message text.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool delMsg($msgId, $userId, $keepFiles = TRUE)```**

**Description**  
Deletes the message.

**Parameters**  
*```msgId```* - identifier of the message to delete.  
*```userId```* - identifier of the user who owns the message.  
*```keepFiles```* - a flag which points whether needed to keep files that are attached to the message. If value is ```TRUE``` files are kept. If value is ```FALSE``` files that are attached to the message are deleted together with it.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```string / bool repostFile($fileId, $userId, $hlink = TRUE)```**

**Description**  
Makes repost of the file.

**Parameters**  
*```fileId```* - identifier of the file to copy.  
*```userId```* - identifier of the user who makes repost.  
*```hlink```* - a flag which points whether needed to create hard link to the file (it allows to save disk storage capacity), or whether needed to copy the file. If value is ```TRUE``` hard link is created. If value is ```FALSE``` the file is copied.

**Return values**  
In case of success returns identifier of the file copy (GUID). Othrwise returns ```FALSE```.

** **

**```array / bool sumUserMsgs(int $userId, int $rpp = 20)```**

**Description**  
Returns summary data about messages published by the user per page.

**Parameters**  
*```userId```* - identifier of the user whose messages are required.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
Array like ```array('records' => int $totalRecs, 'pages' => int $totalPages)```, where ```totalRecs``` - total numbes of messages, ```totalPages``` - total number of pages, calculated as ```totalRecs/rpp```. Data are displayed with taking into account of access rights. In case of failure returns ```FALSE```.

** **

**```string / DOMDocument / bool userMsgs(int $userId, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = array('time'), bool $ascent = FALSE)```**

**Description**  
Returns a page with messages of the user.

**Parameters**  
*```userId```* - identifier of the user whose messages are required.  
*```pageNumber```* - page number, integer from 1 and more.  
*```totalPages```* - total number of pages.  
*```rpp```* - desired maximum number of records per page.  
*```orderBy```* - sorting parameters of output of user messages. Allowed values of parameters: ```'time'```, ```'title'```. Sequence of parameters is defined as array, for example, ```array('title', 'time')```. Incorrectly defined sequence of parameters is set equal to ```array('time')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns user messages as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "source", "title", "text", "time"]
            }
        }
    },
    "required": ["username", "uid", "fullname", "messages"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="username"> <!-- username -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- user identifier -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- full name of the user -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- node of a message -->
                    <element name="id"> <!-- identifier of a message -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- source of a message -->
                        <element name="source"> <!-- message does not have any source -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- message has a source -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- title of a message -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- text of a message -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of message creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'username' => 'username',
    'uid' => user_identifier,
    'fullname' => 'full name of the user',
    'messages' => array(
        array(
            'id' => 'identifier of a message',
            'source' => 'source of a message',
            'title' => 'title of a message',
            'text' => 'text of a message',
            'time' => 'time of message creation'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool msgStripe(int $userId, int $rpp = 20)```**

**Description**  
Returns stripe of user messages.

**Parameters**  
*```userId```* - identifier of the user whose messages are requested.  
*```rpp```* - desired maximum number of records per page.

**Return values**  
In case of success returns stripe of user messages as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. Data are displayed with taking into account of access rights.  

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "source", "title", "text", "time"]
            }
        },
        "maxmark": {
            "type": "number"
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["username", "uid", "fullname", "messages", "minmark", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="username"> <!-- username -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- user identifier -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- full name of the user -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- node of a message -->
                    <element name="id"> <!-- identifier of a message -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- source of a message -->
                        <element name="source"> <!-- message does not have any source -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- message has a source -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- title of a message -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- text of a message -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of message creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'username' => 'username',
    'uid' => user_identifier,
    'fullname' => 'full name of the user',
    'messages' => array(
        array(
            'id' => 'identifier of a message',
            'source' => 'source of a message',
            'title' => 'title of a message',
            'text' => 'text of a message',
            'time' => 'time of message creation',
        ),
        array(...),
        ...
    ),
    'minmark' => minimum.mark,
    'maxmark' => maximum.mark
)
```

** **

**```string / DOMDocument / bool appendMsgStripe(int $userId, double $minMark, int $rpp = 20)```**

**Description**  
Returns stripe of user messages following after the message with minimum mark.

**Parameters**  
*```userId```* - identifier of the user whose messages are requested.  
*```minMark```* - minimum mark.  
*```rpp```* - desired maximum number of records per page.

**Return values**  
In case of success returns stripe of user messages as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "source", "title", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["username", "uid", "fullname", "messages", "minmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="username"> <!-- username -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- identifier of the user -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- full name of the user -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- node of a message -->
                    <element name="id"> <!-- identifier of a message -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- source of a message -->
                        <element name="source"> <!-- message does not have any source -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- message has a source -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- title of a message -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- text of a message -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of message creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'username' => 'username',
    'uid' => user_identifier,
    'fullname' => 'full name of the user',
    'messages' => array(
        array(
            'id' => 'identifier of a message',
            'source' => 'source of a message',
            'title' => 'title of a message',
            'text' => 'text of a message',
            'time' => 'time of message creation',
        ),
        array(...),
        ...
    ),
    'minmark' => minimum.mark
)
```

** **

**```string / DOMDocument / bool updateMsgStripe(int $userId, double $maxMark)```**

**Description**  
Returns stripe of user messages following before the message with maximum mark.

**Parameters**  
*```userId```* - identifier of the user whose messages are requested.  
*```maxMark```* - maximim mark.

**Return values**  
In case of success returns stripe of user messages as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "source", "title", "text", "time"]
            }
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["username", "uid", "fullname", "messages", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="username"> <!-- username -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- identifier of the user -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- full name of the user -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- node of a message -->
                    <element name="id"> <!-- identifier of a message -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- source of a message -->
                        <element name="source"> <!-- message does not have any source -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- message does a source -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- title of a message -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- text of a message -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of message creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'username' => 'username',
    'uid' => user_identifier,
    'fullname' => 'full name of the user',
    'messages' => array(
        array(
            'id' => 'identifier of a message',
            'source' => 'source of a message',
            'title' => 'title of a message',
            'text' => 'text of a message',
            'time' => 'time of message creation',
        ),
        array(...),
        ...
    ),
    'maxmark' => maximum.mark
)
```

** **

**```array / bool sumUserFiles(int $userId, int $rpp = 20)```**

**Description**  
Returns summary information about files staged by a user, per page.

**Parameters**  
*```userId```* - identifier of the user whose messages are requested.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
Array like ```array('records' => int $totalRecs, 'pages' => int $totalPages)```, where ```totalRecs``` - total number files, ```totalPages``` - total number of pages, calculated as ```totalRecs/rpp```. Data are displayed with taking into account of access rights. In case of failure returns ```FALSE```.

** **

**```string / DOMDocument / bool userFiles(int $userId, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = array('time'), bool $ascent = FALSE)```**

**Description**  
Returns a page with file of the user.

**Parameters**  
*```userId```* - identifier of the user whose messages are requested.  
*```pageNumber```* - page number, integer from 1 and more.  
*```totalPages```* - total number of pages.  
*```rpp```* - desired maximum number of records per page.  
*```orderBy```* - sorting parameters of output user files. Allowed values of parameters: ```'time'```, ```'title'```. Sequence of parameters is defined as array, for example, ```array('title', 'time')```. Incorrectly defined sequence of parameters is set equal to ```array('time')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns files of the user as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "files": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 255
                    },
                    "filename": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 255
                    },
                    "comment": {
                        "type": "string",
                        "maxLength": 1024
                    },
                    "mime": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})\/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})$",
                        "maxLength": 50
                    },
                    "size": {
                        "type": "integer",
                        "minimal": 1
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "title", "filename", "comment", "mime", "size", "time"]
            }
        }
    },
    "required": ["username", "uid", "fullname", "files"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="files">
            <attribute name="username"> <!-- username -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- identifier of the user -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- full name of the user -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="file"> <!-- node of a file -->
                    <element name="id"> <!-- identifier of a file -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="title"> <!-- tile of a file -->
                        <data type="string">
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="filename"> <!-- file name -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="comment"> <!-- comment of a file -->
                        <data type="string">
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="mime"> <!-- MIME-type of a file -->
                        <data type="string">
                            <param name="pattern">[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})</param>
                        </data>
                    </element>
                    <element name="size"> <!-- file size in bytes -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- date and time when file was staged -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'username' => 'username',
    'uid' => user_identifier,
    'fullname' => 'full name of the user',
    'files' => array(
        array(
            'id' => 'file identifier',
            'title' => 'file title',
            'filename' => 'file name',
            'comment' => 'comment of the file',
            'mime' => 'mime type of the file',
            'size' => file_size_in_bytes,
            'time' => 'time when the file was uploaded'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool fileStripe(int $userId, int $rpp = 20)```**

**Description**  
Returns stripe of user files.

**Parameters**  
*```userId```* - identifier of the user whose files are requested.  
*```rpp```* - desired maximum number of records per page.

**Return values**  
In case of success returns stripe of user files as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "files": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 255
                    },
                    "filename": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 255
                    },
                    "comment": {
                        "type": "string",
                        "maxLength": 1024
                    },
                    "mime": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})\/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})$",
                        "maxLength": 50
                    },
                    "size": {
                        "type": "integer",
                        "minimal": 1
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "title", "filename", "comment", "mime", "size", "time"]
            }
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["username", "uid", "fullname", "files", "minmark", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="files">
            <attribute name="username"> <!-- username -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- identifier of the user -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- fill name of the user -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="file"> <!-- node of a file -->
                    <element name="id"> <!-- identifier of a file -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="title"> <!-- title of a file -->
                        <data type="string">
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="filename"> <!-- file name -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="comment"> <!-- comment of a file -->
                        <data type="string">
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="mime"> <!-- MIME-type of a file -->
                        <data type="string">
                            <param name="pattern">[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})</param>
                        </data>
                    </element>
                    <element name="size"> <!-- file size in bytes -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- date and time when file was staged -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'username' => 'username',
    'uid' => user_identifier,
    'fullname' => 'full name of the user',
    'files' => array(
        array(
            'id' => 'file identifier',
            'title' => 'file title',
            'filename' => 'file name',
            'comment' => 'comment of the file',
            'mime' => 'mime type of the file',
            'size' => file_size_in_bytes,
            'time' => 'time when the file was uploaded'
        ),
        array(...),
        ...
    ),
    'minmark' => minimum.mark,
    'maxmark' => maximum.mark
)
```

** **

**```string / DOMDocument / bool appendFileStripe(int $userId, double $minMark, int $rpp = 20)```**

**Description**  
Returns stripe of user files following after the file with minimum mark.

**Parameters**  
*```userId```* - identifier of the user whose files are requested.  
*```minMark```* - minimum mark.  
*```rpp```* - desired maximum number of records per page.

**Return values**  
In case of success returns stripe of user files as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "files": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 255
                    },
                    "filename": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 255
                    },
                    "comment": {
                        "type": "string",
                        "maxLength": 1024
                    },
                    "mime": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})\/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})$",
                        "maxLength": 50
                    },
                    "size": {
                        "type": "integer",
                        "minimal": 1
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "title", "filename", "comment", "mime", "size", "time"]
            }
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["username", "uid", "fullname", "files", "minmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="files">
            <attribute name="username"> <!-- username -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- identifier of the user -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- full name of the user -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="file"> <!-- node of a file -->
                    <element name="id"> <!-- identifier of a file -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="title"> <!-- title of a file -->
                        <data type="string">
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="filename"> <!-- file name -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="comment"> <!-- comment of a file -->
                        <data type="string">
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="mime"> <!-- MIME-type of a file -->
                        <data type="string">
                            <param name="pattern">[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})</param>
                        </data>
                    </element>
                    <element name="size"> <!-- file size in bytes -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- date and time when file was staged -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'username' => 'username',
    'uid' => user_identifier,
    'fullname' => 'full name of the user',
    'files' => array(
        array(
            'id' => 'file identifier',
            'title' => 'file title',
            'filename' => 'file name',
            'comment' => 'comment of the file',
            'mime' => 'mime type of the file',
            'size' => file_size_in_bytes,
            'time' => 'time when the file was uploaded'
        ),
        array(...),
        ...
    ),
    'minmark' => minimum.mark
)
```

** **

**```string / DOMDocument / bool updateFileStripe(int $userId, double $maxMark)```**

**Description**  
Returns stripe of user files following before the file with maximum mark.

**Parameters**  
*```userId```* - identifier of the user whose files are requested.  
*```maxMark```* - maximum mark.

**Return values**  
In case of success returns stripe of user files as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.
 
JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "files": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 255
                    },
                    "filename": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 255
                    },
                    "comment": {
                        "type": "string",
                        "maxLength": 1024
                    },
                    "mime": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})\/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})$",
                        "maxLength": 50
                    },
                    "size": {
                        "type": "integer",
                        "minimal": 1
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "title", "filename", "comment", "mime", "size", "time"]
            }
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["username", "uid", "fullname", "files", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="files">
            <attribute name="username"> <!-- userman -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- identifier of the user -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- full name of the user -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="file"> <!-- node of a file -->
                    <element name="id"> <!-- identifier of a file -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="title"> <!-- tile of a file -->
                        <data type="string">
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="filename"> <!-- file name -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="comment"> <!-- comment of a file -->
                        <data type="string">
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="mime"> <!-- MIME-type of a file -->
                        <data type="string">
                            <param name="pattern">[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})</param>
                        </data>
                    </element>
                    <element name="size"> <!-- file size in bytes -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- date and time when file was staged -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'username' => 'username',
    'uid' => user_identifier,
    'fullname' => 'full name of the user',
    'files' => array(
        array(
            'id' => 'file identifier',
            'title' => 'file title',
            'filename' => 'file name',
            'comment' => 'comment of the file',
            'mime' => 'mime type of the file',
            'size' => file_size_in_bytes,
            'time' => 'time when the file was uploaded'
        ),
        array(...),
        ...
    ),
    'maxmark' => maximum.mark
)
```

** **

**```array / bool sumUserSubs(int $userId, int $rpp = 20)```**

**Description**  
Returns summary data about messages published by contacts of the user for him, per page.

**Parameters**  
*```userId```* - identifier of the user for whom messages' data is requested.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
Array like ```array('records' => int $totalRecs, 'pages' => int $totalPages)```, where ```totalRecs``` - total number of messages, ```totalPages``` - total number of pages, calculated as ```totalRecs/rpp```. Data are displayed with taking into account of access rights. In case of failure returns ```FALSE```.

** **

**```string / DOMDocument / bool userSubs(int $userId, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = array('time'), bool $ascent = FALSE)```**

**Description**  
Returns a page with messages published by contacts of the user for him.

**Parameters**  
*```userId```* - identifier of the user for whom messages are requested.  
*```pageNumber```* - page number, integer from 1 and more.  
*```totalPages```* - total number of pages.  
*```rpp```* - desired maximum number of records per page.  
*```orderBy```* - sorting parameters of output of user subscriptions. Allowed values of parameters: ```'time'```, ```'title'```. Sequence of parameters is defined as array, for example, ```array('title', 'time')```. Incorrectly defined sequence of parameters is set equal to ```array('time')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns a page with messages published by contacts of the user for him as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.  

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "uid": {
                "type": "integer",
                "minimum": 1
            },
            "username": {
                "type": "string",
                "pattern": "^[a-zA-Z0-9_]{3,20}$"
            },
            "fullname": {
                "type": "string",
                "maxLength": 100
            },
            "id": {
                "type": "string",
                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
            },
            "source": {
                "type": "string",
                "oneOf": [
                    {
                        "length": 0
                    },
                    {
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    }
                ]
            },
            "title": {
                "type": "string",
                "maxLength": 250
            },
            "text": {
                "type": "string",
                "maxLength": 512
            },
            "time": {
                "type": "string",
                "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
            }
        },
        "required": ["uid", "username", "fullname", "id", "source", "title", "text", "time"]
    }
}
```

XML structure is described by the following schema RELAX NG:
<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <zeroOrMore>
                <element name="message"> <!-- node of a message -->
                    <attribute name="uid"> <!-- user identifier -->
                        <data type="positiveInteger" />
                    </attribute>
                    <attribute name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </attribute>
                    <attribute name="fullname"> <!-- full name of a user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </attribute>
                    <element name="id"> <!-- message identifier -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- source of a message -->
                        <element name="source"> <!-- message does not have any source -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- message has a source -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- title of a message -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- text of a message -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of message creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>

Array structure has a view like:

```
array(
    array(
        'uid' => user_identifier,
        'username' => 'username',
        'fullname' => 'full name of the user',
        'id' => 'identifier of a message',
        'source' => 'source of a message',
        'title' => 'title of a message',
        'text' => 'text of a message',
        'time' => 'time of message creation',
    ),
    array(...),
    ...
)
```

** **

**```string / DOMDocument / bool subStripe(int $userId, int $rpp = 20)```**

**Description**  
Returns stripe of messages published by contacts of the user for him.

**Parameters**  
*```userId```* - identifier of the user for whom messages are requested.  
*```rpp```* - desired maximum number of records per page.

**Return values**  
In case of success returns stripe of messages published by contacts of the user for him as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.. Data are displayed with taking into account of access rights.  

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "uid": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": "string",
                        "maxLength": 100
                    },
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["uid", "username", "fullname", "id", "source", "title", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["messages", "minmark", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- node of a message -->
                    <attribute name="uid"> <!-- identifier of a user -->
                        <data type="positiveInteger" />
                    </attribute>
                    <attribute name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </attribute>
                    <attribute name="fullname"> <!-- full name of a user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </attribute>
                    <element name="id"> <!-- identifier of a message -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- source of a message -->
                        <element name="source"> <!-- message does not have any source -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- message has a source -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- title of a message -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- text of a message -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of message creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'messages' => array(
        array(
            'uid' => user_identifier,
            'username' => 'username',
            'fullname' => 'full name of the user',
            'id' => 'identifier of a message',
            'source' => 'source of a message',
            'title' => 'title of a message',
            'text' => 'text of a message',
            'time' => 'time of message creation',
        ),
        array(...),
        ...
    ),
    'minmark' => minimum.mark,
    'maxmark' => maximum.mark
)
```

** **

**```string / DOMDocument / bool appendSubStripe(int $userId, double $minMark, int $rpp = 20)```**

**Description**  
Returns stripe of messages published by contacts of the user for him and following after the message with minimum mark.

**Parameters**  
*```userId```* - identifier of the user for whom messages are requested.  
*```minMark```* - minimum mark.  
*```rpp```* - desired maximum number of records per page.

**Return values**  
In case of success returns stripe of messages published by contacts of the user for him as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. Data are displayed with taking into account of access rights.  

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "uid": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": "string",
                        "maxLength": 100
                    },
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["uid", "username", "fullname", "id", "source", "title", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["messages", "minmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- node of a message -->
                    <attribute name="uid"> <!-- identifier of a user -->
                        <data type="positiveInteger" />
                    </attribute>
                    <attribute name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </attribute>
                    <attribute name="fullname"> <!-- full name of a user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </attribute>
                    <element name="id"> <!-- identifier of a message -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- source of a message -->
                        <element name="source"> <!-- message does not have a source -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- message has a source -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- title of a message -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- text of a message -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of message creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'messages' => array(
        array(
            'uid' => user_identifier,
            'username' => 'username',
            'fullname' => 'full name of the user',
            'id' => 'identifier of a message',
            'source' => 'source of a message',
            'title' => 'title of a message',
            'text' => 'text of a message',
            'time' => 'time of message creation',
        ),
        array(...),
        ...
    ),
    'minmark' => minimum.mark
)
```

** **

**```string / DOMDocument / bool updateSubStripe(int $userId, double $maxMark)```**

**Description**  
Returns stripe of messages published by contacts of the user for him and following before the message with maximum mark.

**Parameters**  
*```userId```* - identifier of the user for whom messages are requested.  
*```maxMark```* - maximum mark.

**Return values**  
In case of success returns stripe of messages published by contacts of the user for him as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. Data are displayed with taking into account of access rights.  

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "uid": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": "string",
                        "maxLength": 100
                    },
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["uid", "username", "fullname", "id", "source", "title", "text", "time"]
            }
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["messages", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- node of a message -->
                    <attribute name="uid"> <!-- identifier of a message -->
                        <data type="positiveInteger" />
                    </attribute>
                    <attribute name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </attribute>
                    <attribute name="fullname"> <!-- full name of a user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </attribute>
                    <element name="id"> <!-- identifier of a message -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- source of a message -->
                        <element name="source"> <!-- message does not have any source -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- message has a source -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- title of a message -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- text of a message -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- data and time of message creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'messages' => array(
        array(
            'uid' => user_identifier,
            'username' => 'username',
            'fullname' => 'full name of the user',
            'id' => 'identifier of a message',
            'source' => 'source of a message',
            'title' => 'title of a message',
            'text' => 'text of a message',
            'time' => 'time of message creation',
        ),
        array(...),
        ...
    ),
    'maxmark' => maximum.mark
)
```

** **

**```string / bool createMsgComment(string $msgId, int $userId, string $comment, string $parentId = '')```**

**Description**  
Creates comment to a message.

**Parameters**  
*```msgId```* - identifier of a message to which the comment is created.  
*```userId```* - identifier of the user from name of whom a comment is created.  
*```comment```* - text of the comment, up to 1024 characters.  
*```parentId```* - identifier of the parental comment, define it if a comment is created as an answer to previous comment.

**Return values**  
In case of success returns identifier (GUID) of the created comment. Otherwise returns FALSE.

** **

**```bool editMsgComment(string $comment, string $commentId, int $userId)```**

**Description**  
Edits comment to a message.

**Parameters**  
*```comment```* - text of the comment, up to 1024 characters.  
*```commentId```* - identifier of a comment.  
*```userId```* - identifier of the user who owns an editable comment.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```string / DOMDocument / bool getMsgComment(sting $commentId, int $userId)```**

**Description**  
Reads and returns information about the comment.

**Parameters**  
*```commentId```* - identifier of the comment.  
*```userId```* - identifier of the user who owns an editable comment.

**Return values**  
In case of success returns information about the comment as object ```DOMDocument```, or as string JSON, otherwise returns ```FALSE```.  

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "uid": {
            "type": "integer"
        },
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "fullname": {
            "type": ["string", "null"],
            "maxLength": 100
        },
        "cid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "text": {
            "type": "string",
            "minLength": 1,
            "maxLength": 1024
        }
    },
    "required": ["uid", "username", "fullname", "cid", "text"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comment">
            <element name="uid"> <!-- user identifier -->
                <data type="positiveInteger" />
            </element>
            <element name="username"> <!-- user name -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </element>
            <element name="fullname"> <!-- full name of the user -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="cid"> <!-- identifier of the comment -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </element>
            <element name="text"> <!-- text of the comment -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">1024</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'uid' => 'user identifier', 
    'username' => 'username', 
    'fullname' => 'full name of the user', 
    'cid' => 'identifier of the comment', 
    'text' => 'text of the comment'
)
```

** **

**```bool eraseMsgComment(string $commentId, string $userId)```**

**Description**  
Erases the comment.

**Parameters**  
*```commentId```* - identifier of the comment.  
*```userId```* - identifier of the user who owns the comment.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```string / DOMDocument / bool getMsgAllComments(string $msgId)```**

**Description**  
Reads and returns stripe of all comments to the message.

**Parameters**  
*```msgId```* - message identifier.  

**Return values**  
In case of success returns stripe of comments to the message as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.  

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "topic": {
            "type": ["string", "null"]
        },
        "tid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["comments", "topic", "tid", "minmark", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="topic"> <!-- topic name -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="tid"> <!-- topic identifier -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- node of the comment -->
                    <element name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- full name of the user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- identifier othe commen -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- identifier of the parental comment -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- text of the comment -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of the comment creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'topic' => 'topic name',
    'tid' => 'topic identifier',
    'minmark' => minimum.mark,
    'maxmark' => maximum.mark,
    'comments' => array(
        array(
            'username' => 'username',
            'fullname' => 'full name of the user',
            'cid' => 'identifier of the comment',
            'pcid' => 'identifier of the parental comment',
            'text' => 'text of the comment',
            'time' => 'date and time of the comment creation'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool getMsgComments(string $msgId, int $rpp = 20)```**

**Description**  
Reads and returns stripe of comments to the message.

**Parameters**  
*```msgId```* - message identifier.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
In case of success returns stripe of comments to the message as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.  

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "topic": {
            "type": ["string", "null"]
        },
        "tid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["comments", "topic", "tid", "minmark", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="topic"> <!-- topic name -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="tid"> <!-- topic identifier -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- node of the comment -->
                    <element name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- full name of the user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- identifier othe commen -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- identifier of the parental comment -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- text of the comment -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of the comment creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'topic' => 'topic name',
    'tid' => 'topic identifier',
    'minmark' => minimum.mark,
    'maxmark' => maximum.mark,
    'comments' => array(
        array(
            'username' => 'username',
            'fullname' => 'full name of the user',
            'cid' => 'identifier of the comment',
            'pcid' => 'identifier of the parental comment',
            'text' => 'text of the comment',
            'time' => 'date and time of the comment creation'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool appendMsgComments($msgId, $minMark, $rpp = 20)```**

**Description**  
Reads and returns stripe of comments to the message and following after the comment with minimum mark.

**Parameters**  
*```msgId```* - message identifier.  
*```minMark```* - minimum mark.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
In case of success returns stripe of comments to the message as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.  

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["comments", "minmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- node of the comment -->
                    <element name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- full name of the user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- identifier othe comment -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- identifier othe parental comment -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- text othe comment -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of the comment creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'topic' => 'topic name',
    'tid' => 'topic identifier',
    'minmark' => minimum.mark,
    'comments' => array(
        array(
            'username' => 'username',
            'fullname' => 'full name of the user',
            'cid' => 'identifier of the comment',
            'pcid' => 'identifier of the parental comment',
            'text' => 'text of the comment',
            'time' => 'date and time of the comment creation'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool updateMsgComments(string $msgId, double $maxMark)```**

**Description**  
Reads and returns stripe of comments to the message and following before the comment with maximum mark.

**Parameters**  
*```msgId```* - message identifier.  
*```maxMark```* - maximum mark.

**Return values**  
In case of success returns stripe of comments to the message as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.  

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["comments", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- node of the comment -->
                    <element name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- full name of the user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- identifier of the comment -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- identifier of the parental comment -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- text of the comment -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of the comment creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'topic' => 'topic name',
    'tid' => 'topic identifier',
    'maxmark' => maximum.mark,
    'comments' => array(
        array(
            'username' => 'username',
            'fullname' => 'full name of the user',
            'cid' => 'identifier of the comment',
            'pcid' => 'identifier of the parental comment',
            'text' => 'text of the comment',
            'time' => 'date and time of the comment creation'
        ),
        array(...),
        ...
    )
)
```

**  **

**```string / DOMDocument / bool pubMsgs(int $rpp = 20)```**

**Description**  
Returns a stripe with public messages from all users.

**Parameters**  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
In case of success returns a stripe with public messages from all users as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.  

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "uid": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": "string",
                        "maxLength": 100
                    },
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["uid", "username", "fullname", "id", "source", "title", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["messages", "minmark", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- node of a message -->
                    <attribute name="uid"> <!-- user identifier -->
                        <data type="positiveInteger" />
                    </attribute>
                    <attribute name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </attribute>
                    <attribute name="fullname"> <!-- full name of a user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </attribute>
                    <element name="id"> <!-- identifier of a message -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- source of a message -->
                        <element name="source"> <!-- message does not have any source -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- message has a source -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- title of a message -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- text of a message -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of message creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'messages' => array(
        array(
            'uid' => user_identifier,
            'username' => 'username',
            'fullname' => 'full name of the user',
            'id' => 'identifier of a message',
            'source' => 'source of a message',
            'title' => 'title of a message',
            'text' => 'text of a message',
            'time' => 'time of message creation',
        ),
        array(...),
        ...
    ),
    'minmark' => minimum.mark,
    'maxmark' => maximum.mark
)
```

** **

**```string / DOMDocument / bool appendPubMsgs(double $minMark, int $rpp = 20)```**
**Description**  
Returns a stripe with public messages from all users and following after the message with minimum mark.

**Parameters**  
*```minMark```* - minimum mark.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
In case of success returns a stripe with public messages from all users and following after the message with minimum mark as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```.  

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "uid": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": "string",
                        "maxLength": 100
                    },
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["uid", "username", "fullname", "id", "source", "title", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["messages", "minmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- node of a message -->
                    <attribute name="uid"> <!-- user identifier -->
                        <data type="positiveInteger" />
                    </attribute>
                    <attribute name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </attribute>
                    <attribute name="fullname"> <!-- full name of a user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </attribute>
                    <element name="id"> <!-- identifier of a message -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- source of a message -->
                        <element name="source"> <!-- message does not have any source -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- message has a source -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- title of a message -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- text of a message -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of message creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'messages' => array(
        array(
            'uid' => user_identifier,
            'username' => 'username',
            'fullname' => 'full name of the user',
            'id' => 'identifier of a message',
            'source' => 'source of a message',
            'title' => 'title of a message',
            'text' => 'text of a message',
            'time' => 'time of message creation',
        ),
        array(...),
        ...
    ),
    'minmark' => minimum.mark
```

** **

**```void outputFormat(string $output = 'xml')```**

**Description**  
Sets format of output data.

**Parameters**  
*```output```* - can get values *json*, or *xml*.

**[>>> Contents <<<](en.index)**