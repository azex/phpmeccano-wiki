![logo.svg](https://bitbucket.org/azexmail/phpmeccano/downloads/logo.svg)  
**[Выбрать язык](Home)**

**Содержание**

* [Фреймворк для веб-программирования phpMeccano (версия 0.2.0)](ru.phpmeccano)
* [Файл настроек conf.php](ru.conf)
* [Установка](ru.webinstaller)
* Модули ядра
    * [Модуль \_\_loader\_\_.php](ru.core.__loader__)
    * [Модуль swconst.php](ru.core.swconst)
    * [Модуль unifunctions.php](ru.core.unifunctions)
    * [Модуль extclass.php](ru.core.extclass)
    * [Модуль policy.php](ru.core.policy)
    * [Модуль logman.php](ru.core.logman)
    * [Модуль auth.php](ru.core.auth)
    * [Модуль userman.php](ru.core.userman)
    * [Модуль langman.php](ru.core.langman)
    * [Модуль files.php](ru.core.files)
    * [Модуль plugins.php](ru.core.plugins)
    * [Модуль discuss.php](ru.core.discuss)
    * [Модуль share.php](ru.core.share)
    * [Модуль maintenance.php](ru.core.maintenance)
* Установочный пакет плагина (спецификация версии 0.3)
    * [Структура пакета](ru.installation.package)
    * [Вспомогательные скрипты и описание процессов установки и удаления плагина](ru.inst.rm.script)
    * [Пояснение к структуре файлов titles.xml и texts.xml, а также установка и обновление текстов и заголовков на нескольких языках](ru.lang.install)
    * [Разъяснение приципа создания и хранения записей журнала событий](ru.log.install)