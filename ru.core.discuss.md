**[>>> Содержание <<<](ru.index)**

## Модуль discuss.php ##

Общее описание

Содержит класс ```Discuss```, предназначенный для для организации тем обсуждения и комментирования чего-либо.

### Методы класса ```Discuss``` *(class ```Discuss``` extends ```LogMan``)* ###

**```__construct(mysqli $dbLink)```**

**Описание**  
Устанавливает соединение модуля с базой данных.

**Принимаемые значения**  
*```dbLink```* - объект ```mysqli```.

** **

**```string / bool createTopic(string $topic = '')```**

**Описание**  
Создаёт тему для обсуждения или комментирования.

**Принимаемые значения**  
*```topic```* - название темы, длина до 100 символов.

**Возвращаемые значения**  
В случае успеха возвращает идентификатор созданной темы, иначе ```FALSE```.

** **

**```string / bool createComment(string $comment, int $userId, string $topicId, string $parentId = '')```**

**Описание**  
Создает комментарий в указанной теме.

**Принимаемые значения**  
*```comment```* - текст комментария, длина до 1024 символов.  
*```userId```* - идентификатор пользователя, от имени которого создается комментарий.  
*```topicId```* - идентификатор темы, в которой создаётся комментарий.  
*```parendId```* - идентификатор родительского комментария, если комментарий создаётся, как ответ на предшествующий комментарий.

**Возвращаемые значения**  
В случае успеха возвращает идентификатор созданного комментария, иначе ```FALSE```.

** **

**```string / DOMDocument / bool getAllComments(string $topicId)```**

**Описание**  
Получает комментарии к указанной теме.

**Принимаемые значения**  
*```topicId```* - идентификатор темы.  

**Возвращаемые значения**
В случае успеха возвращает комментарии к теме в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "topic": {
            "type": ["string", "null"]
        },
        "tid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["comments", "topic", "tid", "minmark", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="topic"> <!-- название темы -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="tid"> <!-- идентификатор темы -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- блок комментария -->
                    <element name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- полное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- идентификатор комментария -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- идентификатор родительского комментария -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- текст комментария -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания комментария -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'topic' => 'название темы',
    'tid' => 'идентификатор темы',
    'minmark' => минимальная_метка,
    'maxmark' => максимальная_метка,
    'comments' => array(
        array(
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'cid' => 'идентификатор комментария',
            'pcid' => 'идентификатор родительского комментария',
            'text' => 'текст комментария',
            'time' => 'время создания комментария'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool getComments(string $topicId, int $rpp = 20)```**

**Описание**  
Получает комментарии к указанной теме.

**Принимаемые значения**  
*```topicId```* - идентификатор темы.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**
В случае успеха возвращает комментарии к теме в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "topic": {
            "type": ["string", "null"]
        },
        "tid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["comments", "topic", "tid", "minmark", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="topic"> <!-- название темы -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="tid"> <!-- идентификатор темы -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- блок комментария -->
                    <element name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- полное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- идентификатор комментария -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- идентификатор родительского комментария -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- текст комментария -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания комментария -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'topic' => 'название темы',
    'tid' => 'идентификатор темы',
    'minmark' => минимальная_метка,
    'maxmark' => максимальная_метка,
    'comments' => array(
        array(
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'cid' => 'идентификатор комментария',
            'pcid' => 'идентификатор родительского комментария',
            'text' => 'текст комментария',
            'time' => 'время создания комментария'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool appendComments(string $topicId, double $minMark, int $rpp = 20)```**

**Описание**  
Получает комментарии к указанной теме, следующие за комментарием с минимальной меткой.

**Принимаемые значения**  
*```topicId```* - идентификатор темы.  
*```minMark```* - минимальная метка.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
В случае успеха возвращает комментарии к теме в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["comments", "minmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- блок комментария -->
                    <element name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- полное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- идентификатор комментария -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- идентификатор родительского комментария -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- текст комментария -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания комментария -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'topic' => 'название темы',
    'tid' => 'идентификатор темы',
    'minmark' => минимальная_метка,,
    'comments' => array(
        array(
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'cid' => 'идентификатор комментария',
            'pcid' => 'идентификатор родительского комментария',
            'text' => 'текст комментария',
            'time' => 'время создания комментария'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool updateComments(string $topicId, double $maxMark)```**

**Описание**  
Получает все комментарии к указанной теме, идущие перед комментарием с максимальной меткой.

**Принимаемые значения**  
*```topicId```* - идентификатор темы.  
*```maxMark```* - максимальная метка.

**Возвращаемые значения**  
В случае успеха возвращает комментарии к теме в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["comments", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- блок комментария -->
                    <element name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- полное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- идентификатор комментария -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- идентификатор родительского комментария -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- текст комментария -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания комментария -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'topic' => 'название темы',
    'tid' => 'идентификатор темы',
    'maxmark' => максимальная_метка,
    'comments' => array(
        array(
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'cid' => 'идентификатор комментария',
            'pcid' => 'идентификатор родительского комментария',
            'text' => 'текст комментария',
            'time' => 'время создания комментария'
        ),
        array(...),
        ...
    )
)
```

** **

**```bool editComment(string $comment, string $commentId, int $userId)```**

**Описание**  
Редактирует комментарий.

**Принимаемые значения**  
*```comment```* - текст комментария, длина до 1024 символов.  
*```commentId```* - идентификатор комментария.  
*```userId```* - идентификатор пользователя, которому принадлежит редактируемый комментарий.

**Возвращаемые значения**
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```string / DOMDocument / bool getComment(string $commentId, int $userId)```**

**Описание**  
Получает информацию о комментарии.

**Принимаемые значения**  
*```commentId```* - идентификатор комментария.  
*```userId```* - идентификатор пользователя, которому принадлежит комментарий.

**Возвращаемые значения**  
В случае успеха возвращает информацию о комментарии в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "uid": {
            "type": "integer"
        },
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "fullname": {
            "type": ["string", "null"],
            "maxLength": 100
        },
        "cid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "text": {
            "type": "string",
            "minLength": 1,
            "maxLength": 1024
        }
    },
    "required": ["uid", "username", "fullname", "cid", "text"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comment">
            <element name="uid"> <!-- идентификатор пользователя -->
                <data type="positiveInteger" />
            </element>
            <element name="username"> <!-- имя пользователя -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </element>
            <element name="fullname"> <!-- полное имя пользователя -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="cid"> <!-- идентификатор комментария -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </element>
            <element name="text"> <!-- текст комментария -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">1024</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'uid' => 'идентификатор пользователя', 
    'username' => 'имя пользователя', 
    'fullname' => 'полное имя пользователя', 
    'cid' => 'идентификатор комментария', 
    'text' => 'текст комментария' 
)
```

** **

**```bool eraseComment(string $commentId, int $userId)```**

**Описание**  
Затирает комментарий.

**Принимаемые значения**  
*```commentId```* - идентификатор комментария.  
*```userId```* - идентификатор пользователя, которому принадлежит комментарий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```void outputFormat(string $output = 'xml')```**

**Описание**  
Устанавливает формат выводимых данных.

**Принимаемые значения**  
*```output```* - может принимать значения *json*, или *xml*.

**[>>> Содержание <<<](ru.index)**