**[>>> Contents <<<](en.index)**

## Clarification of the schema of files *titles.xml* и *texts.xml* and installation and update of texts and titles at several languages ##

Framework phpMeccano gives abilities to create multilingual interface with the help of texts and titles (further generalized record), which are stored in various languages and installed from files *titles.xml* and *texts.xml* from the installation package. Storage schema looks like this:

***plug-in => section => record name => records in various languages***

Sections of records can be of two types: static and non-static. The difference lies in that the static section cannot be removed, also its content cannot be changed. Static sections can be deleted or changed only by removing, downgrading or reinstallation of the plug-in.

Opposite, non-static sections can be removed and them content can be changed. Also new non-static sections can be created for already installed plug-ins without upgrade of the plug-in.

In addition, it is needed to take in account that the language which is used to create a record must exist in the registry of the framework. Exactly similarly, while installation of the records from files *titles.xml* and *texts.xml* only records in languages which exist in the registry will be installed.

Changing of a type of the section from static to non-static and back is only available by installation of records from files *titles.xml* and *texts.xml* during upgrade of the plug-in. It is needed to take in account that while changing type from static to non-static content of the section will be erased and replaced by new content.

Also names of the sections can be renamed while upgrade of the plug-in. This can be made with the help of the attributes *```name```* and *```oldname```* of the node *```section```*.

While upgrade of the records any sections not defined in files *titles.xml* and *text.xml* will be removed together with all records. Thats why if there any non-static sections created by anyone, they have to be added manually to files *titles.xml* and *texts.xml* of the plug-in package before starting installation of the package.

**[>>> Contents <<<](en.index)**