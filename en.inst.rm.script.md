**[>>> Contents <<<](en.index)**

## Auxiliary scripts and description of processes of installation and deletion of the plugin ##

Auxiliary scripts *inst.php* and *rm.php* are loaded and executed automatically while operations of installation (reinstallation, update, downgrade) and removing of plug-ins. Loading of script inst.php is made by call of method ```Plugins::install```, but *rm.php* by call of method ```Plugins::delInstalled```.

### Sequence of actions when method ```Plugins::install``` is called with valid parameters and if all needed dependencies are satisfied ###

After validation of the structure of the installation package, in case if a package is being installed, then a plug-in registry gets rewritten and the plug-in receives an identifier. Then a script inst.php from installation package is loaded and object ```Install(\mysqli $dblink, int $id, string $version = '', bool $reset = FALSE)``` is created.

**Parameters**  
*```dblink```* - object ```mysqli```. Value of this private variable is assigned to variable ```dlink``` of the created object.  
*```id```* - plug-in identifier. Value of this argument is assigned to variable ```id``` of the created object.  
*```version```* - version of the plug-in. If the plug-in is reinstalled, updated or downgraded, then this argument is assigned to version of existent plug-in. In case of installation of a new plug-in this argument is assigned to empty string. Value of this argument is assigned to variable version of the created object.  
*```reset```* - a flag that indicates whether needed to reset data related with the plug-in. The value ```TRUE``` indicates to reset. The value ```FALSE``` indicates not to reset. Value of this private variable is assigned to variable reset of the created object ```Install```.

Then method ```Install::preinst()``` is called, it has to make pre-setting to install the plug-in, if it is necessary. If method ```Install::preinst()``` returns ```FALSE```, then execution of method ```Plugins::install``` is interrupted.  
If it returns ```TRUE```, then group policies of access, log events, texts and titles are installed from files *policy.xml*, *log.xml*, *texts.xml* and *titles.xml*, respectively.  
Then content of folders *documents*, *php* and *js* is copied to ```MECCANO_DOCUMENTS_DIR.</plugin_name>```, ```MECCANO_PHP_DIR.</plugin_name>``` and ```MECCANO_JS_DIR.</plugin_name>```, respectively. Also the script *rm.php* is copied to ```MECCANO_UNINSTALL.</plugin_name.php>```.
Then method ```Install::postinst()``` is called, it has to make post-setting to install the plug-in, if it is necessary. If method ```Install::postinst()``` returns ```FALSE```, then execution of method ```Plugins::install``` is interrupted. If it returns ```TRUE```, then execution of method ```Plugins::install``` is finished succeeded.

### Sequence of actions when method ```Plugins::delInstalled``` is called with valid parameters ###

Script ```MECCANO_UNINSTALL.</plugin_name.php>``` is loaded and object ```Remove(\mysqli $dblink, $id, $keepData = TRUE)``` is created.

**Parameters**  
*```dblink```* - object ```mysqli```. Value of this private variable is assigned to variable dlink of the created object.  
*```id```* - plug-in identifier. Value of this argument is assigned to variable id of the created object.  
*```keepData```* - a flag that indicates whether needed to keep and not to delete all data related with the plug-in. The value ```TRUE``` indicates to keep data. The value ```FALSE``` indicates not to keep data. Value of this private variable is assigned to variable ```keepData``` of the created object ```Remove```.

Then method ```Remove::prerm()``` is called, it has to make pre-setting to remove the plug-in, if it is necessary. If method ```Remove::prerm()``` returns ```FALSE```, then execution of method ```Plugins::delInstalled``` is interrupted.  
If it returns ```TRUE```, then group policies of access, log events, texts and titles related with plug-in are removed from database.
Then method ```Remove::postrm()``` is called, it has to make post-setting to remove the plug-in, if it is necessary. If method ```Remove::postrm()``` returns ```FALSE```, then execution of method ```Plugins::delInstalled``` is interrupted.

If it returns ```TRUE```, then files and folders related with the plug-in and placed by paths ```MECCANO_DOCUMENTS_DIR.</plugin_name>```, ```MECCANO_PHP_DIR.</plugin_name>```, ```MECCANO_JS_DIR.</plugin_name>```, ```MECCANO_DOCUMENTS_DIR.</plugin_name>``` and ```MECCANO_UNINSTALL.</plugin_name.php>``` are removed. After this execution of method ```Plugins::delInstalled``` is finished succeeded.

**[>>> Contents <<<](en.index)**