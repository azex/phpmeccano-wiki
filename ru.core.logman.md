**[>>> Содержание <<<](ru.index)**

## Модуль logman.php ##

### Общее описание ###

Содержит класс ```LogMan```, предназначенный для ведения журнала событий и управления его записями.

### Методы класса ```LogMan``` *(class ```LogMan``` extends ```Policy```)* ###

**```__construct(mysqli $dbLink)```**

**Описание**  
Устанавливает соединение модуля с базой данных и контролем доступа.

**Принимаемые значения**  
*```dbLink```* - объект ```mysqli```.

** **

**bool installEvents(\DOMDocument $events, bool $validate = TRUE)**  
**Описание**  
Устанавливает набор событий плагина.

**Принимаемые значения**  
*```events```* - объект ```DOMDocument```, содержащий набор событий плагина и их описание на разных языках.  
Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="log">
            <attribute name="plugin"> <!-- короткое имя плагина -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="event"> <!-- событие -->
                    <attribute name="keyword"> <!-- ключевое слово события -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                        </data>
                    </attribute>
                    <oneOrMore>
                        <element name="desc"> <!-- описание события -->
                            <attribute name="code"> <!-- код языка описания события -->
                                <data type="string">
                                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                </data>
                            </attribute>
                            <data type="string">
                                <param name="maxLength">128</param>
                            </data>
                        </element>
                    </oneOrMore>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

*```validate```* - при значении ```TRUE``` производится предварительная проверка структуры DOM. При значении ```FALSE``` проверка не проводится.

**Возвращаемые значения**  
В случае успешной установки возвращает ```TRUE```, иначе ```FALSE```.

** **

**```delLogEvents(string $plugin)```**

**Описание**  
Удаляет все записи и события плагина.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону ```[a-zA-Z\d_]{3,30}```.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool clearLog()```**

**Описание**  
Очищает журнал событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```array sumLogAllPlugins(int $rpp = 20)```**

**Описание**  
Возвращает обобщенные данные о журнале для вывода данных по всем плагинам постранично.

**Принимаемые значения**  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
Массив вида ```array('records' => int $totalRecs, 'pages' => int $totalPages)```, где ```totalRecs``` - общее количество записей в журнале, ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalRecs/rpp```. В случае неудачи возвращает ```FALSE```.

** **

**```DOMDocument getPageAllPlugins(int $pageNumber, int $totalPages, int $rpp = 20, string $code = MECCANO_DEF_LANG, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает страницу из журнала событий по всем плагинам.

**Принимаемые значения**  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalPages```* - общее количество страниц.  
*```rpp```* - желаемое максимальное количество записей на странице.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - параметры сортировки вывода записей журнала событий. Допустимые значения параметров: ```'id'```, ```'user'```, ```'event'``` и ```'time'```. Последовательность параметров задаётся как массив, например, ```array('event', 'user')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает страницу из журнала событий в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            },
            "event": {
                "type": "string"
            },
            "user": {
                "type": "string",
                "pattern": "^([a-zA-Z0-9_]{3,20}|M)$"
            }
        },
        "required": ["id", "time", "event", "user"]
    }
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="log">
            <zeroOrMore>
                <element name="record"> <!-- информация о событии -->
                    <element name="id"> <!-- идентификатор записи -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- дата и время записи события -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="event"> <!-- описание события -->
                        <data type="string" />
                    </element>
                    <element name="user"> <!-- имя пользователя, от имени которого была совершена запись; если M - неавторизованный пользователь -->
                        <data type="string">
                            <param name="pattern">([a-zA-Z0-9_]{3,20}|M)</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    array(
        'id' => идентификатор_записи,
        'time' => 'дата и время записи события',
        'event' => 'описание события',
        'user' => 'имя пользователя, от имени которого была совершена запись; если M - неавторизованный пользователь'
        ),
    array(...),
    ...
)
```

** **

**```DOMDocument getLogAllPlugins(string $code = MECCANO_DEF_LANG, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает все записи журнала событий по всем плагинам.

**Принимаемые значения**  
*```code``` - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - параметры сортировки вывода записей журнала событий. Допустимые значения параметров: ```'id'```, ```'user'```, ```'event'``` и ```'time'```. Последовательность параметров задаётся как массив, например, ```array('event', 'user')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает все записи журнала событий в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            },
            "event": {
                "type": "string"
            },
            "user": {
                "type": "string",
                "pattern": "^([a-zA-Z0-9_]{3,20}|M)$"
            }
        },
        "required": ["id", "time", "event", "user"]
    }
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="log">
            <zeroOrMore>
                <element name="record"> <!-- информация о событии -->
                    <element name="id"> <!-- идентификатор записи -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- дата и время записи события -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="event"> <!-- описание события -->
                        <data type="string" />
                    </element>
                    <element name="user"> <!-- имя пользователя, от имени которого была совершена запись; если M - неавторизованный пользователь -->
                        <data type="string">
                            <param name="pattern">([a-zA-Z0-9_]{3,20}|M)</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    array(
        'id' => идентификатор_записи,
        'time' => 'дата и время записи события',
        'event' => 'описание события',
        'user' => 'имя пользователя, от имени которого была совершена запись; если M - неавторизованный пользователь'
        ),
    array(...),
    ...
)
```

** **

**```array sumLogByPlugin(string $plugin, int $rpp = 20)```**

**Описание**  
Возвращает обобщенные данные о журнале для вывода данных по заданному плагину постранично.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
Массив вида ```array('records' => int $totalRecs, 'pages' => int $totalPages)```, где ```totalRecs``` - общее количество записей в журнале, ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalRecs/rpp```. В случае неудачи возвращает ```FALSE```.

** **

**```DOMDocument getPageByPlugin(string $plugin, int $pageNumber, int $totalPages, int $rpp = 20, string $code = MECCANO_DEF_LANG, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает страницу из журнала событий по заданному плагину.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону ```[a-zA-Z\d_]{3,30}```.  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalPages```* - общее количество страниц.  
*```rpp```* - желаемое максимальное количество записей на странице.  
*```code```* - код языка, соответствующий шаблону [a-z]{2}-[A-Z]{2}.  
*```orderBy```* - параметры сортировки вывода записей журнала событий. Допустимые значения параметров: ```'id'```, ```'user'```, ```'event'``` и ```'time'```. Последовательность параметров задаётся как массив, например, ```array('event', 'user')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает cтраницу журнала в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugin": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "records": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    },
                    "event": {
                        "type": "string"
                    },
                    "user": {
                        "type": "string",
                        "pattern": "^([a-zA-Z0-9_]{3,20}|M)$"
                    }
                },
                "required": ["id", "time", "event", "user"]
            }
        }
    },
    "required": ["plugin", "records"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="log">
            <attribute name="plugin"> <!-- имя плагина -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="record"> <!-- информация о событии -->
                    <element name="id"> <!-- идентификатор записи -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- дата и время записи события -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="event"> <!-- описание события -->
                        <data type="string" />
                    </element>
                    <element name="user"> <!-- имя пользователя, от имени которого была совершена запись; если M - неавторизованный пользователь -->
                        <data type="string">
                            <param name="pattern">([a-zA-Z0-9_]{3,20}|M)</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'plugin' => 'имя плагина',
    'records' => array(
        array(
            'id' => идентификатор_записи,
            'time' => 'дата и время записи события',
            'event' => 'описание события',
            'user' => 'имя пользователя, от имени которого была совершена запись; если M - неавторизованный пользователь'
        ),
        array(...),
        ...
    )
)
```

** **

**```DOMDocument getLogByPlugin(string $plugin, string $code = MECCANO_DEF_LANG, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает все записи журнала событий по заданному плагину.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - параметры сортировки вывода записей журнала событий. Допустимые значения параметров: ```'id'```, ```'user'```, ```'event'``` и ```'time'```. Последовательность параметров задаётся как массив, например, ```array('event', 'user')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха все записи журнала событий в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugin": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "records": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    },
                    "event": {
                        "type": "string"
                    },
                    "user": {
                        "type": "string",
                        "pattern": "^([a-zA-Z0-9_]{3,20}|M)$"
                    }
                },
                "required": ["id", "time", "event", "user"]
            }
        }
    },
    "required": ["plugin", "records"]
}
```

Журнал в виде объекта ```DOMDocument```. Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="log">
            <attribute name="plugin"> <!-- имя плагина -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="record"> <!-- информация о событии -->
                    <element name="id"> <!-- идентификатор записи -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- дата и время записи события -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="event"> <!-- описание события -->
                        <data type="string" />
                    </element>
                    <element name="user"> <!-- имя пользователя, от имени которого была совершена запись; если M - неавторизованный пользователь -->
                        <data type="string">
                            <param name="pattern">([a-zA-Z0-9_]{3,20}|M)</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'plugin' => 'имя плагина',
    'records' => array(
        array(
            'id' => идентификатор_записи,
            'time' => 'дата и время записи события',
            'event' => 'описание события',
            'user' => 'имя пользователя, от имени которого была совершена запись; если M - неавторизованный пользователь'
        ),
        array(...),
        ...
    )
)
```

** **

**```void outputFormat(string $output = 'xml')```**

**Описание**  
Устанавливает формат выводимых данных.

**Принимаемые значения**  
*```output```* - может принимать значения *json*, или *xml*.

**[>>> Содержание <<<](ru.index)**