**[>>> Contents <<<](en.index)**

## Module unifunctions.php ##

### General description ###

Module includes general service functions which are used by most of the core modules.


### Functions ###

**```string makeSalt(string $prefix = '')```**

**Description**  
Generates unique salt for password.

**Parameters**  
*```prefix```* - additional prefix.

**Return values**  
A string of 22 characters length.

** **

**```string makeIdent(string $prefix = '')```**

**Description**  
Generates unique identifier.

**Parameters**  
*```prefix```* - additional prefix.

**Return values**  
A string of 40 characters length.

** **

**```string passwHash(string $password, string $salt)```**

**Description**  
Encrypts a password.

**Parameters**  
*```password```* - password, a string of 8-50 characters length.  
*```salt```* - password salt, a string of 22 characters length.

**Return values**  
Returns a string of 40 characters length in case of success. Otherwise returns ```FALSE```.

** **

**```bool pregDbName(string $dbname)```**

**Description**  
Checks the database name on compliance with the established pattern.

**Parameters**  
*```dbname```* - database name, a string of 1-64 characters length. Numbers, underline, English letters of lower and upper register are allowed.

**Return values**  
Returns ```TRUE``` in case of match. Otherwise returns ```FALSE```.

** **

**```bool pregPref(string $prefix)```**

**Description**  
Checks prefix of the database tables on compliance with the established pattern.

**Parameters**  
*```prefix```* - table prefix, a string of 1-10 characters length. Numbers and English letters of lower and upper register are allowed.

**Return values**  
Returns ```TRUE``` in case of match. Otherwise returns ```FALSE```.

** **

**```bool pregUName(string $username)```**

**Description**  
Checks username on compliance with the established pattern.

**Parameters**  
*```username```* - username, a string of 3-20 characters length. Allowed letters of English ABC upper and lower case, numbers and underline symbol.

**Return values**  
Returns ```TRUE``` in case of match. Otherwise returns ```FALSE```.

** **

**```bool pregGName(string $groupname)```**

**Description**  
Checks group name on compliance with the established pattern.

**Parameters**  
*```groupname```* - name of user group, a string of 1-50 characters length.

**Return values**  
Returns ```TRUE``` in case of match. Otherwise returns ```FALSE```.

** **

**```bool pregPassw(string $password)```**

**Description**  
Checks password on compliance with the established pattern.

**Parameters**  
*```password```* - password, a string of 8-50 characters length. Allowed letters of English ABC upper and lower case, numbers and special characters.

**Return values**  
Returns ```TRUE``` in case of match. Otherwise returns ```FALSE```.

** **

**```bool pregIdent(string $ident)```**

**Description**  
Checks identifier on compliance with the established pattern.

**Parameters**  
*```ident```* - identifier, a string of 40 characters length. Allowed letters of English ABC upper and lower case, and numbers

**Return values**  
Returns ```TRUE``` in case of match. Otherwise returns ```FALSE```.

** **

**```bool pregMail(string $email)```**

**Description**  
Checks correctness of the e-mail format.

**Parameters**  
*```email```* - e-mail address.

**Return values**  
Returns ```TRUE``` in case of match. Otherwise returns ```FALSE```.

** **

**```string openRead(string $path)```**

**Description**  
Reads contents of defined file

**Parameters**  
*```path```* - path to local file.

**Return values**  
Returns file contents as a string in case of match. Otherwise returns ```FALSE```.

** **

**```DOMDocument xmlTransform(string $xml, string $xsl)```**

**Description**  
Make transformation of xml-document by defined xslt-style.

**Parameters**  
*```xml```* - xml-document that is being transformed.  
*```xsl```* - xslt-style.

**Return values**  
Returns object ```DOMDocument``` in case of success. Otherwise returns ```FALSE```.

** **

**```int authUserId()```**

**Description**  
Returns user identifier if active session exists.

**Return values**  
User identifier as integer. Returns ```FALSE``` if active session does not exist.

** **

**```string authUName()```**

**Description**  
Returns username if active session exists.

**Return values**  
Username as string. Returns ```FALSE``` if active session does not exist.

** **

**```int authLimited()```**

**Description**  
Returns session type if active session exists. Session can be limited and unlimited.

**Return values**  
Session type as integer. Returns ```FALSE``` if active session does not exist.

** **

**```string authLang()```**

**Description**  
Returns language code which is set in the user settings if active session exists.

**Return values**  
Language code which is complied to pattern *[a-z]{2}-[A-Z]{2}*. Returns ```FALSE``` if active session does not exist.

** **

**```string authLangDir()```**

**Description**  
Returns direction of the text display that is set in the user settings if active session exists.

**Return values**  
```'ltr'``` or ```'rtl'```. Returns ```FALSE``` if active session does not exist.

** **

**```bool compareVersions(string $version1, string $version2, string $operator = ">=")```**

**Description**  
Compares two versions by comparison of two summary versions. Summary version is calculated by formula *10000 * ```uv``` + 100 * ```mv``` + ```lv```*, where ```uv``` - upper/main version; ```mv``` - medium/intermediate version; ```lv``` - lower/minor version. For example, the summary version of version *2.8.5* is equal to *10000 * 2 + 100 * 8 + 5 = 20805*.

**Parameters**  
*```version1```* - the first being compared version. Matches pattern *[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}*.  
*```version2```* - the second being compared version. Matches pattern *[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}*.  
*```operator```* - the comparison operator. The next operators are allowed: *>=*, *<=*, *==*, *>* and *<*.

**Return values**  
Returns ```TRUE``` if comparison is true, otherwise returns ```FALSE```.

** **

**```int calcSumVersion(string $version)```**
**Description**  
Calculates summary version of plug-in.

**Parameters**  
*```version```* - plug-in version, matches pattern *[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}*. For example, 2.8.5.

**Return values**  
Summary version is calculated by formula *10000 * ```uv``` + 100 * ```mv``` + ```lv```*, where ```uv``` - upper/main version; ```mv``` - medium/intermediate version; ```lv``` - lower/minor version. For example, the summary version of version *2.8.5* is equal to *10000 * 2 + 100 * 8 + 5 = 20805*.

** **

**```string genPassword(int $length = 8, bool $lower = TRUE, bool $upper = TRUE, bool $numbers = TRUE, bool $underline = TRUE, bool $minus = TRUE, bool $special = TRUE)```**

**Description**  
Password generator.

**Parameters**  
*```length```* - desired length of the password. Minimum value - 8, maximum value - 50.  
*```lower```* - a flag of the usage of lower register letters within password. If ```TRUE``` - to use, if ```FALSE``` - not to use.  
*```upper```* - a flag of the usage of upper register letters within password. If ```TRUE``` - to use, if ```FALSE``` - not to use.  
*```numbers```* - a flag of the usage of numbers within password. If ```TRUE``` - to use, if ```FALSE``` - not to use.  
*```underline```* - a flag of the usage of underline within password. If ```TRUE``` - to use, if ```FALSE``` - not to use.  
*```minus```* - a flag of the usage of minus within password. If ```TRUE``` - to use, if ```FALSE``` - not to use.  
*```special```* - a flag of the usage of special characters within password. If ```TRUE``` - to use, if ```FALSE``` - not to use.

**Return values**  
Generated password. Returns ```FALSE``` in case of invalid parameters.

** **

**```string guid()```**

**Description**  
Generates globally unique identifier.

**Return values**  
A string of format *XXXXXXXX-XXXX-4XXX-XXXX-XXXXXXXXXXXX*, where *X* - an English letter of lower register from *A* to *E*, or integer from *0* to *9*.

** **

**```bool pregGuid(string $guid)```**

**Description**  
Checks the format of globally unique identifier.

**Parameters**  
*```guid```* - a string of format *XXXXXXXX-XXXX-4XXX-XXXX-XXXXXXXXXXXX*, where *X* - an English letter of lower register from *A* to *E*, or integer from *0* to *9*.

**[>>> Contents <<<](en.index)**