**[>>> Содержание <<<](ru.index)**

## Модуль plugins.php ##

### Общее описание ###

Содержит класс ```Plugins```, предназначенный для установки, удаления и обновления плагинов фреймворка.

### Методы класса ```Plugins``` *(class ```Plugins``` extends ```LangMan```)* ###

**```__construct(\mysqli $dbLink)```**

**Описание**  
Устанавливает связь с базой данных.

**Принимаемые значения**  
*```dbLink```* - объект ```mysqli```.

** **

**```string unpack(string $package)```**

**Описание**  
Производит предварительную распаковку плагина в промежуточную директорию.

**Принимаемые значения**  
*```package```* - путь до пакета с плагином.

**Возвращаемые значения**  
В случае успеха возвращает короткое имя распакованного плагина, иначе ```FALSE```.

** **

**```bool delUnpacked(string $plugin)```**

**Описание**  
Удаляет распакованный пакет плагина.

**Принимаемые значения**  
*```plugin```* - короткое имя распакованного плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```DOMDocument listUnpacked()```**

**Описание**  
Возвращает список придварительно распакованных пакетов.

**Возвращаемые значения**  
В случае успеха возвращает список предварительно распакованных пакетов в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "short": {
                "type": "string",
                "pattern": "^[a-zA-Z0-9_]{3,30}$"
            },
            "full": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "version": {
                "type": "string",
                "pattern": "^[0-9]{1,2}\\.[0-9]{1,2}\\.[0-9]{1,2}$"
            },
            "action": {
                "type": "string",
                "pattern": "^(install|reinstall|upgrade|downgrade)$"
            }
        },
        "required": ["short", "full", "version", "action"]
    }
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="unpacked">
            <zeroOrMore>
                <element name="plugin"> <!-- блок плагина -->
                    <element name="short"> <!-- имя плагина -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                        </data>
                    </element>
                    <element name="full"> <!-- полное название плагина -->
                        <data type="string">
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="version"> <!-- версия плагина -->
                        <data type="string">
                            <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}</param>
                        </data>
                    </element>
                    <element name="action"> <!-- действие, которое можно совершить -->
                        <choice>
                            <value>install</value>
                            <value>reinstall</value>
                            <value>upgrade</value>
                            <value>downgrade</value>
                        </choice>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    array(
        'short' => 'имя плагина',
        'full' => 'полное название плагина',
        'version' => 'версия плагина',
        'action' => 'действие, которое можно совершить'
    ),
    array(...),
    ...
)
```

** **

**```DOMDocument aboutUnpacked(string $plugin)```**

**Описание**  
Возвращает информацию о распакованном пакете.

**Принимаемые значения**  
*```plugin```* - имя распакованного плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.

**Возвращаемые значения**  
В случае успеха возвращает информацию о распакованном пакете в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "short": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "full": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "version": {
            "type": "string",
            "pattern": "^[0-9]{1,2}\\.[0-9]{1,2}\\.[0-9]{1,2}$"
        },
        "about": {
            "type": "string",
            "maxLength": 65535
        },
        "credits": {
            "type": "string",
            "maxLength": 65535
        },
        "url": {
            "type": "string",
            "maxLength": 100
        },
        "email": {
            "type": "string",
            "maxLength": 100
        },
        "license": {
            "type": "string",
            "maxLength": 65535
        },
        "depends": {
            "type": "string",
            "maxLength": 65535
        },
        "action": {
            "type": "string",
            "pattern": "^(install|reinstall|upgrade|downgrade)$"
        }
    },
    "required": ["short", "full", "version", "about", "credits", "url", "email", "license", "depends", "action"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="unpacked">
            <element name="short"> <!-- имя плагина -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </element>
            <element name="full"> <!-- полное название плагина -->
                <data type="string">
                    <param name="maxLength">50</param>
                </data>
            </element>
            <element name="version"> <!-- версия плагина -->
                <data type="string">
                    <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}</param>
                </data>
            </element>
            <element name="about"> <!-- описание плагина -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="credits"> <!-- перечисление разработчиков плагина -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="url"> <!-- веб сайт плагина -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="email"> <!-- контактный адрес электронной почты -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="license"> <!-- лицензия, использумая для плагина -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="depends"> <!-- перечисление зависимостей, требуемых для плагина -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="action"> <!-- действие, которое можно совершить -->
                <choice>
                    <value>install</value>
                    <value>reinstall</value>
                    <value>upgrade</value>
                    <value>downgrade</value>
                </choice>
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'short' => 'имя плагина',
    'full' => 'полное название плагина',
    'version' => 'версия плагина',
    'about' => 'орисание плагина',
    'credits' => 'разработчики плагина',
    'url' => 'веб сайт плагина',
    'email' => 'контактный адрес электронной почты',
    'license' => 'лицензия, использумая для плагина',
    'depends' => 'зависимости, требуемые для плагина',
    'action' => 'действие, которое можно совершить'
)
```

** **

**```int pluginData(string $plugin)```**

**Описание**  
Возвращает идентификатор и версию установленного плагина.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.

**Возвращаемые значения**  
Возращает массив вида ```array("id" => (int) $id, "version" => $version)```, где ```id``` - числовой идентификатор плагина, а ```version``` - версия плагина.

** **

**```bool install(string $plugin bool $reset = FALSE, bool $log = TRUE)```**

**Описание**  
Производит установку распакованного плагина.

**Принимаемые значения**  
*```plugin```* - имя распакованного плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```reset```* - флаг, который указывает установочному скрипту пакета, что все данные, относящиеся к плагину, необходимо сбростить на изначальные. Флаг имеет смысл использовать при переустановке или обновлении плагина.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool delInstalled(int $id, bool $keepData = TRUE, bool $log = TRUE)```**

**Описание**  
Улаляет установленный плагин.

**Принимаемые значения**  
*```id```* - числовой идентификатор распакованного плагина.  
*```keepData```* - флаг, который сообщает удаляющему скрипту плагина, что все данные, связанные с плагином, необходимо оставить и не удалять. Также при значении ```TRUE``` не удаляются файлы плагина, хранящиеся в директории ```MECCANO_DOCUMENTS_DIR```. При значении ```FALSE``` эти файлы будут удалены. Записи журнала событий, политики доступа, тексты и заголовки на всех доступных языках, которые относятся к плагину, будут удалены в любом случае.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```DOMDocument listInstalled()```**

**Описание**  
Возвращает список всех установленных плагинов.

**Возвращаемые значения**  
В случае успеха возвращает список всех установленных плагинов в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "short": {
                "type": "string",
                "pattern": "^[a-zA-Z0-9_]{3,30}$"
            },
            "full": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "version": {
                "type": "string",
                "pattern": "^[0-9]{1,2}\\.[0-9]{1,2}\\.[0-9]{1,2}$"
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            }
        },
        "required": ["short", "full", "version", "time"]
    }
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="installed">
            <zeroOrMore>
                <element name="plugin"> <!-- блок плагина -->
                    <element name="short"> <!-- имя плагина -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                        </data>
                    </element>
                    <element name="full"> <!-- полное название плагина -->
                        <data type="string">
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="version"> <!-- версия плагина -->
                        <data type="string">
                            <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время установки плагина -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    array(
        "short" => 'имя плагина',
        "full" => 'полное название плагина',
        "version" => 'версия плагина',
        "time" => 'время установки плагина'
    ),
    array(...),
    ...
)
```

** **

**```DOMDocument aboutInstalled(string $plugin)```**

**Описание**  
Возвращает информацию об установленном плагине.

**Принимаемые значения**  
*```plugin```* - короткое имя установленного плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.

**Возвращаемые значения**  
В случае успеха возвращает информацию об установленном плагине в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "short": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "full": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "version": {
            "type": "string",
            "pattern": "^[0-9]{1,2}\\.[0-9]{1,2}\\.[0-9]{1,2}$"
        },
        "time": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "about": {
            "type": "string",
            "maxLength": 65535
        },
        "credits": {
            "type": "string",
            "maxLength": 65535
        },
        "url": {
            "type": "string",
            "maxLength": 100
        },
        "email": {
            "type": "string",
            "max:ength": 100
        },
        "license": {
            "type": "string",
            "maxLength": 65535
        }
    },
    "required": ["short", "full", "version", "time", "about", "credits", "url", "email", "license"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="installed">
            <element name="short"> <!-- имя плагина -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </element>
            <element name="full"> <!-- полное название плагина -->
                <data type="string">
                    <param name="maxLength">50</param>
                </data>
            </element>
            <element name="version"> <!-- версия плагина -->
                <data type="string">
                    <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}</param>
                </data>
            </element>
            <element name="time"> <!-- время установки плагина -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="about"> <!-- описание плагина -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="credits"> <!-- перечисление разработчиков плагина -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="url"> <!-- веб сайт плагина -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="email"> <!-- контактный адрес электронной почты -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="license"> <!-- лицензия, использумая для плагина -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    "short" => 'имя плагина',
    "full" => 'полное название плагина',
    "version" => 'версия плагина',
    "time" => 'время установки плагина',
    "about" => 'описание плагина',
    "credits" => 'перечисление разработчиков плагина',
    "url" => 'веб сайт плагина',
    "email" => 'контактный адрес электронной почты',
    "license" => 'лицензия, использумая для плагина'
)
```

** **

**```void outputFormat(string $output = 'xml')```**

**Описание**  
Устанавливает формат выводимых данных.

**Принимаемые значения**  
*```output```* - может принимать значения *json*, или *xml*.

**[>>> Содержание <<<](ru.index)**