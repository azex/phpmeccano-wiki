**[>>> Contents <<<](en.index)**

## Module policy.php ##

### General description ###

Includes class ```Policy``` that allows to control access of user groups to functions.

### Methods of class ```Policy``` *(class ```Policy``` extends ```ServiceMethods```)* ###

**```__construct(mysqli $dbLink)```**

**Description**  
Sets a connection to database.

**Parameters**  
*```dbLink```* - object ```mysqli```.

** **

**```bool installPolicy(DOMDocument $policy, bool $validate = TRUE)```**

**Description**  
Installs access policies of the plugin and their descriptions.

**Parameters**  
*```policy```* - object ```DOMDocument```, includes plugin name, a list of it's policies and description of the policies.  
XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="policy">
            <attribute name="plugin"> <!-- short plugin name -->
                <ref name="plugFuncName" />
            </attribute>
            <zeroOrMore>
                <element name="function"> <!-- access policies to plugin functions -->
                    <attribute name="name"> <!-- function name -->
                        <ref name="plugFuncName" />
                    </attribute>
                    <attribute name="nonauth"> <!-- default policy value for non-authenticated users -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </attribute>
                    <attribute name="auth"> <!-- default policy value for user groups -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </attribute>
                    <oneOrMore>
                        <element name="description"> <!-- description of policies -->
                            <attribute name="code"> <!-- language code of description -->
                                <data type="string">
                                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                </data>
                            </attribute>
                            <element name="short">
                                <data type="string"> <!-- short description -->
                                    <param name="minLength">1</param>
                                    <param name="maxLength">128</param>
                                </data>
                            </element>
                            <element name="detailed">
                                <data type="string"> <!-- detailed description -->
                                    <param name="minLength">0</param>
                                    <param name="maxLength">1024</param>
                                </data>
                            </element>
                        </element>
                    </oneOrMore>
                </element>
            </zeroOrMore>
        </element>
    </start>
    <define name="plugFuncName">
        <data type="string">
            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
        </data>
    </define>
</grammar>
```

*```validate```* - the structure of DOM is checked beforehand if value is ```TRUE```. The structure is not checked if value is ```FALSE```.

**Return values**  
Returns ```TRUE``` in case of successful installation. Otherwise returns ```FALSE```.

** **

**```bool delPolicy(string $plugin)```**

**Description**  
Deletes policies by plugin name.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool setFuncAccess(string $plugin, string $func, int $groupid, bool $access = TRUE)```**

**Description**  
Sets value of access to plugin function.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```func```* - name of plugin function.  
*```groupid```* - group identifier for which is set the value of access. Value 0 matches to nonauthenticated session.  
*```access```* - access value. Value ```TRUE``` enables access, value ```FALSE``` disables access.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```DOMDocument / bool / array groupPolicyList(string $plugin, int $groupId, string $code = MECCANO_DEF_LANG)```**

**Description**  
Returns access policies by defined plugin, group identifier and language.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```groupId```* - group identifier. Value 0 matches to nonauthenticated session.  
*```code```** - language code, matches pattern *[a-z]{2}-[A-Z]{2}*. Value ```NULL``` is equal to ```MECCANO_DEF_LANG```.

**Return values**  
In case of success returns access policies as object ```DOMDocument```, or as string JSON, otherwise returns ```FALSE```.  
JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugins": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "group": {
            "type": "integer",
            "minimum": 0
        },
        "functions": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "short": {
                        "type": "string",
                        "maxLength": 128
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,30}$"
                    },
                    "access": {
                        "type": "integer",
                        "minimum": 0,
                        "maximum": 1
                    }
                },
                "required": ["id", "short", "name", "access"]
            }
        }
    },
    "required": ["plugin", "group", "functions"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="policy">
            <attribute name="plugin"> <!-- plugin name -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <attribute name="group"> <!-- group identifier -->
                <data type="nonNegativeInteger" />
            </attribute>
            <zeroOrMore>
                <element name="function"> <!-- information about function -->
                    <element name="id"> <!-- identifier of policy description -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="short"> <!-- short description of a policy -->
                        <data type="string">
                            <param name="maxLength">128</param>
                        </data>
                    </element>
                    <element name="name"> <!-- function name -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,30}</param>
                        </data>
                    </element>
                    <element name="access"> <!-- access value, 1 - allowed, 0 - restricted -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array('plugin' => "plugin_name",
    'group' => group_identifier,
    'functions' => array(
        array('id' => identifier_of_policy_description,
        'short' => "short_description_of_a_policy",
        'name' => "function_name",
        'access' => access_value),
        array(...),
        ...
    )
)
```

** **

**```DOMDocument / bool / array getPolicyDescById(int $id)```**

**Description**  
Returns short and detailed description of access policy by identifier.

**Parameters**  
*```id```* - identifier of policy description.

**Return values**  
In case of success returns policy description as object ```DOMDocument```, or as string JSON, otherwise returns ```FALSE```.  
JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "id": {
            "type": "integer",
            "minimum": 1
        },
        "short": {
            "type": "string",
            "minLength": 1,
            "maxLength": 128
        },
        "detailed": {
            "type": "string",
            "minLength": 0,
            "maxLength": 1024
        }
    },
    "required": ["short", "detailed"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="policy">
            <element name="id"> <!-- policy identifier -->
                <data type="positiveInteger" />
            </element>
            <element name="short"> <!-- short description of a policy -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">128</param>
                </data>
            </element>
            <element name="detailed"> <!-- detailed description of a policy -->
                <data type="string">
                    <param name="minLength">0</param>
                    <param name="maxLength">1024</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'id' => policy_identifier,
    'short' => "short_description",
    'detailed' => "detailed_description"
)
```

** **

**```void outputFormat(string $output = 'xml')```**

**Description**  
Sets format of output data.

**Parameters**  
*```output```* - can get values *json*, or *xml*.

**[>>> Contents <<<](en.index)**