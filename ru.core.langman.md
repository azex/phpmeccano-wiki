**[>>> Содержание <<<](ru.index)**

## Модуль langman.php ##

### Общее описание ###

Содержит класс ```LangMan```, предназначенный для управления многоязычностью.

### Методы класса ```LangMan``` *(class ```LangMan``` extends ```LogMan```)* ###

**```__construct(\mysqli $dbLink)```**

**Описание**  
Устанавливает связь с базой данных.

**Принимаемые значения**
*```dbLink```* - объект ```mysqli```.

** **

**```bool installLang(\DOMDocument $langs, bool $validate = TRUE)```**

**Описание**  
Устанавливает новые языки.

**Принимаемые значения**  
*```langs```* - объект ```DOMDocument```, содержащий устанавливаемые языки.  
Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="languages">
            <zeroOrMore>
                <element name="lang"> <!-- нод языка -->
                    <attribute name="code"> <!-- код языка -->
                        <data type="string">
                            <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                        </data>
                    </attribute>
                    <attribute name="name"> <!-- название языка -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </attribute>
                    <attribute name="dir"> <!-- направление текста в языке -->
                        <choice>
                            <value>ltr</value>
                            <value>rtl</value>
                        </choice>
                    </attribute>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

*```validate```* - при значении ```TRUE``` производится предварительная проверка структуры DOM. При значении ```FALSE``` проверка не проводится.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool addLang(string $code, string $name, string dir = 'ltr', bool $log = TRUE)```**

**Описание**  
Добавляет новый язык в список доступных.

**Принимаемые значения**  
*```code```* - код нового языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.  
*```name```* - название языка длиной до 50 символов.  
*```dir```* - указывает, в каком направлении должен выводиться текст для языка. Допустимы два значения: ```'ltr'``` (с лева на право) и ```'rtl'``` (с права на лево).  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool delLang(string $code, bool $log = TRUE)```**

**Описание**  
Удаляет заданный язык и все связанные с ним данные.

**Принимаемые значения**  
*```code```* - код нового языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*. Не может принимать значение равное ```MECCANO_DEF_LANG```.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```DOMDocument langList()```**

**Описание**  
Возвращает список доступных языков.

**Возвращаемые значения**  
В случае успеха возвращает список доступных языков в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "code": {
                "type": "string",
                "pattern": "^[a-z]{2}-[A-Z]{2}$"
            },
            "name": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "dir": {
                "type": "string",
                "pattern": "^(ltr|rtl)$"
            }
        },
        "requied": ["id", "code", "name", "dir"]
    }
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="languages">
            <oneOrMore>
                <element name="lang"> <!-- информация о языке -->
                    <element name="id"> <!-- идентификатор языка -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="code"> <!-- код языка (языковой тег) -->
                        <data type="string">
                            <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                        </data>
                    </element>
                    <element name="name"> <!-- название языка -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="dir"> <!-- направление, в котором должен выводится текст -->
                        <choice>
                            <value>ltr</value>
                            <value>rtl</value>
                        </choice>
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    array(
        "id" => идентификатор_языка,
        "code" => код_языка,
        "name" => название_языка,
        "dir" => направление_текста
    ),
    array(...),
    ...
)
```

** **

**```bool installTitles(DOMDocument $titles, bool $validate = TRUE)```**

**Описание**  
Устанавливает многоязычные заголовки плагина.

**Принимаемые значения**  
*```titles```* - объект ```DOMDocument```, содержащий заголовки плагина.  
Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="titles">
            <attribute name="plugin"> <!-- короткое имя плагина -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <choice>
                    <element name="section"> <!-- заголовочный раздел -->
                        <attribute name="name"> <!-- имя раздела -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- старое имя раздела -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- тип раздела, статичный -->
                            <value>1</value>
                        </attribute>
                        <oneOrMore>
                            <element name="title"> <!-- заголовок -->
                                <attribute name="name"> <!-- имя заголовка -->
                                    <data type="string">
                                        <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                    </data>
                                </attribute>
                                <oneOrMore>
                                    <element name="language"> <!-- сам заголовок -->
                                        <attribute name="code"> <!-- код языка заголовка -->
                                            <data type="string">
                                                <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                            </data>
                                        </attribute>
                                        <data type="string">
                                            <param name="minLength">1</param>
                                            <param name="maxLength">128</param>
                                        </data>
                                    </element>
                                </oneOrMore>
                            </element>
                        </oneOrMore>
                    </element>
                    <element name="section"> <!-- заголовочный раздел -->
                        <attribute name="name"> <!-- имя раздела -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- старое имя раздела -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- тип раздела, нестатичный -->
                            <value>0</value>
                        </attribute>
                    </element>
                </choice>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

*```validate```* - при значении ```TRUE``` производится предварительная проверка структуры DOM. При значении ```FALSE``` проверка не проводится.

**Возвращаемые значения**  
```TRUE``` в случае успеха, иначе ```FALSE```.

** **

**```bool installTexts(DOMDocument $texts, bool $validate = TRUE)```**

**Описание**  
Устанавливает многоязычные тексты плагина.

**Принимаемые значения**  
*```texts```* - Объект ```DOMDocument```, содержащий тексты плагина.  
Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="texts">
            <attribute name="plugin"> <!-- короткое имя плагина -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <choice>
                    <element name="section"> <!-- текстовый раздел -->
                        <attribute name="name"> <!-- имя раздела -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- старое имя раздела -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- тип раздела, статичный -->
                            <value>1</value>
                        </attribute>
                        <oneOrMore>
                            <element name="text"> <!-- текст -->
                                <attribute name="name"> <!-- имя текста -->
                                    <data type="string">
                                        <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                    </data>
                                </attribute>
                                <oneOrMore>
                                    <element name="language">
                                        <attribute name="code"> <!-- код языка текста -->
                                            <data type="string">
                                                <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                            </data>
                                        </attribute>
                                        <element name="title"> <!-- заголовок текста -->
                                            <data type="string">
                                                <param name="minLength">1</param>
                                                <param name="maxLength">128</param>
                                            </data>
                                        </element>
                                        <element name="document"> <!-- сам текст -->
                                            <data type="string">
                                                <param name="minLength">0</param>
                                                <param name="maxLength">65535</param>
                                            </data>
                                        </element>
                                    </element>
                                </oneOrMore>
                            </element>
                        </oneOrMore>
                    </element>
                    <element name="section"> <!-- текстовый раздел -->
                        <attribute name="name"> <!-- имя раздела -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- старое имя раздела -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- тип раздела, нестатичный -->
                            <value>0</value>
                        </attribute>
                    </element>
                </choice>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

*```validate```* - при значении ```TRUE``` производится предварительная проверка структуры DOM. При значении ```FALSE``` проверка не проводится.

**Возвращаемые значения**  
```TRUE``` в случае успеха, иначе ```FALSE```.

** **

**```bool delPlugin(string $plugin)```**

**Описание**  
Удаляет многоязычные заголовки и тексты, связанные с плагином.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.

**Возвращаемые значения**  
```TRUE``` в случае успеха, иначе ```FALSE```.

** **

**```int addTitleSection(string $section, string $plugin, bool $log = TRUE)```**

**Описание**  
Добавляет новый нестатичный заголовочный раздел плагина.

**Принимаемые значения**  
*```section```* - имя нового раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
Числовой идентификатор нового раздела в случае успеха, иначе ```FALSE```.

** **

**```bool delTitleSection(int $sid, bool $log = TRUE)```**

**Описание**  
Удаляет нестатичный заголовочный раздел плагина вместе со всеми его заголовка на всех языках.

**Принимаемые значения**  
*```sid```* - идентификатор удаляемого раздела плагина.  
*```log```* - флаг создания записи в журнале событий.  

**Возвращаемые значения**  
```TRUE``` в случае успеха, иначе ```FALSE```.

** **

**```int addTextSection(string $section, string $plugin, bool $log = TRUE)```**

**Описание**  
Добавляет новый нестатичный текстовый раздел плагина.

**Принимаемые значения**  
*```section```* - имя нового раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
Числовой идентификатор нового раздела в случае успеха, иначе ```FALSE```.

** **

**```bool delTextSection(int $sid, bool $log = TRUE)```**

**Описание**  
Удаляет нестатичный текстовый раздел плагина вместе со всеми его текстами на всех языках.

**Принимаемые значения**  
*```sid```* - идентификатор удаляемого раздела плагина.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
```TRUE``` в случае успеха, иначе ```FALSE```.

** **

**```int addTitleName(string $name, string $section, string $plugin, bool $log = TRUE)```**

**Описание**  
Добавляет новое имя в заголовочный раздел плагина.

**Принимаемые значения**  
*```name```* - имя нового заголовка, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
Числовой идентификатор нового имени заголовка в случае успеха, иначе ```FALSE```.

** **

**```bool delTitleName(int $nameid, bool $log = TRUE)```**

**Описание**  
Удаляет имя заголовками вместе со всеми заголовками на всех языках.

**Принимаемые значения**  
*```nameid```* - идентификатор удаляемого имени.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
```TRUE``` в случае успеха, иначе ```FALSE```.

** **

**```int addTextName(string $name, string $section, string $plugin, bool $log = TRUE)```**

**Описание**  
Добавляет новое имя в текстовый раздел плагина.

**Принимаемые значения**  
*```name```* - имя нового текста, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
Числовой идентификатор нового имени текста в случае успеха, иначе ```FALSE```.

** **

**```bool delTextName(int $nameid, bool $log = TRUE)```**

**Описание**  
Удаляет имя текста вместе со всеми текстами на всех языках.

**Принимаемые значения**  
*```nameid```* - идентификатор удаляемого имени.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
```TRUE``` в случае успеха, иначе ```FALSE```.

** **

**```int addTitle(string $title, string $name, string $section, string $plugin, string $code = MECCANO_DEF0LANG, bool $log = TRUE)```**

**Описание**  
Добавляет новый заголовок с заданным именем, разделом и плагином на заданном языке.

**Принимаемые значения**  
*```title```* - заголовок длиной до 128 символов.  
*```name```* - имя заголовка, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
Числовой идентификатор нового заголовка в случае успеха, иначе ```FALSE```.

** **

**```bool delTitle(int $tid, bool $log = TRUE)```**

**Описание**  
Удаляет заголовок с заданным идентификатором.

**Принимаемые значения**  
*```tid```* - идентификатор удаляемого заголовка.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
```TRUE``` в случае успеха, иначе ```FALSE```.

** **

**```int addText(string $title, string $document, string $name, string $section, string $plugin, string $code = MECCANO_DEF_LANG, bool $log = TRUE)```**

**Описание**  
Добавляет новый текст с заданным именем, разделом и плагином на заданном языке.

**Принимаемые значения**  
*```title```* - заголовок длиной до 128 символов.  
*```document```* - текст длиной до 65535 символов.  
*```name```* - имя текста, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
Числовой идентификатор нового текста в случае успеха, иначе ```FALSE```.

** **

**```bool delText(int $tid, bool $log = TRUE)```**

**Описание**  
Удаляет текст с заданным идентификатором.

**Принимаемые значения**  
*```tid```* - идентификатор удаляемого текста.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**
```TRUE``` в случае успеха, иначе ```FALSE```.

** **

**```bool updateTitle(int $tid, string $title, bool $log = TRUE)```**

**Описание**  
Обновляет указанный существующий заголовок.

**Принимаемые значения**  
*```tid```* - идентификатор заголовка.  
*```title```* - новый заголовок длиной до 128 символов.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
```TRUE``` в случае успеха, иначе ```FALSE```.

** **

**```bool updateText(int $tid, string $title, string $document, bool $log = TRUE)```**

**Описание**  
Обновляет указанный существующий текст.

**Принимаемые значения**  
*```tid```* - идентификатор текста.  
*```title```* - новый заголовок текста длиной до 128 символов.  
*```document```* - новый текст длиной до 65635 символов.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
```TRUE``` в случае успеха, иначе ```FALSE```.

** **

**```array / bool / DOMDocument / string getTitle(string $name, string $section, string $plugin, string $code = MECCANO_DEF_LANG)```**

**Описание**  
Возвращает заголовок по заданным имени, разделу, плагину и коду языка.

**Принимаемые значения**  
*```name```* - имя заголовка, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.

**Возвращаемые значения**  
В случае успеха возвращает заголовок и направление отображения текста в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "title": {
            "type": "string",
            "minLength": 1,
            "maxLength": 128
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        }
    },
    "required": ["title", "dir"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="string">
            <element name="title"> <!-- заголовок -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">128</param>
                </data>
            </element>
            <element name="dir"> <!-- направление текста -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'title' => "пример заголовка", 
    'dir' => "направление текста [ltr или rtl]"
)
```

** **

**```array / bool / DOMDocument / string getText(string $name, string $section, string $plugin, string $code = MECCANO_DEF_LANG)```**

**Описание**  
Возвращает заголовок и текст по заданным имени, разделу, плагину и коду языка.

**Принимаемые значения**  
*```name```* - имя текста, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.

**Возвращаемые значения**  
В случае успеха возвращает заголовок, текст, время создания/редактирования и направление отображения текста в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "title": {
            "type": "string",
            "maxLength": 128
        },
        "document": {
            "type": "string",
            "minLength": 1,
            "maxLength": 65535
        },
        "created": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "edited": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        }
    },
    "required": ["title", "document", "created", "edited", "dir"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="text">
            <element name="title"> <!-- заголовок текста -->
                <data type="string">
                    <param name="maxLength">128</param>
                </data>
            </element>
            <element name="document"> <!-- текст -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="created"> <!-- дата создания текста -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="edited"> <!-- дата последнего редактирования текста -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="dir"> <!-- направление текста -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'title' => "пример заголовка текста", 
    'document' => "пример текста", 
    'created' => 0000-00-00 00:00:00, 
    'edited' => 0000-00-00 00:00:00, 
    'dir' => "направление текста [ltr или rtl]"
)
```

** **

**```array getTitles(string $section, string $plugin, string $code = MECCANO_DEF_LANG)```**

**Описание**  
Возвращает все заголовки по заданным разделу, плагину и коду языка для использования по принципу ключ-значение.

**Принимаемые значения**  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.

**Возвращаемые значения**  
В случае успеха возвращает все заголовки в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "patternProperties": {
        "^[a-zA-Z0-9_]{3,40}$": {
            "type": "string",
            "minLength": 1,
            "maxLength": 128
        }
    },
    "additionalProperties": false
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="titles">
            <zeroOrMore>
                <element name="title"> <!-- блок заголовка -->
                    <attribute name="name"> <!-- имя заголовка -->
                        <data type="string"> <!-- заголовок -->
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </attribute>
                    <data type="string">
                        <param name="minLength">1</param>
                        <param name="maxLength">128</param>
                    </data>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    "title1" => "заголовок #1",
    "title2" => "заголовок #2"
    ...
)
```

** **

**```DOMDocument getAllTextsList(string $section, string $plugin, string $code = MECCANO_DEF_LANG, array $orderBy = 'id', bool $ascent = FALSE)```**

**Описание**  
Возвращает список заголовков всех текстов по заданным разделу, плагину и коду языка.

**Принимаемые значения**  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - параметры сортировки вывода заголовков текстов. Допустимые значения параметров: ```'id'```, ```'title'```, ```'name'```, ```'created'``` и ```'edited'```. Последовательность параметров задаётся как массив, например, ```array('title', 'created')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает список заголовков всех текстов в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "code": {
            "type": "string",
            "pattern": "^[a-z]{2}-[A-Z]{2}$"
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        },
        "texts": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "title": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    },
                    "created": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    },
                    "edited": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["id", "title", "name", "created", "edited"]
            }
        }
    },
    "required": ["code", "dir", "texts"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="texts">
            <attribute name="code"> <!-- код языка, на котором выводятся данные -->
                <data type="string">
                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                </data>
            </attribute>
            <attribute name="dir"> <!-- направление, в котором должен выводиться текст -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </attribute>
            <zeroOrMore>
                <element name="text"> <!-- текстовый блок -->
                    <element name="id"> <!-- идентификатор текста -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="title"> <!-- заголовок текста -->
                        <data type="string">
                            <param name="maxLength">128</param>
                        </data>
                    </element>
                    <element name="name"> <!-- имя текста -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                    <element name="created"> <!-- дата создания текста -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="edited"> <!-- дата редактирования текста -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    "code" => код_языка,
    "dir" => направление_текста,
    "texts" => array(
        array(
            "id" => идентификатор_текста,
            "title" => заголовок_текста,
            "name" => имя_текста,
            "created" => дата_создания_текста,
            "edited" => дата_редактирования_текста
        ),
        array(...),
        ...
    )
)
```

** **

**```array / bool / DOMDocument / string getTextById(int $id)```**

**Описание**  
Возвращает текст по заданному идентификатору.

**Принимаемые значения**  
*```id```* - идентификатор текста.

**Возвращаемые значения**  
В случае успеха возвращает заголовок, текст, время создания/редактирования и направление отображения текста в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "title": {
            "type": "string",
            "maxLength": 128
        },
        "document": {
            "type": "string",
            "minLength": 1,
            "maxLength": 65535
        },
        "created": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "edited": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        }
    },
    "required": ["title", "document", "created", "edited", "dir"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="text">
            <element name="title"> <!-- заголовок текста -->
                <data type="string">
                    <param name="maxLength">128</param>
                </data>
            </element>
            <element name="document"> <!-- текст -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="created"> <!-- дата создания текста -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="edited"> <!-- дата последнего редактирования текста -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="dir"> <!-- направление текста -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'title' => "пример заголовка текста", 
    'document' => "пример текста", 
    'created' => 0000-00-00 00:00:00, 
    'edited' => 0000-00-00 00:00:00, 
    'dir' => "направление текста [ltr или rtl]"
)
```

** **

**```array sumTexts(string $section, plugin $plugin, string $code = MECCANO_DEF_LANG, int $rpp = 20)```**

**Описание**  
Возвращает обобщённые данные о текстах в разделе плагина.

**Принимаемые значения**  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*. Значение ```NULL``` приравнивается к ```MECCANO_DEF_LANG```.

**Возвращаемые значения**  
Массив вида ```array('records' => int $totalTexts, 'pages' => int $totalPages)```, где ```totalTexts``` - общее количество текстов в разделе плагина, ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalTexts/rpp```. В случае неудачи возвращает ```FALSE```.

** **

**```DOMDocument getTextsList(string $section, string $plugin, int $pageNumber, int $totalPages, int $rpp = 20, string $code = MECCANO_DEF_LANG, $orderBy = 'id', bool $ascent = FALSE)```**

**Описание**  
Возвращает страницу с заголовками текстов по заданным разделу, плагину и коду языка.

**Принимаемые значения**  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalPages```* - общее количество страниц.  
*```rpp```* - желаемое максимальное количество записей на странице.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - параметры сортировки вывода текстовых заголовков. Допустимые значения параметров: ```'id'```, ```'title'```, ```'name'```, ```'created'``` и ```'edited'```. Последовательность параметров задаётся как массив, например, ```array('title', 'created')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает страницу с заголовками текстов в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "code": {
            "type": "string",
            "pattern": "^[a-z]{2}-[A-Z]{2}$"
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        },
        "texts": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "title": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    },
                    "created": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    },
                    "edited": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["id", "title", "name", "created", "edited"]
            }
        }
    },
    "required": ["code", "dir", "texts"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="texts">
            <attribute name="code"> <!-- код языка, на котором выводятся данные -->
                <data type="string">
                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                </data>
            </attribute>
            <attribute name="dir"> <!-- направление, в котором должен выводиться текст -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </attribute>
            <zeroOrMore>
                <element name="text"> <!-- текстовый блок -->
                    <element name="id"> <!-- идентификатор текста -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="title"> <!-- заголовок текста -->
                        <data type="string">
                            <param name="maxLength">128</param>
                        </data>
                    </element>
                    <element name="name"> <!-- имя текста -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                    <element name="created"> <!-- дата создания текста -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="edited"> <!-- дата редактирования текста -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    "code" => код_языка,
    "dir" => направление_текста,
    "texts" => array(
        array(
            "id" => идентификатор_текста,
            "title" => заголовок_текста,
            "name" => имя_текста,
            "created" => дата_создания_текста,
            "edited" => дата_редактирования_текста
        ),
        array(...),
        ...
    )
)
```

** **

**```array getTexts($section, $plugin, $code = MECCANO_DEF_LANG)```**

**Описание**  
Возвращает все тексты с заголовками по заданным разделу, плагину и коду языка для использования по принципу ключ-значение.

**Принимаемые значения**  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.

**Возвращаемые значения**  
В случае успеха возвращает все тексты с заголовками в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "patternProperties": {
        "^[a-zA-Z0-9_]{3,40}$": {
            "type": "object",
            "properties": {
                "title" : {
                    "type": "string",
                    "maxLength": 128
                },
                "document": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 65535
                }
            },
            "required": ["title", "document"]
        }
    },
    "additionalProperties": false
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="texts">
            <zeroOrMore>
                <element name="text"> <!-- блок текста -->
                    <attribute name="name"> <!-- имя текста -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </attribute>
                    <element name="title"> <!-- заголовок текста -->
                        <data type="string">
                            <param name="maxLength">128</param>
                        </data>
                    </element>
                    <element name="document"> <!-- текст документа -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">65535</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    "имя_текста" => array(
        "title" => "заголовок текста",
        "document" => "текст документа"
    ),
    "другое_имя_текста" => array(...)
    ...
)
```

** **

**```array sumTitles($section, $plugin, $code = MECCANO_DEF_LANG, $rpp = 20)```**

**Описание**  
Возвращает обобщённые данные о заголовках в разделе плагина.

**Принимаемые значения**  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
Массив вида ```array('records' => int $totalTitles, 'pages' => int $totalPages)```, где ```totalTitles``` - общее количество текстов в разделе плагина, ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalTitles/rpp```. В случае неудачи возвращает ```FALSE```.

** **

**```DOMDocument getTitlesList(string $section, string $plugin, int $pageNumber, int $totalPages, int $rpp = 20, string $code = MECCANO_DEF_LANG, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает страницу с заголовками по заданным разделу, плагину и коду языка.

**Принимаемые значения**  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalPages```* - общее количество страниц.  
*```rpp```* - желаемое максимальное количество записей на странице.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - параметры сортировки вывода заголовков. Допустимые значения параметров: ```'id'```, ```'title'``` и ```'name'```. Последовательность параметров задаётся как массив, например, ```array('title', 'name')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает страницу с заголовками в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "code": {
            "type": "string",
            "pattern": "^[a-z]{2}-[A-Z]{2}$"
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        },
        "titles": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "title": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    }
                },
                "required": ["id", "title", "name"]
            }
        }
    },
    "required": ["code", "dir", "titles"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="titles">
            <attribute name="code"> <!-- код языка, на котором выводятся данные -->
                <data type="string">
                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                </data>
            </attribute>
            <attribute name="dir"> <!-- направление, в котором должен выводиться текст заголовка -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </attribute>
            <zeroOrMore>
                <element name="title"> <!-- заголовочный блок -->
                    <element name="id"> <!-- идентификатор заголовка -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="title"> <!-- заголовок -->
                        <data type="string">
                            <param name="maxLength">128</param>
                        </data>
                    </element>
                    <element name="name"> <!-- имя заголовка -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    "code" => код_языка,
    "dir" => направление_текста,
    "titles" => array(
        array(
            "id" => идентификатор_заголовка,
            "title" => заголовок,
            "name" => имя_заголовка
        ),
        array(...),
        ...
    )
)
```

** **

**```DOMDocument getAllTitlesList(string $section, string $plugin, string $code = MECCANO_DEF_LANG, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает список всех заголовков по заданным разделу, плагину и коду языка.

**Принимаемые значения**  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - параметры сортировки вывода заголовков. Допустимые значения параметров: ```'id'```, ```'title'``` и ```'name'```. Последовательность параметров задаётся как массив, например, ```array('title', 'name')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает список всех заголовков в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "code": {
            "type": "string",
            "pattern": "^[a-z]{2}-[A-Z]{2}$"
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        },
        "titles": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "title": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    }
                },
                "required": ["id", "title", "name"]
            }
        }
    },
    "required": ["code", "dir", "titles"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="titles">
            <attribute name="code"> <!-- код языка, на котором выводятся данные -->
                <data type="string">
                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                </data>
            </attribute>
            <attribute name="dir"> <!-- направление, в котором должен выводиться текст заголовка -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </attribute>
            <zeroOrMore>
                <element name="title"> <!-- заголовочный блок -->
                    <element name="id"> <!-- идентификатор заголовка -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="title"> <!-- заголовок -->
                        <data type="string">
                            <param name="maxLength">128</param>
                        </data>
                    </element>
                    <element name="name"> <!-- имя заголовка -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    "code" => код_языка,
    "dir" => направление_текста,
    "titles" => array(
        array(
            "id" => идентификатор_заголовка,
            "title" => заголовок,
            "name" => имя_заголовка
        ),
        array(...),
        ...
    )
)
```

** **

**```array / bool / DOMDocument / string getTitleById(int $id)```**

**Описание**  
Возвращает заголовок по идентификатору.

**Принимаемые значения**  
*```id```* - идентификатор заколовка.

**Возвращаемые значения**  
В случае успеха возвращает заголовок и направление отображения текста в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "title": {
            "type": "string",
            "minLength": 1,
            "maxLength": 128
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        }
    },
    "required": ["title", "dir"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="string">
            <element name="title"> <!-- заголовок -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">128</param>
                </data>
            </element>
            <element name="dir"> <!-- направление текста -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'title' => "пример заголовка", 
    'dir' => "направление текста [ltr или rtl]"
)
```

** **

**```array sumTextSections(string $plugin, int $rpp = 20)```**

**Описание**  
Возвращает обобщённые данные о текстовых разделах плагина.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
Массив вида ```array('records' => int $totalSections, 'pages' => int $totalPages)```, где ```totalSections``` - общее количество текстов в разделе плагина, ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalSections/rpp```. В случае неудачи возвращает ```FALSE```.

** **

**```DOMDocument getTextSectionsList(string $plugin, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает список текстовых разделов плагина.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalPages```* - общее количество страниц.  
*```rpp```* - желаемое максимальное количество записей на странице.  
*```orderBy```* - параметры сортировки вывода разделов плагина. Допустимые значения параметров: ```'id'```, ```'name'``` и ```'static'```. Последовательность параметров задаётся как массив, например, ```array('id', 'static')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает список текстовых разделов плагина в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugin": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "sections": {
            "type": "array",
            "item": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    },
                    "static": {
                        "type": "integer",
                        "minimum": 0,
                        "maximum": 1
                    },
                    "contains": {
                        "type": "integer",
                        "minimum": 0
                    }
                },
                "required": ["id", "name", "static", "contains"]
            }
        }
    },
    "required": ["plugin", "sections"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="sections">
            <attribute name="plugin"> <!-- имя плагина, к которому относятся разделы -->
                <ref name="plugFuncName" />
            </attribute>
            <oneOrMore>
                <element name="section"> <!-- блок раздела -->
                    <element name="id"> <!-- идентификатор раздела -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="name"> <!-- имя раздела -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                    <element name="static"> <!-- тип раздела. 0 - нестатичный, 1 - статичный -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                    <element name="contains"> <!-- количество записей в разделе (считается по количеству имен) -->
                        <data type="nonNegativeInteger" />
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
    <define name="plugFuncName">
        <data type="string">
            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
        </data>
    </define>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    "plugin" => имя_плагина,
    "sections" => array(
        array(
            "id" => идентификатор_раздела,
            "name" => имя_раздела,
            "static" => тип_раздела,
            "contains" => количество_записей
        ),
        array(
            ...
        ),
        ...
    )
)
```

** **

**```array sumTitleSections(string $plugin, int $rpp = 20)```**

**Описание**  
Возвращает обобщённые данные о заголовочных разделах плагина.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
Массив вида ```array('records' => int $totalSections, 'pages' => int $totalPages)```, где ```totalSections``` - общее количество текстов в разделе плагина, ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalSections/rpp```. В случае неудачи возвращает ```FALSE```.

** **

**```DOMDocument getTitleSectionsList(string $plugin, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает список заголовочных разделов плагина.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalPages```* - общее количество страниц.  
*```rpp```* - желаемое максимальное количество записей на странице.  
*```orderBy```* - параметры сортировки вывода разделов плагина. Допустимые значения параметров: ```'id'```, ```'name'``` и ```'static'```. Последовательность параметров задаётся как массив, например, ```array('id', 'static')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает список заголовочных разделов плагина в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugin": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "sections": {
            "type": "array",
            "item": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    },
                    "static": {
                        "type": "integer",
                        "minimum": 0,
                        "maximum": 1
                    },
                    "contains": {
                        "type": "integer",
                        "minimum": 0
                    }
                },
                "required": ["id", "name", "static", "contains"]
            }
        }
    },
    "required": ["plugin", "sections"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="sections">
            <attribute name="plugin"> <!-- имя плагина, к которому относятся разделы -->
                <ref name="plugFuncName" />
            </attribute>
            <oneOrMore>
                <element name="section"> <!-- блок раздела -->
                    <element name="id"> <!-- идентификатор раздела -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="name"> <!-- имя раздела -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                    <element name="static"> <!-- тип раздела. 0 - нестатичный, 1 - статичный -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                    <element name="contains"> <!-- количество записей в разделе (считается по количеству имен) -->
                        <data type="nonNegativeInteger" />
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
    <define name="plugFuncName">
        <data type="string">
            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
        </data>
    </define>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    "plugin" => имя_плагина,
    "sections" => array(
        array(
            "id" => идентификатор_раздела,
            "name" => имя_раздела,
            "static" => тип_раздела,
            "contains" => количество_записей
        ),
        array(
            ...
        ),
        ...
    )
)
```

** **

**```array sumTextNames(string $plugin, string $section, int $rpp = 20)```**

**Описание**  
Возвращает обобщённые данные о текстовых именах по плагину и разделу.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
Массив вида ```array('records' => int $totalNames, 'pages' => int $totalPages)```, где ```totalNames``` - общее количество имен в разделе плагина, ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalNames/rpp```. В случае неудачи возвращает ```FALSE```.

** **

**```DOMDocument getTextNamesList(string $plugin, string $section, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = 'id', bool $ascent = FALSE)```**

**Описание**  
Возвращает список текстовых имен по плагину и разделу.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalPages```* - общее количество страниц.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.  
*```orderBy```* - параметры сортировки вывода текстовых имён из разделов плагина. Допустимые значения параметров: ```'id'```, ```'name'```. Последовательность параметров задаётся как массив, например, ```array('name', 'id')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает текстовые имена в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```.

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugin": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "section": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,40}$"
        },
        "texts": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    },
                    "languages": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "pattern": "^[a-z]{2}-[A-Z]{2}$"
                        }
                    }
                },
                "required": ["id", "name", "languages"]
            }
        }
    },
    "required": ["plugin", "section", "texts"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="page">
            <attribute name="plugin"> <!-- имя плагина, к которому относится обозреваемый раздел -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <attribute name="section"> <!-- имя раздела, к которому относятся имена -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="text"> <!-- именной блок -->
                    <element name="id"> <!-- идентификатор имени -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="name"> <!-- имя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                    <element name="languages"> <!-- перечень языков, на которых присутствуют записи -->
                        <zeroOrMore>
                            <element name="code"> <!-- код языка -->
                                <data type="string">
                                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                </data>
                            </element>
                        </zeroOrMore>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    "plugin" => имя_плагина,
    "section" => имя раздела,
    "texts" => array(
        array(
            "id" => идентификатор имени,
            "name" => имя,
            "languages" => array("яз-ЫК", ...)
        ),
        array(
            ...
        ),
        ...
    )
)
```

** **

**```array sumTitleNames(string $plugin, string $section, int $rpp = 20)```**

**Описание**  
Возвращает обобщённые данные о заголовочных именах по плагину и разделу.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
Массив вида ```array('records' => int $totalNames, 'pages' => int $totalPages)```, где ```totalNames``` - общее количество имен в разделе плагина, ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalNames/rpp```. В случае неудачи возвращает ```FALSE```.

** **

**```DOMDocument getTitleNamesList(string $plugin, string $section, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает список заголовочных имен по плагину и разделу.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону *[a-zA-Z\d_]{3,30}*.  
*```section```* - имя раздела, соответствующее шаблону *[a-zA-Z\d_]{3,40}*.  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalPages```* - общее количество страниц.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.  
*```orderBy```* - параметры сортировки вывода заголовочных имён из разделов плагина. Допустимые значения параметров: ```'id'```, ```'name'```. Последовательность параметров задаётся как массив, например, ```array('name', 'id')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает заголовочные имена в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugin": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "section": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,40}$"
        },
        "titles": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    },
                    "languages": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "pattern": "^[a-z]{2}-[A-Z]{2}$"
                        }
                    }
                },
                "required": ["id", "name", "languages"]
            }
        }
    },
    "required": ["plugin", "section", "titles"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="page">
            <attribute name="plugin"> <!-- имя плагина, к которому относится обозреваемый раздел -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <attribute name="section"> <!-- имя раздела, к которому относятся имена -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="title"> <!-- именной блок -->
                    <element name="id"> <!-- идентификатор имени -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="name"> <!-- имя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                    <element name="languages"> <!-- перечень языков, на которых присутствуют записи -->
                        <zeroOrMore>
                            <element name="code"> <!-- код языка -->
                                <data type="string">
                                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                </data>
                            </element>
                        </zeroOrMore>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    "plugin" => имя_плагина,
    "section" => имя раздела,
    "titles" => array(
        array(
            "id" => идентификатор имени,
            "name" => имя,
            "languages" => array("яз-ЫК", ...)
        ),
        array(
            ...
        ),
        ...
    )
)
```

** **

**```void outputFormat(string $output = 'xml')```**

**Описание**  
Устанавливает формат выводимых данных.

**Принимаемые значения**  
*```output```* - может принимать значения *json*, или *xml*.

**[>>> Содержание <<<](ru.index)**