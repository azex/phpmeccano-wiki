**[>>> Содержание <<<](ru.index)**

## Модуль policy.php ##

### Общее описание ###

Содержит класс ```Policy```, позволяющий осуществлять контроль доступа групп пользователей к функциям.

### Методы класса ```Policy``` *(class ```Policy``` extends ```ServiceMethods```)* ###

**```__construct(mysqli $dbLink)```**

**Описание**  
Устанавливает соединение модуля с базой данных.

**Принимаемые значения**  
*```dbLink```* - объект ```mysqli```.

** **

**```bool installPolicy(DOMDocument $policy, bool $validate = TRUE)```**

**Описание**  
Устанавливает политики доступа плагина и описание этих политик.

**Принимаемые значения**  
*```policy```* - объект ```DOMDocument```, содержащий имя плагина, перечень его политик и описание этих политик.  
Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="policy">
            <attribute name="plugin"> <!-- короткое имя плагина -->
                <ref name="plugFuncName" />
            </attribute>
            <zeroOrMore>
                <element name="function"> <!-- политики доступа к функциям плагина -->
                    <attribute name="name"> <!-- имя функции -->
                        <ref name="plugFuncName" />
                    </attribute>
                    <attribute name="nonauth"> <!-- значение политики по умолчанию для пользователей, не прошедших аутентификацию -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </attribute>
                    <attribute name="auth"> <!-- значение политики по умолчанию для групп пользователей -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </attribute>
                    <oneOrMore>
                        <element name="description"> <!-- Описание политик -->
                            <attribute name="code"> <!-- код языка описания -->
                                <data type="string">
                                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                </data>
                            </attribute>
                            <element name="short">
                                <data type="string"> <!-- короткое описание -->
                                    <param name="minLength">1</param>
                                    <param name="maxLength">128</param>
                                </data>
                            </element>
                            <element name="detailed">
                                <data type="string"> <!-- детальное описание -->
                                    <param name="minLength">0</param>
                                    <param name="maxLength">1024</param>
                                </data>
                            </element>
                        </element>
                    </oneOrMore>
                </element>
            </zeroOrMore>
        </element>
    </start>
    <define name="plugFuncName">
        <data type="string">
            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
        </data>
    </define>
</grammar>
```

*```validate```* - при значении ```TRUE``` производится предварительная проверка структуры DOM. При значении ```FALSE``` проверка не проводится.

**Возвращаемые значения**  
В случае успешной установки возвращает ```TRUE```, иначе ```FALSE```.

** **

**bool delPolicy(string $plugin)**  

**Описание**  
Удаляет политики по имени плагина.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону ```[a-zA-Z\d_]{3,30}```.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool setFuncAccess(string $plugin, string $func, int $groupid, bool $access = TRUE)```**

**Описание**  
Устанавливает значение доступа к функции плагина.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону ```[a-zA-Z\d_]{3,30}```.  
*```func```* - имя функции плагина.  
*```groupid```* - идентификатор группы, для которой задается значение доступа. Значение 0 соответствует неавторизованной сессии.  
*```access```* - значение доступа. ```TRUE``` дает доступ, а ```FALSE``` запрещает.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```DOMDocument / bool / array groupPolicyList(string $plugin, int $groupId, string $code = MECCANO_DEF_LANG)```**

**Описание**  
Возвращает политики доступа по заданным плагину, идентификатору группы и языку.

**Принимаемые значения**  
*```plugin```* - имя плагина, соответствующее шаблону ```[a-zA-Z\d_]{3,30}```.  
*```groupId```* - идентификатор группы. Значение 0 соответствует неавторизованной сессии.  
*```code```* - код языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*. Значение ```NULL``` приравнивается к ```MECCANO_DEF_LANG```.

**Возвращаемые значения**  
В случае успеха возвращает политики доступа в виде объекта ```DOMDocument```, либо строки в формате ```JSON```, иначе возвращает ```FALSE```.  
Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugins": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "group": {
            "type": "integer",
            "minimum": 0
        },
        "functions": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "short": {
                        "type": "string",
                        "maxLength": 128
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,30}$"
                    },
                    "access": {
                        "type": "integer",
                        "minimum": 0,
                        "maximum": 1
                    }
                },
                "required": ["id", "short", "name", "access"]
            }
        }
    },
    "required": ["plugin", "group", "functions"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="policy">
            <attribute name="plugin"> <!-- имя плагина -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <attribute name="group"> <!-- идентификатор группы -->
                <data type="nonNegativeInteger" />
            </attribute>
            <zeroOrMore>
                <element name="function"> <!-- информация о функции -->
                    <element name="id"> <!-- идентификатор описания политики -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="short"> <!-- короткое описание политики -->
                        <data type="string">
                            <param name="maxLength">128</param>
                        </data>
                    </element>
                    <element name="name"> <!-- имя функции -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,30}</param>
                        </data>
                    </element>
                    <element name="access"> <!-- значение доступа, 1 - есть доступ, 0 - нет доступа -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array('plugin' => "имя_плагина",
    'group' => идентификатор_группы,
    'functions' => array(
        array('id' => идентификатор_описания_политики,
        'short' => "короткое_описание_политики",
        'name' => "имя_функции",
        'access' => значение_доступа),
        array(...),
        ...
    )
)
```

** **

**```DOMDocument / bool / array getPolicyDescById(int $id)```**

**Описание**  
Возращает короткое и полное описание политики доступа по идентификатору.

**Принимаемые значения**  
*```id```* - идентификатор описания политики.

**Возвращаемые значения**  
В случае успеха возвращает описания политики в виде объекта ```DOMDocument```, либо строки в формате ```JSON```, иначе возвращает ```FALSE```.  
Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "id": {
            "type": "integer",
            "minimum": 1
        },
        "short": {
            "type": "string",
            "minLength": 1,
            "maxLength": 128
        },
        "detailed": {
            "type": "string",
            "minLength": 0,
            "maxLength": 1024
        }
    },
    "required": ["short", "detailed"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="policy">
            <element name="id"> <!-- идентификатор политики -->
                <data type="positiveInteger" />
            </element>
            <element name="short"> <!-- короткое описание политики -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">128</param>
                </data>
            </element>
            <element name="detailed"> <!-- детальное описание политики -->
                <data type="string">
                    <param name="minLength">0</param>
                    <param name="maxLength">1024</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'id' => идентификатор_политики,
    'short' => "короткое_описание",
    'detailed' => "детальное_описание"
)
```

** **

**```void outputFormat(string $output = 'xml')```**

**Описание**  
Устанавливает формат выводимых данных.

**Принимаемые значения**  
*```output```* - может принимать значения *json*, или *xml*.

**[>>> Содержание <<<](ru.index)**