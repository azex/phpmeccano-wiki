**[>>> Contents <<<](en.index)**

## Module discuss.php ##

### General description ###

Includes class ```Discuss``` that is purposed to create topics and to comment something.


### Methods of class ```Discuss``` *(class ```Discuss``` extends ```LogMan```)* ###

**```__construct(mysqli $dbLink)```**

**Description**  
Sets a connection to database.

**Parameters**  
*```dbLink```* - object ```mysqli```.

** **

**```string / bool createTopic(string $topic = '')```**

**Description**  
Creates a topic to discuss or to comment.

**Parameters**  
*```topic```* - topic name, up to 100 characters.

**Return values**  
In case of success returns identifier (GUID) of the created topic. Otherwise returns ```FALSE```.

** **

**```string / bool createComment(string $comment, int $userId, string $topicId, string $parentId = '')```**

**Description**  
Creates comment in the defined topic.

**Parameters**  
*```comment```* - text of the comment, up to 1024 characters.  
*```userId```* - identifier of the user from name of whom a comment is created.  
*```topicId```* - identifier of the topic in which a comment is created.  
*```parendId```* - identifier of the parental comment, define it if a comment is created as an answer to previous comment.

**Return values**  
In case of success returns identifier (GUID) of the created comment. Otherwise returns ```FALSE```.

** **

**```string / DOMDocument / bool getAllComments(string $topicId)```**

**Description**  
Reads and returns comments of the defined topic.

**Parameters**  
*```topicId```* - topic identifier.  

**Return values**  
In case of success returns comments of the defined topic as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "topic": {
            "type": ["string", "null"]
        },
        "tid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["comments", "topic", "tid", "minmark", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="topic"> <!-- topic name -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="tid"> <!-- topic identifier -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- node of the comment -->
                    <element name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- full name of the user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- identifier of the comment -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- identifier of the parental comment -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- text of the comment -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of the comment creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'topic' => 'topic name',
    'tid' => 'topic identifier',
    'minmark' => minimum_mark,
    'maxmark' => maximum_mark,
    'comments' => array(
        array(
            'username' => 'username',
            'fullname' => 'full name of the user',
            'cid' => 'identifier of the comment',
            'pcid' => 'identifier of the parental comment',
            'text' => 'text of the comment',
            'time' => 'date and time of the comment creation'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool getComments(string $topicId, int $rpp = 20)```**

**Description**  
Reads and returns comments of the defined topic.

**Parameters**  
*```topicId```* - topic identifier.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
In case of success returns comments of the defined topic as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "topic": {
            "type": ["string", "null"]
        },
        "tid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["comments", "topic", "tid", "minmark", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="topic"> <!-- topic name -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="tid"> <!-- topic identifier -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- node of the comment -->
                    <element name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- full name of the user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- identifier of the comment -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- identifier of the parental comment -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- text of the comment -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of the comment creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'topic' => 'topic name',
    'tid' => 'topic identifier',
    'minmark' => minimum_mark,
    'maxmark' => maximum_mark,
    'comments' => array(
        array(
            'username' => 'username',
            'fullname' => 'full name of the user',
            'cid' => 'identifier of the comment',
            'pcid' => 'identifier of the parental comment',
            'text' => 'text of the comment',
            'time' => 'date and time of the comment creation'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool appendComments(string $topicId, double $minMark, int $rpp = 20)```**

**Description**  
Reads and returns comment of defined topic and following after the comment with minimum mark.

**Parameters**  
*```topicId```* - topic identifier.  
*```minMark```* - minimum mark.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
In case of success returns comments of the defined topic as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["comments", "minmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="minmark"> <!-- minimum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- node of the comment -->
                    <element name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- full name of the user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- identifier othe comment -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- identifier othe parental comment -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- text othe comment -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of the comment creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'topic' => 'topic name',
    'tid' => 'topic identifier',
    'minmark' => minimum_mark,
    'comments' => array(
        array(
            'username' => 'username',
            'fullname' => 'full name of the user',
            'cid' => 'identifier of the comment',
            'pcid' => 'identifier of the parental comment',
            'text' => 'text of the comment',
            'time' => 'date and time of the comment creation'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool updateComments(string $topicId, double $maxMark)```**

**Description**  
Reads and returns comment of defined topic and following before the comment with maximum mark.

**Parameters**  
*```topicId```* - topic identifier.  
*```maxMark```* - maximum mark.

**Return values**  
In case of success returns comments of the defined topic as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["comments", "maxmark"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="maxmark"> <!-- maximum mark -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- node of the comment -->
                    <element name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- full name of the user -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- identifier of the comment -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- identifier of the parental comment -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- text of the comment -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of the comment creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'topic' => 'topic name',
    'tid' => 'topic identifier',
    'maxmark' => maximum_mark,
    'comments' => array(
        array(
            'username' => 'username',
            'fullname' => 'full name of the user',
            'cid' => 'identifier of the comment',
            'pcid' => 'identifier of the parental comment',
            'text' => 'text of the comment',
            'time' => 'date and time of the comment creation'
        ),
        array(...),
        ...
    )
)
```

** **

**```bool editComment(string $comment, string $commentId, int $userId)```**

**Description**  
Edits comment.

**Parameters**  
*```comment```* - text of the comment, up to 1024 characters.  
*```commentId```* - identifier of the comment.  
*```userId```* - identifier of the user who owns an editable comment.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```string / DOMDocument / bool getComment(string $commentId, int $userId)```**

**Description**  
Reads and returns information about the comment.

**Parameters**  
*```commentId```* - identifier of the comment.  
*```userId```* - identifier of the user who owns the comment.  

**Return values**  
In case of success returns information about the comment as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "uid": {
            "type": "integer"
        },
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "fullname": {
            "type": ["string", "null"],
            "maxLength": 100
        },
        "cid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "text": {
            "type": "string",
            "minLength": 1,
            "maxLength": 1024
        }
    },
    "required": ["uid", "username", "fullname", "cid", "text"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comment">
            <element name="uid"> <!-- user identifier -->
                <data type="positiveInteger" />
            </element>
            <element name="username"> <!-- username -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </element>
            <element name="fullname"> <!-- full name of the user -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="cid"> <!-- identifier of the comment -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </element>
            <element name="text"> <!-- text of the comment -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">1024</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'uid' => 'user identifier', 
    'username' => 'username', 
    'fullname' => 'full name of the user', 
    'cid' => 'identifier of the comment', 
    'text' => 'text of the comment'
)
```

** **

**```bool eraseComment(string $commentId, int $userId)```**

**Description**  
Erases the comment.

**Parameters**  
*```commentId```* - identifier of the comment.  
*```userId```* - identifier of the user who owns the comment.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```void outputFormat(string $output = 'xml')```**

**Description**  
Sets format of output data.

**Parameters**  
*```output```* - can get values *json*, or *xml*.

**[>>> Contents <<<](en.index)**