**[>>> Contents <<<](en.index)**

## Module extclass.php ##

### General description ###

Includes classes that are used to extend other classes.

### Methods of class ```ServiceMethods``` ###

**```int errId()```**

**Description**  
Returns code of last occurred errors.

**Return values**  
Integer that matches to constants of view ```ERROR_*```. Value 0 means absence of error.

** **

**```string errExp()```**

**Description**  
Returns description of last occurred errors.

**Return values**  
A string with description of error. Empty string means absence of error.

** **

**```void applyPolicy(bool $flag = TRUE)```**

**Description**  
Sets value of flag of application of access policy. It is applied through whole class.

**Parameters**  
*```flag```* - flag of application of access policy. Policies are applied if value is ```TRUE```. Policies are not applied if value is ```FALSE```.

** **

**```bool newLogRecord(string $plugin, string $keyword, string $insertion = '')```**

**Description**  
Makes an event record to the log.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```keyword```* - keyword that matches to defined plugin event. A string that matches pattern *[a-zA-Z\d_]{3,30}*.  
*```insertion```* - description of event that is inserted to general pattern. A string of 1024 characters length.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```int checkFuncAccess(string $plugin, string $func, int $userId = 0)```**

**Description**  
Checks access value for the user groups by the specified plugin and function, and also by the user identifier if it's needed.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```func```* - name of plugin function.  
*```userId```* - user identifier.

**Return values**  
Returns ```1``` if access is allowed and returns ```0``` if access is restricted. In case of failure returns ```FALSE```. Returns value for user group if session is authenticated. Otherwise returns value for nonauthenticated session.  
While active session, if the value of ```userId``` is equal to the identifier of the user, then returns 1. If the value of ```userId``` isn't equal to the identifier of the user, then access value is checked by the group policies.

** **

**```void outputFormat(string $output = 'xml')```**

**Description**  
Sets format output data.

**Parameters**  
*```output```* - it may get values *json*, *xml* or *array*.

**[>>> Contents <<<](en.index)**