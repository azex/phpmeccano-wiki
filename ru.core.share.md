**[>>> Содержание <<<](ru.index)**

## Модуль share.php ##

### Общее описание ###

Содержит класс ```Share```. Предназначен для организации обмена сообщениями и файлами по типу простой социальной сети.

### Методы класса ```Share``` *(class ```Share``` extends ```Discuss```)* ###

**```__construct(\mysqli $dbLink)```**

**Описание**  
Устанавливает связь с базой данных.

**Принимаемые значения**  
*```dbLink```* - объект ```mysqli```.

** **

**```string createCircle(int $userId, string $name)```**

**Описание**  
Создает новый круг контактов пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, для которого создаётся круг.  
*```name```* - имя круга.

**Возвращаемые значения**  
В случае успеха, возвращает строковый идентификатор (GUID), созданного круга, иначе возвращает ```FALSE```.

** **

**```string / DOMDocument / bool userCircles(int $userId)```**

**Описание**  
Возвращает список кругов пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя.

**Возвращаемые значения**  
В случае успеха возвращает список кругов пользователя в виде объекта ```DOMDocument```, строки в формате JSON, либо массива ,иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "string",
                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
            },
            "name": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            }
        },
        "required": ["id", "name"]
    }
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="circles">
            <zeroOrMore>
                <element name="circle"> <!-- блок круга контактов -->
                    <attribute name="id"> <!-- идентификатор круга контактов -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </attribute>
                    <attribute name="name"> <!-- имя круга контактов -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </attribute>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    array(
        'id' => 'идентификатор круга контактов',
        'name' => 'имя круга контактов'
    ),
    array(...),
    ...
)
```

** **

**```bool renameCircle(int $userId, string $circleId, string $newName)```**

**Описание**  
Переименовывает круг контактов пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя.  
*```circleId```* - идентификатор (GUID) круга.  
*```newName```* - новое имя круга.

**Возвращаемые значение**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool addToCircle(int $contactId, string $circleId, int $userId)```**

**Описание**  
Добавляет другого пользователя в круг контактов.

**Принимаемые значение**  
*```contactId```* - идентификатор пользователя, который добавляется в круг в качестве нового контакта.  
*```circleId```* - идентификатор (GUID) круга, в который добавляется контакт.  
*```userId```* - идентификатор пользователе, в круг которого добавляется контакт.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```string / DOMDocument / bool circleContacts(int $userId, string $circleId)```**

**Описание**  
Возвращает список пользователей из круга контактов.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, которому принадлежит круг.  
*```circleId```* - идентификатор (GUID) круга.

**Возвращаемые значения**  
В случае успеха возвращает список контактов, содержащихся в круге, в виде объекта ```DOMDocument```, строки в формате JSON, либо массива ,иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "cid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "cname": {
            "type": "string",
            "minlength": 1,
            "maxlength": 50
        },
        "contacts": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1,
                        "exclusiveMinimum": false
                    },
                    "username": {
                        "type": "string",
                        "pattern": "[a-zA-Z0-9_]{3,20}"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    }
                },
                "required": ["id", "username", "fullname"]
            }
        }
    },
    "required": ["cid", "cname", "contacts"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="contacts">
            <attribute name="cid"> <!-- идентификатор круга -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <attribute name="cname"> <!-- имя круга -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">50</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="contact"> <!-- блок контакта -->
                    <element name="id"> <!-- идентификатор контакта -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="username"> <!-- имя контакта -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- полное имя контакта -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'cid' => 'идентификатор круга',
    'cname' => 'имя круга',
    'contacts' => array(
        array(
            'id' => 'идентификатор контакта',
            'username' => 'имя контакта',
            'fullname' => 'полное имя контакта'
        ),
        array(...),
        ...
    )
)
```

** **

**```bool rmFromCircle(int $userId, string $circleId, int $contactId)```**

**Описание**  
Удаляет пользователя из круга контактов.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, из круга которого удаляется контакт.  
*```circleId```* - идентификатор (GUID) круг.  
*```contactId```* - идентификактор пользователя, которого удаляют из круга.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool delCircle(int $userId, string $circleId)```**

**Описание**  
Удаляет круг контактов пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя.  
*```circleId```* - идентификатор круга.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```string createMsg(int $userId, string $title, string $text)```**

**Описание**  
Создает сообщение пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, от имени которого создаётся сообщение.  
*```title```* - заголовок сообщения.  
*```text```* - текст сообщения.

**Возвращаемые значения**  
Идентификатор (GUID) созданного сообщения, иначе ```FALSE```.

** **

**```string stageFile(string $file, string $filename, int $userid, string $title = '', string $comment = '')```**

**Описание**  
Перемещает, выгруженный на сервер, файл из временной директории и размещает его за именем указанного пользователя.

**Принимаемые значения**  
*```file```* - путь до выгруженного файла во временной директории.  
*```filename```* - имя файла, с которым он будет отдаваться при скачивании.  
*```userId```* - идентификатор пользователя, за именем которого размещается файл.  
*```title```* - краткий заголовок файла.
*```comment```* - краткий комментарий к файлу.

**Возвращаемые значения**  
Идентификатор (GUID) размещённого файла, иначе возвращает ```FALSE```.

** **

**```bool shareFile(string $fileId, int $userId, array $circles)```**

**Описание**  
Определяет права доступа к файлу среди кругов контактов пользователя для того, чтобы им можно было поделиться с другими пользователями.

**Принимаемые значения**  
*```fileId```* - идентификатор (GUID) файла.  
*```userId```* - идентификатор пользователя, которому принадлежит файл.  
*```circles```* - массив вида ```array('идентификатор_круга' => bool ...)```.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```string / DOMDocument / array / bool getFileInfo($fileId)```**

**Описание**  
Возвращает информацию о файле.

**Принимаемые значения**  
*```fileId```* - идентификатор файла (GUID).  

**Возвращаемые значения**  
В случае успеха возвращает информацию о файле в виде объекта ```DOMDocument```, строки в формате JSON, либо массива ,иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "id": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "fullname": {
            "type": ["string", "null"],
            "maxLength": 100
        },
        "title": {
            "type": ["string", "null"],
            "maxLength": 255
        },
        "filename": {
            "type": "string",
            "maxLength": 255
        },
        "comment": {
            "type": ["string", "null"],
            "maxLength": 1024
        },
        "mime": {
            "type": "string",
            "pattern": "^[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})\/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})$"
        },
        "size": {
            "type": "integer",
            "minimum": 1
        },
        "time": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        }
    },
    "required": ["id", "username", "fullname", "title", "filename", "comment", "mime", "size", "time"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="fileinfo">
            <attribute name="id"> <!-- идентификатор файла -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <element name="username"> <!-- имя пользователя, загрузившего файл -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </element>
            <element name="fullname"> <!-- полное имя пользователя, загрузившего файл -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="title"> <!-- заголовок файла -->
                <data type="string">
                    <param name="maxLength">255</param>
                </data>
            </element>
            <element name="filename"> <!-- имя файла  -->
                <data type="string">
                    <param name="maxLength">255</param>
                </data>
            </element>
            <element name="comment"> <!-- комментарий к файлу -->
                <data type="string">
                    <param name="maxLength">1024</param>
                </data>
            </element>
            <element name="mime"> <!-- mime тип файла -->
                <data type="string">
                    <param name="pattern">[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})</param>
                </data>
            </element>
            <element name="size"> <!-- размер файла в байтах -->
                <data type="positiveInteger" />
            </element>
            <element name="time"> <!-- время выгрузки файла -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'id' => 'идентификатор файла',
    'username' => 'имя пользователя, загрузившего файл',
    'fullname' => 'полное имя пользователя, загрузившего файл',
    'title' => 'заголовок файла',
    'filename' => 'имя файла',
    'comment' => 'комментарий к файлу',
    'mime' => 'mime тип файла',
    'size' => размер_файла_в_байтах,
    'time' => 'время выгрузки файла'
)
```

** **

**```bool getFile(string $fileId, string $disp = 'inline', bool $nocache = FALSE)```**

**Описание**  
Проверяет права на доступ и возвращает файл при помощи *mod_xsendfile* (для *Apache2*) и *X-Accel-Redirect* (для *NGINX*) или *X-LIGHTTPD-send-file* (для *lighttpd*).

**Принимаемые значения**  
*```fileId```* - идентификатор (GUID) файла.  
*```disp```* - может принимать значения ```'inline'``` и ```'attachment'```. Указывает значение заголовка Content-Disposition.  Значение ```'inline'``` указывает, что контент может быть отображён внутри страницы браузера. А значение ```'attachment'``` указывает, что контент должен быть скачен, большенство браузеров предоставляют для этого диалог 'Сохранить как'.  
*```nocache```* - флаг, указывающий веб-браузеру, что он не должен кешировать полученый файл, если значение ```TRUE```. Если значение ```FALSE```, браузер должен кешировать полученый файл.

**Возвращаемые значения**  
В случае успеха возвращает запрашиваемый файл, иначе возвращает страницу с одним из HTTP статусов:  
```400 Bad Request```, если входящие параметры неверны;  
```403 Forbidden```, если в доступе отказано;  
```404 Not Found```, если файл не обнаружен;  
```501 Not Implemented```, если метод исполняется на неподдерживаемом веб-сервере;  
```503 Service Unavailable```, если на сервере возникла какая-либо ошибка.  
При попытке выполнения не на веб-сервере возвращается ```FALSE```.

** **

**```bool attachFile( string $fileId, string $msgId, int $userId)```**

**Описание**  
Прикрепляет файл к сообщению.

**Принимаемые значения**  
*```fileId```* - идентификатор (GUID) файла.  
*```msgId```* - идентификатор (GUID) сообщения.  
*```userId```* - идентификатор пользователя, которому принадлежит файл и сообщение.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool unattachFile(string $fileId, string $msgId, int $userId)```**

**Описание**  
Открепляет файл от сообщения.

**Принимаемые значения**  
*```fileId```* - идентификатор (GUID) файла.  
*```msgId```* - идентификатор (GUID) сообщения.  
*```userId```* - идентификатор пользователя, которому принадлежит файл и сообщение.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool delFile(string $fileId, int $userId, bool $force = FALSE)```**

**Описание**  
Удаляет размещённый файл.

**Принимаемые значения**  
*```fileId```* - идентификатор (GUID) файла.  
*```userId```* - идентификатор пользователя, которому принадлежит файл.  
*```force```* - при значении ```FALSE```, файл будет удалён только если от не прикреплён к сообщению. При значении ```TRUE```, файл будет удалён даже если он прикреплён к сообщению.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool shareMsg(string $msgId, int $userId, array $circles)```**

**Описание**  
Определяет права доступа к сообщению среди кругов контактов пользователя для того, чтобы им можно было поделиться с другими пользователями.

**Принимаемые значения**  
*```msgId```* - идентификатор (GUID) файла.  
*```userId```* - идентификатор пользователя, которому принадлежит сообщение.  
*```circles```* - массив вида ```array('идентификатор_круга' => bool ...)```.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```string / DOMDocument / bool getMsg(string $msgId)```**

**Описание**  
Возвращает данные сообщения.

**Принимаемые значения**  
*```msgId```* - идентификатор сообщения (GUID).

**Возвращаемые значения**  
В случае успеха возвращает данные сообщения в виде объекта ```DOMDocument```, строки в формате JSON, либо массива ,иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "id": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "source": {
            "oneOf": [
                {
                    "type": ["string", "null"],
                    "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                },
                {
                    "type": "string",
                    "length": 0
                }
            ]
        },
        "title": {
            "type": "string",
            "maxLength": 250
        },
        "text": {
            "type": "string",
            "maxLength": 65535
        },
        "time": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "username": {
            "type": "string",
            "pattern": "[a-zA-Z0-9_]{3,20}"
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        }
    },
    "required": ["id", "source", "title", "text", "time", "username", "fullname"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="message"> <!-- блок сообщения -->
            <attribute name="id"> <!-- идентификатор сообщения -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <choice> <!-- идентификатор исходного сообщения -->
                <element name="source">
                    <data type="string">
                        <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                    </data>
                </element>
                <element name="source">
                    <data type="string">
                        <param name="length">0</param>
                    </data>
                </element>
            </choice>
            <element name="title"> <!-- заголовок сообщения -->
                <data type="string">
                    <param name="maxLength">250</param>
                </data>
            </element>
            <element name="text"> <!-- текст сообщения -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="time"> <!-- время создания сообщения -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="username"> <!-- имя пользователя, от имени которого было создано сообщение -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </element>
            <element name="fullname"> <!-- полное имя пользователя, от имени которого было создано сообщение -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'id' => 'идентификатор сообщения',
    'source' => 'идентификатор исходного сообщения',
    'title' => 'заголовок сообщения',
    'text' => 'текст сообщения',
    'time' => 'время создания сообщения',
    'username' => 'имя пользователя, от имени которого было создано сообщение',
    'fullname' => 'полное имя пользователя, от имени которого было создано сообщение'
)
```

** **

**```string / DOMDocument / bool msgFiles(string $msgId)```**

**Описание**  
Возвращает список файлов, прикреплённых к сообщению.

**Принимаемые значения**  
*```msgId```* - идентификатор сообщения (GUID).

**Возвращаемые значения**  
В случае успеха возвращает список файлов, прикреплённых к сообщению, в виде объекта ```DOMDocument```, строки в формате JSON, либо массива ,иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "msgid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "files": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 255
                    },
                    "filename": {
                        "type": "string",
                        "minLength": 3,
                        "maxLength": 255
                    },
                    "mime": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})\/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})$"
                    }
                },
                "required": ["id", "title", "filename", "mime"]
            }
        }
    },
    "required": ["msgid", "files"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="files">
            <attribute name="msgid"> <!-- идентификатор сообщения -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="file"> <!-- блок данных файла -->
                    <element name="id"> <!-- идентификатор файла -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="title"> <!-- заголовок файла -->
                        <data type="string">
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="filename"> <!-- имя файла -->
                        <data type="string">
                            <param name="minLength">3</param>
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="mime"> <!-- MIME-тип файла -->
                        <data type="string">
                            <param name="pattern">[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'msgid' => '',
    'files' => array(
        array(
            'id' => 'идентификатор файла',
            'title' => 'заголовок файла',
            'filename' => 'имя файла',
            'mime' => 'MIME-тип файла'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool getFileShares(string $fileId, int $userId)```**

**Описание**  
Возвращает данные о правах доступа к файлу.

**Принимаемые значения**  
*```fileId```* - идентификатор файла (GUID).  
*```userId```* - идентификатор пользователя, которому принадлежит файл.

**Возвращаемые значения**  
В случае успеха возвращает данные о правах доступа к файлу в виде объекта ```DOMDocument```, строки в формате JSON, либо массива ,иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "fileId": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "circles": {
            "type": "array",
            "items": {
                "type": "object",
                "oneOf": [
                    {
                        "properties": {
                            "id": {
                                "type": "string",
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            },
                            "name": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 50
                            },
                            "access": {
                                "type": "integer",
                                "minimum": 0,
                                "maximum": 1
                            }
                        }
                    },
                    {
                        "properties": {
                            "id": {
                                "type": "string",
                                "pattern": "^public$"
                            },
                            "name": {
                                "type": "string",
                                "length": 0
                            },
                            "access": {
                                "type": "integer",
                                "minimum": 0,
                                "maximum": 1
                            }
                        }
                    }
                ],
                "required": ["id", "name", "access"]
            }
        }
    },
    "required": ["fileId", "circles"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="shares">
            <attribute name="fileId"> <!-- идентификатор файла -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <choice>
                    <element name="circle"> <!-- первый вариант блока круга контактов пользователя -->
                        <element name="id"> <!-- идентификатор круга -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="name"> <!-- имя круга -->
                            <data type="string">
                                <param name="minLength">1</param>
                                <param name="maxLength">50</param>
                            </data>
                        </element>
                        <element name="access"> <!-- значение доступа к файлу -->
                            <choice>
                                <value>0</value>
                                <value>1</value>
                            </choice>
                        </element>
                    </element>
                    <element name="circle"> <!-- второй вариант блока круга контактов пользователя -->
                        <element name="id"> <!-- идентификатор круга публичного доступа -->
                            <data type="string">
                                <param name="pattern">public</param>
                            </data>
                        </element>
                        <element name="name"> <!-- пустое имя круга -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="access"> <!-- значение доступа к файлу -->
                            <choice>
                                <value>0</value>
                                <value>1</value>
                            </choice>
                        </element>
                    </element>
                </choice>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'fileId' => 'идентификатор файла',
    'circles' => array(
        array(
            'id' => 'идентификатор круга',
            'name' => 'имя круга',
            'access' => значение_доступа_к_файлу
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool getMsgShares(string $msgId, int $userId)```**

**Описание**  
Возвращает данные о правах доступа к сообщению.

**Принимаемые значения**  
*```msgId```* - идентификатор сообщения (GUID).  
*```userId```* - идентификатор пользователя, которому принадлежит сообщение.

**Возвращаемые значения**  
В случае успеха возвращает данные о правах доступа к сообщению в виде объекта ```DOMDocument```, строки в формате JSON, либо массива ,иначе возвращает ```FALSE```. 

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "msgId": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "circles": {
            "type": "array",
            "items": {
                "type": "object",
                "oneOf": [
                    {
                        "properties": {
                            "id": {
                                "type": "string",
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            },
                            "name": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 50
                            },
                            "access": {
                                "type": "integer",
                                "minimum": 0,
                                "maximum": 1
                            }
                        }
                    },
                    {
                        "properties": {
                            "id": {
                                "type": "string",
                                "pattern": "^public$"
                            },
                            "name": {
                                "type": "string",
                                "length": 0
                            },
                            "access": {
                                "type": "integer",
                                "minimum": 0,
                                "maximum": 1
                            }
                        }
                    }
                ],
                "required": ["id", "name", "access"]
            }
        }
    },
    "required": ["msgId", "circles"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="shares">
            <attribute name="msgId"> <!-- идентификатор сообщения -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <choice>
                    <element name="circle"> <!-- первый вариант блока круга контактов пользователя -->
                        <element name="id"> <!-- идентификатор круга -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="name"> <!-- имя круга -->
                            <data type="string">
                                <param name="minLength">1</param>
                                <param name="maxLength">50</param>
                            </data>
                        </element>
                        <element name="access"> <!-- значение доступа к сообщению -->
                            <choice>
                                <value>0</value>
                                <value>1</value>
                            </choice>
                        </element>
                    </element>
                    <element name="circle"> <!-- второй вариант блока круга контактов пользователя -->
                        <element name="id"> <!-- идентификатор круга публичного доступа -->
                            <data type="string">
                                <param name="pattern">public</param>
                            </data>
                        </element>
                        <element name="name"> <!-- пустое имя круга -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="access"> <!-- значение доступа к сообщению -->
                            <choice>
                                <value>0</value>
                                <value>1</value>
                            </choice>
                        </element>
                    </element>
                </choice>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'msgId' => 'идентификатор сообщения',
    'circles' => array(
        array(
            'id' => 'идентификатор круга',
            'name' => 'имя круга',
            'access' => значение_доступа_к_сообщению
        ),
        array(...),
        ...
    )
)
```

** **

**```bool editFile(string $fileId, int $userId, string $title, string $comment)```**

**Описание**  
Редактирует информацию о файле.

**Принимаемые значения**  
*```fileId```* - идентификатор файла.  
*```userId```* - идентификатори пользователя.  
*```title```* - новый краткий заголовок файла.  
*```comment```* - новый краткий комментарий к файлу.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```array / bool repostMsg(string $msgId, int $userId, bool $hlink = TRUE)```**

**Описание**  
Производит репост сообщения со всеми файлами, прикреплёнными к сообщению, к которым предоставлен доступ.

**Принимаемые значения**  
*```msgId```* - идентификатор копируемого сообщения.  
*```userId```* - идентификатор пользователя, производящего репост.  
*```hlink```* - флаг, который указывает на то, нужно ли создавать жёсткие ссылки на файлы, прикреплённые к сообщению (позволяет сэкономить дисковое пространство), или же нужно производить копирование этих файлов. При значении ```TRUE```, создаются жёсткие ссылки. При значении ```FALSE```, файлы копируются.

**Возвращаемые значения**  
При успешном репосте сообщения возвращается массив с информацией о копии сообщения вида ```array('message' => 'идентификатор_сообщения', 'files' => array_values('идентификатор_файла_1', 'идентификатор_файла_2' ...))```.

** **

**```bool editMsg(string $msgId, int $userId, string $title, string $text)```**

**Описание**  
Редактирует сообщение.

**Принимаемые значения**  
*```msgId```* - идентификатор сообщения.  
*```userId```* - идентификатор пользователя, которому принадлежит сообщение.  
*```title```* - заголовок сообщения.  
*```text```* - текст сообщения.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool delMsg($msgId, $userId, $keepFiles = TRUE)```**

**Описание**  
Удаляет сообщение.

**Принимаемые значения**  
*```msgId```* - идентификатор удаляемого сообщения.  
*```userId```* - идентификатор пользователя, которому принадлежит сообщение.  
*```keepFiles```* - флаг, который указывает на то, нужно ли сохранить файлы, прикреплённые к сообщению. При значении ```TRUE```, файлы будут сохранены. При значении ```FALSE```, файлы, прикреплённые к сообщению, будут удалены вместе с ним.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```string / bool repostFile($fileId, $userId, $hlink = TRUE)```**

**Описание**  
Производит репост файла.

**Принимаемые значения**  
*```fileId```* - идентификатор копируемого файла.  
*```userId```* - идентификатор пользователя, производящего репост.  
*```hlink```* - глаг, который указывает на то, нужно ли создать жёсткую ссылку на файл (позволяет сэкономить дисковое пространство), или же нужно произвести копирование этого файла. При значении ```TRUE```, создаётся жёсткая ссылка. При значении ```FALSE```, файл копируется.

**Возвращаемые значения**  
В случае успеха возвращает идентификатор копии файла (GUID), иначе ```FALSE```.

** **

**```array / bool sumUserMsgs(int $userId, int $rpp = 20)```**

**Описание**  
Возвращает обобщенные данные о сообщениях, опубликованных пользователем, постранично.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, данные о сообщениях которого запрашиваются.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
Массив вида ```array('records' => int $totalRecs, 'pages' => int $totalPages)```, где ```totalRecs``` - общее количество сообщения, ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalRecs/rpp```. Данные выводятся с учётом прав доступа. В случае неудачи возвращает ```FALSE```.

** **

**```string / DOMDocument / bool userMsgs(int $userId, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = array('time'), bool $ascent = FALSE)```**

**Описание**  
Возвращает страницу с сообщениями пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, чьи сообщения запрашиваются  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalPages```* - общее количество страниц.  
*```rpp```* - желаемое максимальное количество записей на странице.  
*```orderBy```* - параметры сортировки вывода сообщений пользователя. Допустимые значения параметров: ```'time'```, ```'title'```. Последовательность параметров задаётся как массив, например, ```array('title', 'time')```. Некорректно заданная последовательность параметров приравнивается к ```array('time')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает сообщения пользователя в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "source", "title", "text", "time"]
            }
        }
    },
    "required": ["username", "uid", "fullname", "messages"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="username"> <!-- имя пользователя -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- идентификатор пользователя -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- полное имя пользователя -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- блок сообщения -->
                    <element name="id"> <!-- идентификатор сообщения -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- источник сообщения -->
                        <element name="source"> <!-- сообщение не имеет источника -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- сообщение имеет источник -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- заголовок сообщения -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- текст сообщения -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания сообщения -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'username' => 'имя пользователя',
    'uid' => идентификатор_пользователя,
    'fullname' => 'полное имя пользователя',
    'messages' => array(
        array(
            'id' => 'идентификатор сообщения',
            'source' => 'источник сообщения',
            'title' => 'заголовок сообщения',
            'text' => 'текст сообщения',
            'time' => 'время создания сообщения'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool msgStripe(int $userId, int $rpp = 20)```**

**Описание**  
Возвращает ленту сообщений пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, чьи сообщения запрашиваются.  
*```rpp```* - желаемое максимальное количество записей.

**Возвращаемые значения**  
В случае успеха возвращает ленту сообщений пользователя в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "source", "title", "text", "time"]
            }
        },
        "maxmark": {
            "type": "number"
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["username", "uid", "fullname", "messages", "minmark", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="username"> <!-- имя пользователя -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- идентификатор пользователя -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- полное имя пользователя -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- блок сообщения -->
                    <element name="id"> <!-- идентификатор сообщения -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- источник сообщения -->
                        <element name="source"> <!-- сообщение не имеет источника -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- сообщение имеет источник -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- заголовок сообщения -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- текст сообщения -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания сообщения -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'username' => 'имя пользователя',
    'uid' => идентификатор_пользователя,
    'fullname' => 'полное имя пользователя',
    'messages' => array(
        array(
            'id' => 'идентификатор сообщения',
            'source' => 'источник сообщения',
            'title' => 'заголовок сообщения',
            'text' => 'текст сообщения',
            'time' => 'время создания сообщения',
        ),
        array(...),
        ...
    ),
    'minmark' => минимальная.метка,
    'maxmark' => максимальная.метка
)
```

** **

**```string / DOMDocument / bool appendMsgStripe(int $userId, double $minMark, int $rpp = 20)```**

**Описание**  
Возвращает ленту сообщений пользователя, следующих за минимальной меткой.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, чьи сообщения запрашиваются.  
*```minMark```* - минимальная метка.  
*```rpp```* - желаемое максимальное количество записей.

**Возвращаемые значения**  
В случае успеха возвращает ленту сообщений пользователя в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "source", "title", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["username", "uid", "fullname", "messages", "minmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="username"> <!-- имя пользователя -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- идентификатор пользователя -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- полное имя пользователя -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- блок сообщения -->
                    <element name="id"> <!-- идентификатор сообщения -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- источник сообщения -->
                        <element name="source"> <!-- сообщение не имеет источника -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- сообщение имеет источник -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- заголовок сообщения -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- текст сообщения -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания сообщения -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'username' => 'имя пользователя',
    'uid' => идентификатор_пользователя,
    'fullname' => 'полное имя пользователя',
    'messages' => array(
        array(
            'id' => 'идентификатор сообщения',
            'source' => 'источник сообщения',
            'title' => 'заголовок сообщения',
            'text' => 'текст сообщения',
            'time' => 'время создания сообщения',
        ),
        array(...),
        ...
    ),
    'minmark' => минимальная.метка
)
```

** **

**```string / DOMDocument / bool updateMsgStripe(int $userId, double $maxMark)```**

**Описание**  
Возвращает ленту сообщений пользователя, идущих перед максимальной меткой.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, чьи сообщения запрашиваются.  
*```maxMark```* - максимальная метка.

**Возвращаемые значения**  
В случае успеха возвращает ленту сообщений пользователя в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "source", "title", "text", "time"]
            }
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["username", "uid", "fullname", "messages", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="username"> <!-- имя пользователя -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- идентификатор пользователя -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- полное имя пользователя -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- блок сообщения -->
                    <element name="id"> <!-- идентификатор сообщения -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- источник сообщения -->
                        <element name="source"> <!-- сообщение не имеет источника -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- сообщение имеет источник -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- заголовок сообщения -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- текст сообщения -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания сообщения -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'username' => 'имя пользователя',
    'uid' => идентификатор_пользователя,
    'fullname' => 'полное имя пользователя',
    'messages' => array(
        array(
            'id' => 'идентификатор сообщения',
            'source' => 'источник сообщения',
            'title' => 'заголовок сообщения',
            'text' => 'текст сообщения',
            'time' => 'время создания сообщения',
        ),
        array(...),
        ...
    ),
    'maxmark' => максимальная.метка
)
```

** **

**```array / bool sumUserFiles(int $userId, int $rpp = 20)```**

**Описание**  
Возвращает обобщенные данные о файлах, размещённых пользователем, постранично.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, данные о файлах которого запрашиваются.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
Массив вида ```array('records' => int $totalRecs, 'pages' => int $totalPages)```, где ```totalRecs``` - общее количество файлов, ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalRecs/rpp```. Данные выводятся с учётом прав доступа. В случае неудачи возвращает ```FALSE```.

** **

**```string / DOMDocument / bool userFiles(int $userId, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = array('time'), bool $ascent = FALSE)```**

**Описание**  
Возвращает страницу с файлами пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, чьи файлы запрашиваются.  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalPages```* - общее количество страниц.  
*```rpp```* - желаемое максимальное количество записей на странице.  
*```orderBy```* - параметры сортировки вывода файлов пользователя. Допустимые значения параметров: ```'time'```, ```'title'```. Последовательность параметров задаётся как массив, например, ```array('title', 'time')```. Некорректно заданная последовательность параметров приравнивается к ```array('time')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает файлы пользователя в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "files": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 255
                    },
                    "filename": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 255
                    },
                    "comment": {
                        "type": "string",
                        "maxLength": 1024
                    },
                    "mime": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})\/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})$",
                        "maxLength": 50
                    },
                    "size": {
                        "type": "integer",
                        "minimal": 1
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "title", "filename", "comment", "mime", "size", "time"]
            }
        }
    },
    "required": ["username", "uid", "fullname", "files"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="files">
            <attribute name="username"> <!-- имя пользователя -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- идентификатор пользователя -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- полное имя пользователя -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="file"> <!-- блок файла -->
                    <element name="id"> <!-- идентификатор файла -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="title"> <!-- заголовок файла -->
                        <data type="string">
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="filename"> <!-- имя файла -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="comment"> <!-- комментарий к файлу -->
                        <data type="string">
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="mime"> <!-- MIME-тип файла -->
                        <data type="string">
                            <param name="pattern">[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})</param>
                        </data>
                    </element>
                    <element name="size"> <!-- размер файла в байтах -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- время размещения файла -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'username' => 'имя пользователя',
    'uid' => идентификатор_пользователя,
    'fullname' => 'полное имя пользователя',
    'files' => array(
        array(
            'id' => 'идентификатор файла',
            'title' => 'заголовок файла',
            'filename' => 'имя файла',
            'comment' => 'комментарий к файлу',
            'mime' => 'mime тип файла',
            'size' => размер_файла_в_байтах,
            'time' => 'время выгрузки файла'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool fileStripe(int $userId, int $rpp = 20)```**

**Описание**  
Возвращает ленту файлов пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, чьи файлы запрашиваются.  
*```rpp```* - желаемое максимальное количество записей.

**Возвращаемые значения**  
В случае успеха возвращает сообщения пользователя в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "files": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 255
                    },
                    "filename": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 255
                    },
                    "comment": {
                        "type": "string",
                        "maxLength": 1024
                    },
                    "mime": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})\/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})$",
                        "maxLength": 50
                    },
                    "size": {
                        "type": "integer",
                        "minimal": 1
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "title", "filename", "comment", "mime", "size", "time"]
            }
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["username", "uid", "fullname", "files", "minmark", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="files">
            <attribute name="username"> <!-- имя пользователя -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- идентификатор пользователя -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- полное имя пользователя -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="file"> <!-- блок файла -->
                    <element name="id"> <!-- идентификатор файла -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="title"> <!-- заголовок файла -->
                        <data type="string">
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="filename"> <!-- имя файла -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="comment"> <!-- комментарий к файлу -->
                        <data type="string">
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="mime"> <!-- MIME-тип файла -->
                        <data type="string">
                            <param name="pattern">[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})</param>
                        </data>
                    </element>
                    <element name="size"> <!-- размер файла в байтах -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- время размещения файла -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'username' => 'имя пользователя',
    'uid' => идентификатор_пользователя,
    'fullname' => 'полное имя пользователя',
    'files' => array(
        array(
            'id' => 'идентификатор файла',
            'title' => 'заголовок файла',
            'filename' => 'имя файла',
            'comment' => 'комментарий к файлу',
            'mime' => 'mime тип файла',
            'size' => размер_файла_в_байтах,
            'time' => 'время выгрузки файла'
        ),
        array(...),
        ...
    ),
    'minmark' => минимальная.метка,
    'maxmark' => максимальная.метка
)
```

** **

**```string / DOMDocument / bool appendFileStripe(int $userId, double $minMark, int $rpp = 20)```**  

**Описание**  
Возвращает ленту файлов пользователя, следующих за минимальной меткой.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, чьи файлы запрашиваются.  
*```minMark```* - минимальная метка.  
*```rpp```* - желаемое максимальное количество записей.

**Возвращаемые значения**  
В случае успеха возвращает ленту файлов пользователя в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "files": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 255
                    },
                    "filename": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 255
                    },
                    "comment": {
                        "type": "string",
                        "maxLength": 1024
                    },
                    "mime": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})\/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})$",
                        "maxLength": 50
                    },
                    "size": {
                        "type": "integer",
                        "minimal": 1
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "title", "filename", "comment", "mime", "size", "time"]
            }
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["username", "uid", "fullname", "files", "minmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="files">
            <attribute name="username"> <!-- имя пользователя -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- идентификатор пользователя -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- полное имя пользователя -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="file"> <!-- блок файла -->
                    <element name="id"> <!-- идентификатор файла -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="title"> <!-- заголовок файла -->
                        <data type="string">
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="filename"> <!-- имя файла -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="comment"> <!-- комментарий к файлу -->
                        <data type="string">
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="mime"> <!-- MIME-тип файла -->
                        <data type="string">
                            <param name="pattern">[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})</param>
                        </data>
                    </element>
                    <element name="size"> <!-- размер файла в байтах -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- время размещения файла -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'username' => 'имя пользователя',
    'uid' => идентификатор_пользователя,
    'fullname' => 'полное имя пользователя',
    'files' => array(
        array(
            'id' => 'идентификатор файла',
            'title' => 'заголовок файла',
            'filename' => 'имя файла',
            'comment' => 'комментарий к файлу',
            'mime' => 'mime тип файла',
            'size' => размер_файла_в_байтах,
            'time' => 'время выгрузки файла'
        ),
        array(...),
        ...
    ),
    'minmark' => минимальная.метка
)
```

** **

**```string / DOMDocument / bool updateFileStripe(int $userId, double $maxMark)```**

**Описание**  
Возвращает ленту файлов пользователя, идущих перед максимальной меткой.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, чьи файлы запрашиваются.  
*```maxMark```* - максимальная метка.

**Возвращаемые значения**  
В случае успеха возвращает ленту файлов пользователя в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "fullname": {
            "type": "string",
            "maxLength": 100
        },
        "files": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 255
                    },
                    "filename": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 255
                    },
                    "comment": {
                        "type": "string",
                        "maxLength": 1024
                    },
                    "mime": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})\/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})$",
                        "maxLength": 50
                    },
                    "size": {
                        "type": "integer",
                        "minimal": 1
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["id", "title", "filename", "comment", "mime", "size", "time"]
            }
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["username", "uid", "fullname", "files", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="files">
            <attribute name="username"> <!-- имя пользователя -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </attribute>
            <attribute name="uid"> <!-- идентификатор пользователя -->
                <data type="positiveInteger" />
            </attribute>
            <attribute name="fullname"> <!-- полное имя пользователя -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="file"> <!-- блок файла -->
                    <element name="id"> <!-- идентификатор файла -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="title"> <!-- заголовок файла -->
                        <data type="string">
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="filename"> <!-- имя файла -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">255</param>
                        </data>
                    </element>
                    <element name="comment"> <!-- комментарий к файлу -->
                        <data type="string">
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="mime"> <!-- MIME-тип файла -->
                        <data type="string">
                            <param name="pattern">[a-z0-9]{1,12}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})/[a-z0-9]{1,13}([-.+]{1}[a-z0-9]{1,11}|[a-z0-9]{0,12})</param>
                        </data>
                    </element>
                    <element name="size"> <!-- размер файла в байтах -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- время размещения файла -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'username' => 'имя пользователя',
    'uid' => идентификатор_пользователя,
    'fullname' => 'полное имя пользователя',
    'files' => array(
        array(
            'id' => 'идентификатор файла',
            'title' => 'заголовок файла',
            'filename' => 'имя файла',
            'comment' => 'комментарий к файлу',
            'mime' => 'mime тип файла',
            'size' => размер_файла_в_байтах,
            'time' => 'время выгрузки файла'
        ),
        array(...),
        ...
    ),
    'maxmark' => максимальная.метка
)
```

** **

**```array / bool sumUserSubs(int $userId, int $rpp = 20)```**

**Описание**  
Возвращает обобщенные данные о сообщениях, опубликованных контактами пользователя для него, постранично.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, данные о сообщениях для которого запрашиваются.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
Массив вида ```array('records' => int $totalRecs, 'pages' => int $totalPages)```, где ```totalRecs``` - общее количество сообщений, ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalRecs/rpp```. Данные выводятся с учётом прав доступа. В случае неудачи возвращает ```FALSE```.

** **

**```string / DOMDocument / bool userSubs(int $userId, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = array('time'), bool $ascent = FALSE)```**

**Описание**  
Возвращает страницу с сообщениями, опубликованными контактами пользователя для него.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, для которого запрашиваются сообщения.  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalPages```* - общее количество страниц.  
*```rpp```* - желаемое максимальное количество записей на странице.  
*```orderBy```* - параметры сортировки вывода файлов пользователя. Допустимые значения параметров: ```'time'```, ```'title'```. Последовательность параметров задаётся как массив, например, ```array('title', 'time')```. Некорректно заданная последовательность параметров приравнивается к ```array('time')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает сообщения, опубликованные контактами пользователя для него, в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "uid": {
                "type": "integer",
                "minimum": 1
            },
            "username": {
                "type": "string",
                "pattern": "^[a-zA-Z0-9_]{3,20}$"
            },
            "fullname": {
                "type": "string",
                "maxLength": 100
            },
            "id": {
                "type": "string",
                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
            },
            "source": {
                "type": "string",
                "oneOf": [
                    {
                        "length": 0
                    },
                    {
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    }
                ]
            },
            "title": {
                "type": "string",
                "maxLength": 250
            },
            "text": {
                "type": "string",
                "maxLength": 512
            },
            "time": {
                "type": "string",
                "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
            }
        },
        "required": ["uid", "username", "fullname", "id", "source", "title", "text", "time"]
    }
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <zeroOrMore>
                <element name="message"> <!-- блок сообщения -->
                    <attribute name="uid"> <!-- идентификатор пользователя -->
                        <data type="positiveInteger" />
                    </attribute>
                    <attribute name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </attribute>
                    <attribute name="fullname"> <!-- польное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </attribute>
                    <element name="id"> <!-- идентификатор сообщения -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- источник сообщения -->
                        <element name="source"> <!-- сообщение не имеет источника -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- сообщение имеет источник -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- заголовок сообщения -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- текст сообщения -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания сообщения -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    array(
        'uid' => идентификатор_пользователя,
        'username' => 'имя пользователя',
        'fullname' => 'полное имя пользователя',
        'id' => 'идентификатор сообщения',
        'source' => 'источник сообщения',
        'title' => 'заголовок сообщения',
        'text' => 'текст сообщения',
        'time' => 'время создания сообщения',
    ),
    array(...),
    ...
)
```

** **

**```string / DOMDocument / bool subStripe(int $userId, int $rpp = 20)```**

**Описание**  
Возвращает ленту сообщений, опубликованных контактами пользователя для него.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, для которого запрашиваются сообщения.  
*```rpp```* - желаемое максимальное количество записей на странице.

**Возвращаемые значения**  
В случае успеха возвращает ленту сообщений, опубликованных контактами пользователя для него, в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "uid": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": "string",
                        "maxLength": 100
                    },
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["uid", "username", "fullname", "id", "source", "title", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["messages", "minmark", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- блок сообщения -->
                    <attribute name="uid"> <!-- идентификатор пользователя -->
                        <data type="positiveInteger" />
                    </attribute>
                    <attribute name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </attribute>
                    <attribute name="fullname"> <!-- польное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </attribute>
                    <element name="id"> <!-- идентификатор сообщения -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- источник сообщения -->
                        <element name="source"> <!-- сообщение не имеет источника -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- сообщение имеет источник -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- заголовок сообщения -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- текст сообщения -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания сообщения -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'messages' => array(
        array(
            'uid' => идентификатор_пользователя,
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'id' => 'идентификатор сообщения',
            'source' => 'источник сообщения',
            'title' => 'заголовок сообщения',
            'text' => 'текст сообщения',
            'time' => 'время создания сообщения',
        ),
        array(...),
        ...
    ),
    'minmark' => минимальная.метка,
    'maxmark' => максимальная.метка
)
```

** **

**```string / DOMDocument / bool appendSubStripe(int $userId, double $minMark, int $rpp = 20)```**

**Описание**  
Возвращает ленту сообщений, опубликованных контактами пользователя для него, следующих за минимальной меткой.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, для которого запрашиваются сообщения.  
*```minMark```* - минимальная метка.  
*```rpp```* - желаемое максимальное количество записей.

**Возвращаемые значения**  
В случае успеха возвращает ленту сообщений, опубликованных контактами пользователя для него, в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "uid": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": "string",
                        "maxLength": 100
                    },
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["uid", "username", "fullname", "id", "source", "title", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["messages", "minmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- блок сообщения -->
                    <attribute name="uid"> <!-- идентификатор пользователя -->
                        <data type="positiveInteger" />
                    </attribute>
                    <attribute name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </attribute>
                    <attribute name="fullname"> <!-- польное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </attribute>
                    <element name="id"> <!-- идентификатор сообщения -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- источник сообщения -->
                        <element name="source"> <!-- сообщение не имеет источника -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- сообщение имеет источник -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- заголовок сообщения -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- текст сообщения -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания сообщения -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'messages' => array(
        array(
            'uid' => идентификатор_пользователя,
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'id' => 'идентификатор сообщения',
            'source' => 'источник сообщения',
            'title' => 'заголовок сообщения',
            'text' => 'текст сообщения',
            'time' => 'время создания сообщения',
        ),
        array(...),
        ...
    ),
    'minmark' => минимальная.метка
)
```

** **

**```string / DOMDocument / bool updateSubStripe(int $userId, double $maxMark)```**

**Описание**  
Возвращает ленту сообщений, опубликованных контактами пользователя для него, идущих перед максимальной меткой.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя, для которого запрашиваются сообщения.  
*```maxMark```* - максимальная метка.

**Возвращаемые значения**  
В случае успеха возвращает ленту сообщений, опубликованных контактами пользователя для него, в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "uid": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": "string",
                        "maxLength": 100
                    },
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["uid", "username", "fullname", "id", "source", "title", "text", "time"]
            }
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["messages", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- блок сообщения -->
                    <attribute name="uid"> <!-- идентификатор пользователя -->
                        <data type="positiveInteger" />
                    </attribute>
                    <attribute name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </attribute>
                    <attribute name="fullname"> <!-- польное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </attribute>
                    <element name="id"> <!-- идентификатор сообщения -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- источник сообщения -->
                        <element name="source"> <!-- сообщение не имеет источника -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- сообщение имеет источник -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- заголовок сообщения -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- текст сообщения -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания сообщения -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'messages' => array(
        array(
            'uid' => идентификатор_пользователя,
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'id' => 'идентификатор сообщения',
            'source' => 'источник сообщения',
            'title' => 'заголовок сообщения',
            'text' => 'текст сообщения',
            'time' => 'время создания сообщения',
        ),
        array(...),
        ...
    ),
    'maxmark' => максимальная.метка
)
```

** **

**```string / bool createMsgComment(string $msgId, int $userId, string $comment, string $parentId = '')```**

**Описание**  
Создаёт комментарий к сообщению.

**Принимаемые значения**  
*```msgId```* - идентификатор сообщения, к которому создается комментарий.  
*```userId```* - идентификатор пользователя, от имени которого создается сообщение.  
*```comment```* - текст комментария, длина до 1024 символов.  
*```parentId```* - идентификатор родительского комментария, если комментарий создаётся, как ответ на предшествующий комментарий.

**Возвращаемые значения**  
В случае успеха возвращает идентификатор созданного комментария, иначе ```FALSE```.

** **

**```bool editMsgComment(string $comment, string $commentId, int $userId)```**

**Описание**  
Редактирует комментарий к сообщению.

**Принимаемые значения**  
*```comment```* - текст комментария, длина до 1024 символов.  
*```commentId```* - идентификатор комментария.  
*```userId```* - идентификатор пользователя, которому принадлежит редактируемый комментарий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```string / DOMDocument / bool getMsgComment(sting $commentId, int $userId)```**

**Описание**  
Получает информацию о комментарии.

**Принимаемые значения**  
*```commentId```* - идентификатор комментария.  
*```userId```* - идентификатор пользователя, которому принадлежит комментарий.

**Возвращаемые значения**  
В случае успеха возвращает информацию о комментарии в виде объекта ```DOMDocument```, либо строки в формате JSON, иначе возвращает ```FALSE```.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "uid": {
            "type": "integer"
        },
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "fullname": {
            "type": ["string", "null"],
            "maxLength": 100
        },
        "cid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "text": {
            "type": "string",
            "minLength": 1,
            "maxLength": 1024
        }
    },
    "required": ["uid", "username", "fullname", "cid", "text"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comment">
            <element name="uid"> <!-- идентификатор пользователя -->
                <data type="positiveInteger" />
            </element>
            <element name="username"> <!-- имя пользователя -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </element>
            <element name="fullname"> <!-- полное имя пользователя -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="cid"> <!-- идентификатор комментария -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </element>
            <element name="text"> <!-- текст комментария -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">1024</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'uid' => 'идентификатор пользователя', 
    'username' => 'имя пользователя', 
    'fullname' => 'полное имя пользователя', 
    'cid' => 'идентификатор комментария', 
    'text' => 'текст комментария' 
)
```

** **

**```bool eraseMsgComment(string $commentId, string $userId)```**

**Описание**  
Затирает комментарий.

**Принимаемые значения**  
*```commentId```* - идентификатор комментария.  
*```userId```* - идентификатор пользователя, которому принадлежит комментарий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```string / DOMDocument / bool getMsgAllComments(string $msgId)```**

**Описание**  
Получает ленту всех комментариев к сообщению.

**Принимаемые значения**  
*```msgId```* - идентификатор сообщения.  

**Возвращаемые значения**  
В случае успеха возвращает ленту комментариев к сообщению в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "topic": {
            "type": ["string", "null"]
        },
        "tid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["comments", "topic", "tid", "minmark", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="topic"> <!-- название темы -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="tid"> <!-- идентификатор темы -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- блок комментария -->
                    <element name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- польное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- идентификатор комментария -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- идентификатор родительского комментария -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- текст комментария -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания комментария -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'topic' => 'название темы',
    'tid' => 'идентификатор темы',
    'minmark' => минимальная.метка,
    'maxmark' => максимальная.метка,
    'comments' => array(
        array(
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'cid' => 'идентификатор комментария',
            'pcid' => 'идентификатор родительского комментария',
            'text' => 'текст комментария',
            'time' => 'время создания комментария'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool getMsgComments(string $msgId, int $rpp = 20)```**

**Описание**  
Получает ленту комментариев к сообщению.

**Принимаемые значения**  
*```msgId```* - идентификатор сообщения.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
В случае успеха возвращает ленту комментариев к сообщению в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "topic": {
            "type": ["string", "null"]
        },
        "tid": {
            "type": "string",
            "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["comments", "topic", "tid", "minmark", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="topic"> <!-- название темы -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </attribute>
            <attribute name="tid"> <!-- идентификатор темы -->
                <data type="string">
                    <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                </data>
            </attribute>
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- блок комментария -->
                    <element name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- польное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- идентификатор комментария -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- идентификатор родительского комментария -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- текст комментария -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания комментария -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'topic' => 'название темы',
    'tid' => 'идентификатор темы',
    'minmark' => минимальная.метка,
    'maxmark' => максимальная.метка,
    'comments' => array(
        array(
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'cid' => 'идентификатор комментария',
            'pcid' => 'идентификатор родительского комментария',
            'text' => 'текст комментария',
            'time' => 'время создания комментария'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool appendMsgComments($msgId, $minMark, $rpp = 20)```**

**Описание**  
Получает ленту комментариев к указанному сообщению, следующие за комментарием с минимальной меткой.

**Принимаемые значения**  
*```msgId```* - идентификатор сообщения.  
*```minMark```* - минимальная метка.  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
В случае успеха возвращает ленту комментариев к сообщению в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["comments", "minmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- блок комментария -->
                    <element name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- польное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- идентификатор комментария -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- идентификатор родительского комментария -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- текст комментария -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания комментария -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'topic' => 'название темы',
    'tid' => 'идентификатор темы',
    'minmark' => минимальная.метка,,
    'comments' => array(
        array(
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'cid' => 'идентификатор комментария',
            'pcid' => 'идентификатор родительского комментария',
            'text' => 'текст комментария',
            'time' => 'время создания комментария'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool updateMsgComments(string $msgId, double $maxMark)```**

**Описание**  
Получает все комментарии к указанному сообщению, идущие перед комментарием с максимальной меткой.

**Принимаемые значения**  
*```msgId```* - идентификатор сообщения.  
*```maxMark```* - максимальная метка.

**Возвращаемые значения**  
В случае успеха возвращает ленту комментариев к сообщению в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "comments": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": ["string", "null"],
                        "maxLength": 100
                    },
                    "cid": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "pcid": {
                        "type": ["string", "null"],
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["username", "fullname", "cid", "pcid", "text", "time"]
            }
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["comments", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="comments">
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="comment"> <!-- блок комментария -->
                    <element name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- польное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="cid"> <!-- идентификатор комментария -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- идентификатор родительского комментария -->
                        <element name="pcid">
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                        <element name="pcid">
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                    </choice>
                    <element name="text"> <!-- текст комментария -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">1024</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания комментария -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'topic' => 'название темы',
    'tid' => 'идентификатор темы',
    'maxmark' => максимальная.метка,
    'comments' => array(
        array(
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'cid' => 'идентификатор комментария',
            'pcid' => 'идентификатор родительского комментария',
            'text' => 'текст комментария',
            'time' => 'время создания комментария'
        ),
        array(...),
        ...
    )
)
```

** **

**```string / DOMDocument / bool pubMsgs(int $rpp = 20)```**

**Описание**  
Возвращает ленту с публичными сообщениями ото всех пользователей

**Принимаемые значения**  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
В случае успеха возвращает ленту с сообщениями ото всех пользователей, имеюшими публичный доступ, в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "uid": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": "string",
                        "maxLength": 100
                    },
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["uid", "username", "fullname", "id", "source", "title", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        },
        "maxmark": {
            "type": "number"
        }
    },
    "required": ["messages", "minmark", "maxmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <attribute name="maxmark"> <!-- максимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- блок сообщения -->
                    <attribute name="uid"> <!-- идентификатор пользователя -->
                        <data type="positiveInteger" />
                    </attribute>
                    <attribute name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </attribute>
                    <attribute name="fullname"> <!-- польное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </attribute>
                    <element name="id"> <!-- идентификатор сообщения -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- источник сообщения -->
                        <element name="source"> <!-- сообщение не имеет источника -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- сообщение имеет источник -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- заголовок сообщения -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- текст сообщения -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания сообщения -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'messages' => array(
        array(
            'uid' => идентификатор_пользователя,
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'id' => 'идентификатор сообщения',
            'source' => 'источник сообщения',
            'title' => 'заголовок сообщения',
            'text' => 'текст сообщения',
            'time' => 'время создания сообщения',
        ),
        array(...),
        ...
    ),
    'minmark' => минимальная.метка,
    'maxmark' => максимальная.метка
)
```

** **

**```string / DOMDocument / bool appendPubMsgs(double $minMark, int $rpp = 20)```**

**Описание**  
Возвращает ленту с публичными сообщениями ото всех пользователей и следующими за сообением с минимальной меткой.

**Принимаемые значения**  
*```minMark```* - минимальная метка.  
*```rpp```* - желаемое максимальное количество записей.

**Возвращаемые значения**  
В случае успеха возвращает ленту с сообщениями ото всех пользователей, имеюшими публичный доступ и следующими за минимальной меткой, в виде объекта ```DOMDocument```, строки в формате JSON, либо массива, иначе возвращает ```FALSE```. Данные выводятся с учётом прав доступа.  

Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "messages": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "uid": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "username": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,20}$"
                    },
                    "fullname": {
                        "type": "string",
                        "maxLength": 100
                    },
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "source": {
                        "type": "string",
                        "oneOf": [
                            {
                                "length": 0
                            },
                            {
                                "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                            }
                        ]
                    },
                    "title": {
                        "type": "string",
                        "maxLength": 250
                    },
                    "text": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "time": {
                        "type": "string",
                        "pattern": "[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}"
                    }
                },
                "required": ["uid", "username", "fullname", "id", "source", "title", "text", "time"]
            }
        },
        "minmark": {
            "type": "number"
        }
    },
    "required": ["messages", "minmark"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="messages">
            <attribute name="minmark"> <!-- минимальная метка -->
                <data type="double" />
            </attribute>
            <zeroOrMore>
                <element name="message"> <!-- блок сообщения -->
                    <attribute name="uid"> <!-- идентификатор пользователя -->
                        <data type="positiveInteger" />
                    </attribute>
                    <attribute name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </attribute>
                    <attribute name="fullname"> <!-- польное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </attribute>
                    <element name="id"> <!-- идентификатор сообщения -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <choice> <!-- источник сообщения -->
                        <element name="source"> <!-- сообщение не имеет источника -->
                            <data type="string">
                                <param name="length">0</param>
                            </data>
                        </element>
                        <element name="source"> <!-- сообщение имеет источник -->
                            <data type="string">
                                <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                            </data>
                        </element>
                    </choice>
                    <element name="title"> <!-- заголовок сообщения -->
                        <data type="string">
                            <param name="maxLength">250</param>
                        </data>
                    </element>
                    <element name="text"> <!-- текст сообщения -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания сообщения -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'messages' => array(
        array(
            'uid' => идентификатор_пользователя,
            'username' => 'имя пользователя',
            'fullname' => 'полное имя пользователя',
            'id' => 'идентификатор сообщения',
            'source' => 'источник сообщения',
            'title' => 'заголовок сообщения',
            'text' => 'текст сообщения',
            'time' => 'время создания сообщения',
        ),
        array(...),
        ...
    ),
    'minmark' => минимальная.метка
)
```

** **

**```void outputFormat(string $output = 'xml')```**

**Описание**  
Устанавливает формат выводимых данных.

**Принимаемые значения**  
*```output```* - может принимать значения *json*, или *xml*.

**[>>> Содержание <<<](ru.index)**