**[>>> Contents <<<](en.index)**

# phpMeccano web programming framework (version 0.2.0) #

## General features ##

phpMeccano is an open-source module-structured PHP framework. Framework phpMeccano gives APIs with the following abilities:

* creation and management of the user groups;
* authentication of the users, including 2FA;
* control of access to functions through the group policy;
* creation of the multilingual interface;
* copying, moving and removing of the local files and folders;
* logging of the events;
* creation and sharing of the messages and files between contacts of the user like in simple social network;
* creation of the topics to discuss and to comment them;
* management of the web service's maintenance mode.

Plug-in system allows to extend capabilities and to add new features.

**Current framework version 0.2.0 is the third alpha version.**

## Requirements ##

phpMeccano requires web server (*Apache*, *NGINX* or *lighttpd*) with installed and configured PHP and MySQL/MariaDB.  
phpMeccano was tested with the following environments:

* **Debian 10**
* Apache 2.4.38
* PHP 7.3.9 
* MariaDB 10.3.17

** **

* **CentOS Linux 7**
* Apache 2.4.6
* PHP 5.4.16 
* MariaDB 5.5.64

** **

* **Ubuntu 18.04**
* Apache 2.4.29
* PHP 7.2.19
* MySQL 5.7.27

** **

* **Ubuntu 18.04**
* nginx/1.14.0
* PHP 7.2.19
* MySQL 8.0.17

** **

* **Ubuntu 18.04**
* lighttpd/1.4.45
* PHP 7.2.19
* MySQL 8.0.17

**[>>> Contents <<<](en.index)**