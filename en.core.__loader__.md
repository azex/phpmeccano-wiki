**[>>> Contents <<<](en.index)**

## Module \_\_loader\_\_.php ##

### General description ###

The module ```__loader __.php``` is loaded along with the configuration file ```conf.php``` and contains functions intended for easy loading of various libraries (modules) of the core and installed plug-ins.

### Functions ###

**```bool / NULL loadPHP(string $lib, string $plugin = "core")```**

**Description**  
Intended for loading of PHP libraries by specified names of plug-in and it's library.

**Parameters**  
*```lib```* - library name. The sequence of symbols ```../``` is not allowed.  
*```plugin```* - plug-in name. The sequence of symbols ```../``` is not allowed.```.

**Return values**  
```TRUE```, if the library is loaded.  
```NULL```, if received parameters contain sequence of symbols ```../```.  
```FALSE```, if specified library is not found or if it is read-restricted.

** **

**```FALSE / NULL / string loadJS(string $lib, string $plugin = "core")```**

**Description**  
Intended for inserting code of JavaScript libraries into the body ofHTML documents by specified names of plug-in and it's library.

**Parameters**  
*```lib```* - library name. The sequence of symbols ```../``` is not allowed.  
*```plugin```* - plug-in name. The sequence of symbols ```../``` is not allowed.```.

**Return values**  
In case of success, code of the library as a string.  
```NULL```, if received parameters contain sequence of symbols ```../```.  
```FALSE```, if specified library is not found or if it is read-restricted.

** **

**```FALSE / NULL / string loadCSS(string $lib, string $plugin = "core")```**

**Description**  
Intended for inserting code of CSS libraries into the body ofHTML documents by specified names of plug-in and it's library.

**Parameters**  
*```lib```* - library name. The sequence of symbols ```../``` is not allowed.  
*```plugin```* - plug-in name. The sequence of symbols ```../``` is not allowed.```.

**Return values**  
In case of success, code of the library as a string.  
```NULL```, if received parameters contain sequence of symbols ```../```.  
```FALSE```, if specified library is not found or if it is read-restricted.

** **

**```bool loadDOC(string $doc, string $plugin = "core", string $disp = "inline", bool $nocache = FALSE)```**

**Description**  
Intended for returning of file by using of *mod_xsendfile* (for *Apache2*), *X-Accel-Redirect* (for *NGINX*) or *X-LIGHTTPD-send-file* (for *lighttpd*) by specified names of plug-in and file.

**Parameters**  
*```doc```* - file name, including extension. The sequence of symbols ```../``` is not allowed.  
*```plugin```* - plug-in name. The sequence of symbols ```../``` is not allowed.  
*```disp```* - may receive values ```'inline'``` and ```'attachment'```. Specifies value of title Content-Disposition.  Value ```'inline'``` means that content should be displayed inside the browser page. Value ```'attachment'``` means that content should be downloaded, most browsers provide 'Save as' dialog for this.  
*```nocache```* - a flag indicating the browser that it shouldn't cache recieved file, if value is ```TRUE```. If value is ```FALSE``` tha browser should to cache recieved file.

**Return values**  
Returns requared file in case of success. Otherwise returns a page with one of the following HTTP statuses:  
```400 Bad Request```, if incoming parameters are incorrect;  
```403 Forbidden```, if the access is denied;  
```404 Not Found```, if the file isn't found;  
```501 Not Implemented```, if the method is executed under unsupported web server;  
```503 Service Unavailable```, if there is some error on the server.  
Returns ```FALSE``` in case of executing not under the web server.

** **

**```bool mntc()```**

**Description**  
It replaces a requested page by the maintenance mode stub-page of the web service. The function should be inserted into the first lines of the code. See description of the module *[maintenance.php](en.core.maintenance)* to get additional information about maintenance mode management.

**Return values**  
Inserts the stub-page at enabled maintenance mode. Returns ```FALSE``` at disabled maintenance mode.

** **

**```mysqli dbLink()```**

**Description**  
The function is intended for easy getting of the database link based on parameters of configuration file ```conf.php```.

**Return values**  
A link to the database as ```mysqli``` object.
