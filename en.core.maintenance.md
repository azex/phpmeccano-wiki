**[>>> Contents <<<](en.index)**

## Module maintenance.php ##

### General description ###

Includes class ```Maintenance```, that is purposed to manage the maintenance mode. Methods of the class manage writing of the settings into the file which is stored be the address ```MECCANO_SERVICE_PAGES.'/maintenance.json'```. The settings are stored in the JSON format. Parameters of the settings are associated to the following keys:  
* ```enabled``` - a boolean expression, the current status of maintenance mode. If value is ```true```, maintenance mode is turned on, if value is ```false``` then turned off.  
* ```prmsg``` - a string, a primary message to be displayed on the stub-page.  
* ```secmsg``` - a string, a secondary message to be displayed on the stub-page in case if defined timeout is expired but maintenance mode still isn't turned off. If value of ```startpoint``` parameter is defined as 0, then secondary message never displays on the stub-page.  
* ```startpoint``` - an integer, a starting point of timeout defined in seconds past from beginning of Unix Epoch (January 1 1970 00:00:00 GMT). If value of ```startpoint``` is greater than real current time value by GMT, or isn't defined to 0, then maintenance mode isn't turned off even if ```enabled``` parameter is ```true```.  
* ```timeout``` - an integer, an estimated time of maintenance mode end defined in seconds. This value is used inside ```Retry-After``` header of the stub-page. If ```startpoint``` parameter is greater than 0, then date inside ```Retry-After``` header is specified by sum of parameters values ```startpoint + timeout``` formatted as *```"<day-name>, <day> <month> <year> <hour>:<minute>:<second> GMT"```*. If ```startpoint``` parameter value is defined to 0, then date inside ```Retry-After``` header is specified in seconds defined in  ```timeout``` parameter.

### Methods of class ```Maintenance``` *(class ```Maintenance``` extends ```ServiceMethods```)* ###

**```__construct(\mysqli $dbLink)```**

**Description**  
Sets a connection to database.

**Parameters**  
*```dbLink```* - object ```mysqli```.

** **

**```array / bool state()```**

**Description**  
Displays state of the maintenance mode.

**Return values**  
In case of success returns an array structured like:

```
array(
    'enabled' => status_of_the_maintenance_mode,
    'prmsg' => 'primary message',
    'secmsg' => 'secondary message',
    'timeout' => timeout_value,
    'startpoint' => start_point_of_the_timeout,
    'expired' => status_of_the_maintenance_mode_expiration
)
```

Returns ```false``` in case of failure.

** **

**```bool write(object $conf)```**

**Description**  
Executes writing of maintenance mode settings.

**Parameters**  
*```conf```* - an object with maintenance mode settings. The object with maintenance mode settings must contain the following properties: ```enabled```, ```prmsg```, ```secmsg```, ```startpoint```, ```timeout```. The names of the object properties match with the names of the settings parameters of the maintenance mode and fully analogous them by the matter.

**Return values**  
In case of success returns ```true```, otherwise returns ```false```.

** **

**```array / bool enable()```**

**Description**  
Turns on maintenance mode.

**Return values**  
In case of success returns array ```array('enabled' => true)```, otherwise returns ```false```.

** **

**```array / bool disable()```**

**Description**  
Turns off maintenance mode.

**Return values**  
In case of success returns array ```array('enabled' => false)```, otherwise returns ```false```.

** **

**```array / bool prmsg(string $msg = 'The site is under maintenance')```**

**Description**  
Defines primary message to be displayed on the stub-page.

**Parameters**  
*```msg```* - a string with the primary message.

**Return values**  
In case of success returns array like ```array('prmsg' => 'primary message')```, otherwise returns ```false```.

** **

**```array / bool secmsg(string $msg = 'We will be back soon')```**

**Description**  
Defines a secondary message to be displayed on the stub-page in case if defined timeout is expired but maintenance mode still isn't turned off.

**Parameters**  
*```msg```* - a string with the secondary message. If value of ```startpoint``` parameter is defined as 0, then secondary message never displays on the stub-page.

**Return values**  
In case of success returns array like ```array('secmsg' => 'secondary message')```, otherwise returns ```false```.

** **

**```array / bool startpoint(integer $sec = 0);```**

**Description**  
Defines a starting point of timeout defined in seconds past from beginning of Unix Epoch (January 1 1970 00:00:00 GMT).

**Parameters**  
*```sec```* - a starting point, defined in seconds. If value of ```startpoint``` is greater than real current time value by GMT, or isn't defined to 0, then maintenance mode isn't turned off even if ```enabled``` parameter is ```true```.

**Return values**  
In case of success returns array like ```array('startpoint' => defined_seconds)```, otherwise returns ```false```.

** **

**```array / bool timeout(integer $sec = 1800)```**

**Description**  
Defines in second an estimated time of maintenance mode end. This value is used inside ```Retry-After``` header of the stub-page.

**Parameters**  
*```sec```* - a timeout, defined in seconds. If ```startpoint``` parameter is greater than 0, then date inside ```Retry-After``` header is specified by sum of parameters values ```startpoint + timeout``` formatted as *```"<day-name>, <day> <month> <year> <hour>:<minute>:<second> GMT"```*. If ```startpoint``` parameter value is defined to 0, then date inside ```Retry-After``` header is specified in seconds defined in  ```timeout``` parameter.

**Return values**  
In case of success returns array like ```array('timeout' => defined_seconds)```, otherwise returns ```false```.

** **

**```array / bool reset()```**

**Description**  
Resets the settings of the maintenance mode to the values by default. Along with it doesn't change value of parameter ```enabled``` of the maintenance mode settings.

**Return values**  
In case of success returns array like ```array('enabled' => bool, 'prmsg' => 'The site is under maintenance', 'secmsg' => 'We will be back soon', 'timeout' => 1800, 'startpoint' => 0)```, otherwise returns ```false```.
