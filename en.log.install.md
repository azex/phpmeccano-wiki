**[>>> Contents <<<](en.index)**

## Clarification of the principle of creation and storage of log records ##

Any record which is created in the log is related with some plug-in that exists in the registry of the installed plug-in. In addition, log records can be created only by keys defined in the log registry in advance. As a rule, event record is formed from two parts: a pattern to describe the event and some data to insert to a pattern.

For example, let's consider pattern ```"Plugin [%d] has been removed"```. Here instead of ```%d``` will be inserted data that qualifying which plug-in was removed. If sequence of symbols ```%d``` is absent inside of pattern, for example, ```"Log records have been removed"```, then log record will consist only of pattern without specifying data.

Framework phpMeccano allows to use patterns in various languages.  It should be borne in mind that while installation of events from the file log.xml patterns are saved only in the languages which are present in the registry of the framework. If a language not described in the the file is present in the registry, then the pattern like ```"keyword: [%d]"``` will be created for that language.

**[>>> Contents <<<](en.index)**