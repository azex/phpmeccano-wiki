**[>>> Contents <<<](en.index)**

## Module langman.php ##

### General description ###

Includes class ```LangMan``` that is purposed to multilanguage support.

### Methods of class ```LangMan``` *(class ```LangMan``` extends ```LogMan```)* ###

**```__construct(\mysqli $dbLink)```***

**Description**  
Sets a connection to database.

**Parameters**  
*```dbLink```* - object ```mysqli```.

** **

**```bool installLang(\DOMDocument $langs, bool $validate = TRUE)```**

**Description**  
Installs support of new languages.

**Parameters**  
*```langs```* - object ```DOMDocument```, that includes languages to install.  
XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="languages">
            <zeroOrMore>
                <element name="lang"> <!-- language node-->
                    <attribute name="code"> <!-- language code -->
                        <data type="string">
                            <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                        </data>
                    </attribute>
                    <attribute name="name"> <!-- language name -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </attribute>
                    <attribute name="dir"> <!-- text direction -->
                        <choice>
                            <value>ltr</value>
                            <value>rtl</value>
                        </choice>
                    </attribute>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

*```validate```* - the structure of *DOM* is checked beforehand if value is ```TRUE```. The structure is not checked if value is ```FALSE```.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool addLang(string $code, string $name, string dir = 'ltr', bool $log = TRUE)```**

**Description**  
Adds new language to the list of available language.

**Parameters**  
*```code```* - code of new language, matches pattern *[a-z]{2}-[A-Z]{2}*.  
*```name```* - language name, a string of 50 characters length.  
*```dir```* - sets in which direction the text must be displayed for the language. Two values are allowed: ```'ltr'``` (from left to right) and ```'rtl'``` (from right to left).  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool delLang(string $code, bool $log = TRUE)```**

**Description**  
Deletes specified language and all data related to it.

**Parameters**  
*```code```* - code of new language, matches pattern *[a-z]{2}-[A-Z]{2}*. Cannot get value equal to ```MECCANO_DEF_LANG```.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```DOMDocument langList()```**

**Description**  
Returns a list of available languages.

**Return values**  
In case of success returns a list of available language as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "code": {
                "type": "string",
                "pattern": "^[a-z]{2}-[A-Z]{2}$"
            },
            "name": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "dir": {
                "type": "string",
                "pattern": "^(ltr|rtl)$"
            }
        },
        "requied": ["id", "code", "name", "dir"]
    }
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="languages">
            <oneOrMore>
                <element name="lang"> <!-- information about language -->
                    <element name="id"> <!-- language identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="code"> <!-- language code -->
                        <data type="string">
                            <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                        </data>
                    </element>
                    <element name="name"> <!-- language name -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="dir"> <!-- direction of the text display -->
                        <choice>
                            <value>ltr</value>
                            <value>rtl</value>
                        </choice>
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    array(
        "id" => language_identifier,
        "code" => language_code,
        "name" => language_name,
        "dir" => text_direction
    ),
    array(...),
    ...
)
```

** **

**```bool installTitles(DOMDocument $titles, bool $validate = TRUE)```**

**Description**  
Installs multilingual titles of the plugin.

**Parameters**   
*```titles```* - object DOMDocument that includes titles of the plugin.  
XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="titles">
            <attribute name="plugin"> <!-- short name of plugin -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <choice>
                    <element name="section"> <!-- title section -->
                        <attribute name="name"> <!-- section name -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- old section name -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- section type, static -->
                            <value>1</value>
                        </attribute>
                        <oneOrMore>
                            <element name="title"> <!-- title -->
                                <attribute name="name"> <!-- title name -->
                                    <data type="string">
                                        <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                    </data>
                                </attribute>
                                <oneOrMore>
                                    <element name="language"> <!-- title itself -->
                                        <attribute name="code"> <!-- language code of title -->
                                            <data type="string">
                                                <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                            </data>
                                        </attribute>
                                        <data type="string">
                                            <param name="minLength">1</param>
                                            <param name="maxLength">128</param>
                                        </data>
                                    </element>
                                </oneOrMore>
                            </element>
                        </oneOrMore>
                    </element>
                    <element name="section"> <!-- title section -->
                        <attribute name="name"> <!-- section name -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- old section name -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- section type, nonstatic -->
                            <value>0</value>
                        </attribute>
                    </element>
                </choice>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

*```validate```* - the structure of DOM is checked beforehand if value is ```TRUE```. The structure is not checked if value is ```FALSE```.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool installTexts(DOMDocument $texts, bool $validate = TRUE)```**

**Description**  
Installs multilingual texts of the plugin.

**Parameters**  
*```texts```* - object ```DOMDocument``` that includes titles of the plugin.  
XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="texts">
            <attribute name="plugin"> <!-- short plugin name -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <choice>
                    <element name="section"> <!-- test section -->
                        <attribute name="name"> <!-- section name -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- old section name -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- section type, static -->
                            <value>1</value>
                        </attribute>
                        <oneOrMore>
                            <element name="text"> <!-- text -->
                                <attribute name="name"> <!-- text name -->
                                    <data type="string">
                                        <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                    </data>
                                </attribute>
                                <oneOrMore>
                                    <element name="language">
                                        <attribute name="code"> <!-- language code of text -->
                                            <data type="string">
                                                <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                            </data>
                                        </attribute>
                                        <element name="title"> <!-- title of text -->
                                            <data type="string">
                                                <param name="minLength">1</param>
                                                <param name="maxLength">128</param>
                                            </data>
                                        </element>
                                        <element name="document"> <!-- text itself -->
                                            <data type="string">
                                                <param name="minLength">0</param>
                                                <param name="maxLength">65535</param>
                                            </data>
                                        </element>
                                    </element>
                                </oneOrMore>
                            </element>
                        </oneOrMore>
                    </element>
                    <element name="section"> <!-- text section -->
                        <attribute name="name"> <!-- section name -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- pld section name -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- section type, nonstatic -->
                            <value>0</value>
                        </attribute>
                    </element>
                </choice>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

*```validate```* - the structure of DOM is checked beforehand if value is ```TRUE```. The structure is not checked if value is ```FALSE```.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool delPlugin(string $plugin)```**

**Description**  
Deletes multilingual titles and texts related to specified plugin.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```int addTitleSection(string $section, string $plugin, bool $log = TRUE)```**

**Description**  
Adds new nonstatic title section of the plugin.

**Parameters**  
*```section```* - name of new section, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```log```* - a flag to record event in log.

**Return values**  
Returns identifier of created section in case of success. Otherwise returns ```FALSE```.

** **

**```bool delTitleSection(int $sid, bool $log = TRUE)```**

**Description**  
Deletes a nonstatic title section of the plugin with all it's titles at all languages.

**Parameters**  
*```sid```* - identifier of the section to delete.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```int addTextSection(string $section, string $plugin, bool $log = TRUE)```**

**Description**  
Adds new nonstatic text section of the plugin.

**Parameters**  
*```section```* - name of new section, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```log```* - a flag to record event in log.

**Return values**  
Returns identifier of created section in case of success. Otherwise returns ```FALSE```.

** **

**```bool delTextSection(int $sid, bool $log = TRUE)```**

**Description**  
Deletes a nonstatic text section of the plugin with all it's texts at all languages.

**Parameters**  
*```sid```* - identifier of the section to delete.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```int addTitleName(string $name, string $section, string $plugin, bool $log = TRUE)```**

**Description**  
Adds new name to title section of the plugin.

**Parameters**  
*```name```* - name of new title, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```log```* - a flag to record event in log.

**Return values**  
Returns identifier of new title name in case of success. Otherwise returns ```FALSE```.

** **

**```bool delTitleName(int $nameid, bool $log = TRUE)```**

**Description**  
Deletes name with all it's titles at all languages.

**Parameters**  
*```nameid```* - identifier of name to delete.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```int addTextName(string $name, string $section, string $plugin, bool $log = TRUE)```**

**Description**  
Adds new name to text section of the plugin.

**Parameters**  
*```name```* - name of new text, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```log```* - a flag to record event in log.

**Return values**  
Returns identifier of new text name in case of success. Otherwise returns ```FALSE```.

** **

**```bool delTextName(int $nameid, bool $log = TRUE)```**

**Description**  
Deletes name with all it's texts at all languages.

**Parameters**  
*```nameid```* - identifier of name to delete.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```int addTitle(string $title, string $name, string $section, string $plugin, string $code = MECCANO_DEF0LANG, bool $log = TRUE)```**

**Description**  
Adds new title with defined name, section and plugin at specified language.

**Parameters**  
*```title```* - a title of 128 characters length.  
*```name```* - title name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.  
*```log```* - a flag to record event in log.

**Return values**  
Returns identifier of new title in case of success. Otherwise returns ```FALSE```.

** **

**```bool delTitle(int $tid, bool $log = TRUE)```**

**Description**  
Deletes title with defined identifier.

**Parameters**  
*```tid```* - identifier of title to delete.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```int addText(string $title, string $document, string $name, string $section, string $plugin, string $code = MECCANO_DEF_LANG, bool $log = TRUE)```**

**Description**  
Adds new text with defined name, section and plugin at specified language.

**Parameters**  
*```title```* - a title of 128 characters length.  
*```document```* - a text of 65535 characters length.  
*```name```* - text name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.  
*```log```* - a flag to record event in log.

**Return values**  
Returns identifier of new text in case of success. Otherwise returns ```FALSE```.

** **

**```bool delText(int $tid, bool $log = TRUE)```**

**Description**  
Deletes text with defined identifier.

**Parameters**  
*```tid```* - identifier of text to delete.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool updateTitle(int $tid, string $title, bool $log = TRUE)```**

**Description**  
Updates defined existent title.

**Parameters**  
*```tid```* - title identifier.  
*```title```* - a new title of 128 character length.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool updateText(int $tid, string $title, string $document, bool $log = TRUE)```**

**Description**  
Updates defined existent text.

**Parameters**  
*```tid```* - text identifier.  
*```title```* - a new text title of 128 character length.  
*```document```* - a new text of 65635 characters length.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```array / bool / DOMDocument / string getTitle(string $name, string $section, string $plugin, string $code = MECCANO_DEF_LANG)```**

**Description**  
Returns title by defined name, section, plugin and language code.

**Parameters**  
*```name```* - title name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.

**Return values**  
In case of success returns title and text direction as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "title": {
            "type": "string",
            "minLength": 1,
            "maxLength": 128
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        }
    },
    "required": ["title", "dir"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="string">
            <element name="title"> <!-- title -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">128</param>
                </data>
            </element>
            <element name="dir"> <!-- text direction -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'title' => "sample title", 
    'dir' => "text direction [ltr of rtl]"
)
```

** **

**```array / bool / DOMDocument / string getText(string $name, string $section, string $plugin, string $code = MECCANO_DEF_LANG)```**

**Description**  
Returns title and text by defined name, section, plugin and language code.

**Parameters**  
*```name```* - text name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.

**Return values**  
In case of success returns title, text, time of creation/editing and text direction as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "title": {
            "type": "string",
            "maxLength": 128
        },
        "document": {
            "type": "string",
            "minLength": 1,
            "maxLength": 65535
        },
        "created": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "edited": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        }
    },
    "required": ["title", "document", "created", "edited", "dir"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="text">
            <element name="title"> <!-- text title -->
                <data type="string">
                    <param name="maxLength">128</param>
                </data>
            </element>
            <element name="document"> <!-- text -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="created"> <!-- date of the text creation -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="edited"> <!-- date of the last text editing -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="dir"> <!-- text direction -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'title' => "sample text title", 
    'document' => "sample text", 
    'created' => 0000-00-00 00:00:00, 
    'edited' => 0000-00-00 00:00:00, 
    'dir' => "text direction [ltr of rtl]"
)
```

** **

**```array getTitles(string $section, string $plugin, string $code = MECCANO_DEF_LANG)```**

**Description**  
Returns all titles by defined section plugin and language code for using it by principle key-value.

**Parameters**  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.

**Return values**  
In case of success returns all titles as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "patternProperties": {
        "^[a-zA-Z0-9_]{3,40}$": {
            "type": "string",
            "minLength": 1,
            "maxLength": 128
        }
    },
    "additionalProperties": false
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="titles">
            <zeroOrMore>
                <element name="title"> <!-- title node -->
                    <attribute name="name"> <!-- title name -->
                        <data type="string"> <!-- title -->
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </attribute>
                    <data type="string">
                        <param name="minLength">1</param>
                        <param name="maxLength">128</param>
                    </data>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    "title1" => "title #1",
    "title2" => "title #2"
    ...
)
```

** **

**```DOMDocument getAllTextsList(string $section, string $plugin, string $code = MECCANO_DEF_LANG, array $orderBy = 'id', bool $ascent = FALSE)```**

**Description**  
Returns list of all text titles by defined section, plugin, and language code.

**Parameters**  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - sorting parameters of output of text titles. Allowed values of parameters: ```'id'```, ```'title'```, ```'name'```, ```'created'``` and ```'edited'```. Sequence of parameters is defined as array, for example, ```array('title', 'created')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns a whole list of text titles as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "code": {
            "type": "string",
            "pattern": "^[a-z]{2}-[A-Z]{2}$"
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        },
        "texts": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "title": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    },
                    "created": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    },
                    "edited": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["id", "title", "name", "created", "edited"]
            }
        }
    },
    "required": ["code", "dir", "texts"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="texts">
            <attribute name="code"> <!-- language code of data displaying -->
                <data type="string">
                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                </data>
            </attribute>
            <attribute name="dir"> <!-- text direction -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </attribute>
            <zeroOrMore>
                <element name="text"> <!-- text node -->
                    <element name="id"> <!-- text identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="title"> <!-- text title -->
                        <data type="string">
                            <param name="maxLength">128</param>
                        </data>
                    </element>
                    <element name="name"> <!-- text name -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                    <element name="created"> <!-- date and time of text creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="edited"> <!-- date and time of text editing -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    "code" => language_code,
    "dir" => text_direction,
    "texts" => array(
        array(
            "id" => text_identifier,
            "title" => text_title,
            "name" => text_name,
            "created" => date_of_text_creation,
            "edited" => date_of_text_editing
        ),
        array(...),
        ...
    )
)
```

** **

**```array / bool / DOMDocument / string getTextById(int $id)```**

**Description**  
Returns text by defined identifier.

**Parameters**  
*```id```* - text identifier.

**Return values**  
In case of success returns title, text, time of creation/editing and text direction as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "title": {
            "type": "string",
            "maxLength": 128
        },
        "document": {
            "type": "string",
            "minLength": 1,
            "maxLength": 65535
        },
        "created": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "edited": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        }
    },
    "required": ["title", "document", "created", "edited", "dir"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="text">
            <element name="title"> <!-- text title -->
                <data type="string">
                    <param name="maxLength">128</param>
                </data>
            </element>
            <element name="document"> <!-- text -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="created"> <!-- date of the text creation -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="edited"> <!-- date of the last text editing -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="dir"> <!-- text direction -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'title' => "sample text title", 
    'document' => "sample text", 
    'created' => 0000-00-00 00:00:00, 
    'edited' => 0000-00-00 00:00:00, 
    'dir' => "text direction [ltr of rtl]"
)
```

** **

**```array sumTexts(string $section, plugin $plugin, string $code = MECCANO_DEF_LANG, int $rpp = 20)```**

**Description**  
Returns summary data about texts in plugin section.

**Parameters**  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*. Value *NULL* is equel to constant *MECCANO_DEF_LANG*.

**Return values**  
Array like ```array('records' => int $totalTexts, 'pages' => int $totalPages)```, where ```totalTexts``` - total number of texts in plugin section, ```totalPages``` - total number of pages, calculated as ```totalTexts/rpp```. In case of failure returns ```FALSE```.

** **

**```DOMDocument getTextsList(string $section, string $plugin, int $pageNumber, int $totalPages, int $rpp = 20, string $code = MECCANO_DEF_LANG, $orderBy = 'id', bool $ascent = FALSE)```**

**Description**  
Returns a page with text titles by defined section, plugin and language code.

**Parameters**  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```pageNumber```* - page number, integer from 1 and more.  
*```totalPages```* - total number of pages.  
*```rpp```* - desired maximum number of records per page.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - sorting parameters of output of text titles. Allowed values of parameters: ```'id'```, ```'title'```, ```'name'```, ```'created'``` и ```'edited'```. Sequence of parameters is defined as array, for example, ```array('title', 'created')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns a page with text titles as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "code": {
            "type": "string",
            "pattern": "^[a-z]{2}-[A-Z]{2}$"
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        },
        "texts": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "title": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    },
                    "created": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    },
                    "edited": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    }
                },
                "required": ["id", "title", "name", "created", "edited"]
            }
        }
    },
    "required": ["code", "dir", "texts"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="texts">
            <attribute name="code"> <!-- language code -->
                <data type="string">
                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                </data>
            </attribute>
            <attribute name="dir"> <!-- text direction -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </attribute>
            <zeroOrMore>
                <element name="text"> <!-- text node -->
                    <element name="id"> <!-- text identifer -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="title"> <!-- text title -->
                        <data type="string">
                            <param name="maxLength">128</param>
                        </data>
                    </element>
                    <element name="name"> <!-- text name -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                    <element name="created"> <!-- date of text creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="edited"> <!-- date of text editing -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    "code" => language_code,
    "dir" => text_direction,
    "texts" => array(
        array(
            "id" => text_identifier,
            "title" => text_title,
            "name" => text_name,
            "created" => date_of_text_creation,
            "edited" => date_of_text_editing
        ),
        array(...),
        ...
    )
)
```

** **

**```array getTexts($section, $plugin, $code = MECCANO_DEF_LANG)```**

**Description**  
Returns all texts with titles by defined section plugin and language code for using it by principle key-value.

**Parameters**  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.

**Return values**  
In case of success returns all texts with titles as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "patternProperties": {
        "^[a-zA-Z0-9_]{3,40}$": {
            "type": "object",
            "properties": {
                "title" : {
                    "type": "string",
                    "maxLength": 128
                },
                "document": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 65535
                }
            },
            "required": ["title", "document"]
        }
    },
    "additionalProperties": false
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="texts">
            <zeroOrMore>
                <element name="text"> <!-- text block -->
                    <attribute name="name"> <!-- text name -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </attribute>
                    <element name="title"> <!-- text title -->
                        <data type="string">
                            <param name="maxLength">128</param>
                        </data>
                    </element>
                    <element name="document"> <!-- document text -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">65535</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    "text_name" => array(
        "title" => "text title",
        "document" => "document text"
    ),
    "other_text_name" => array(...)
    ...
)
```

** **

**```array sumTitles($section, $plugin, $code = MECCANO_DEF_LANG, $rpp = 20)```**

**Description**  
Returns summary data about titles in plugin section.

**Parameters**  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattrern *[a-zA-Z\d_]{3,30}*.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
Array like ```array('records' => int $totalTitles, 'pages' => int $totalPages)```, where ```totalTitles``` - total number of titles in plugin section, ```totalPages``` - total number of pages, calculated as ```totalTitles/rpp```. In case of failure returns ```FALSE```.

** **

**```DOMDocument getTitlesList(string $section, string $plugin, int $pageNumber, int $totalPages, int $rpp = 20, string $code = MECCANO_DEF_LANG, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Description**  
Returns page with titles by defined section, plugin and language code.

**Parameters**  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```pageNumber```* - page number, integer from 1 and more.  
*```totalPages```* - total number of pages.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - sorting parameters of output of titles. Allowed values of parameters: ```'id'```, ```'title'``` и ```'name'```. Sequence of parameters is defined as array, for example, ```array('title', 'name')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns a page with titles as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "code": {
            "type": "string",
            "pattern": "^[a-z]{2}-[A-Z]{2}$"
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        },
        "titles": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "title": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    }
                },
                "required": ["id", "title", "name"]
            }
        }
    },
    "required": ["code", "dir", "titles"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="titles">
            <attribute name="code"> <!-- language code -->
                <data type="string">
                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                </data>
            </attribute>
            <attribute name="dir"> <!-- text direction -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </attribute>
            <zeroOrMore>
                <element name="title"> <!-- title node -->
                    <element name="id"> <!-- title identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="title"> <!-- title -->
                        <data type="string">
                            <param name="maxLength">128</param>
                        </data>
                    </element>
                    <element name="name"> <!-- title name -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    "code" => language_code,
    "dir" => text_direction,
    "titles" => array(
        array(
            "id" => title_identifier,
            "title" => title,
            "name" => title_name
        ),
        array(...),
        ...
    )
)
```

** **

**```DOMDocument getAllTitlesList(string $section, string $plugin, string $code = MECCANO_DEF_LANG, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Description**  
Returns list of all titles by defined section, plugin, and language code.

**Parameters**  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - sorting parameters of output of titles. Allowed values of parameters: ```'id'```, ```'title'``` и ```'name'```. Sequence of parameters is defined as array, for example, ```array('title', 'name')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns a whole list of titles as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "code": {
            "type": "string",
            "pattern": "^[a-z]{2}-[A-Z]{2}$"
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        },
        "titles": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "title": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    }
                },
                "required": ["id", "title", "name"]
            }
        }
    },
    "required": ["code", "dir", "titles"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="titles">
            <attribute name="code"> <!-- language code -->
                <data type="string">
                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                </data>
            </attribute>
            <attribute name="dir"> <!-- text direction -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </attribute>
            <zeroOrMore>
                <element name="title"> <!-- title node -->
                    <element name="id"> <!-- title identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="title"> <!-- title -->
                        <data type="string">
                            <param name="maxLength">128</param>
                        </data>
                    </element>
                    <element name="name"> <!-- title name -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    "code" => language_code,
    "dir" => text_direction,
    "titles" => array(
        array(
            "id" => title_identifier,
            "title" => title,
            "name" => title_name
        ),
        array(...),
        ...
    )
)
```

** **

**```array / bool / DOMDocument / string getTitleById(int $id)```**

**Description**  
Returns title by identifier.

**Parameters**  
*```id```* - title identifier.

**Return values**  
In case of success returns title and text direction as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "title": {
            "type": "string",
            "minLength": 1,
            "maxLength": 128
        },
        "dir": {
            "type": "string",
            "pattern": "^(ltr|rtl)$"
        }
    },
    "required": ["title", "dir"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="string">
            <element name="title"> <!-- title -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">128</param>
                </data>
            </element>
            <element name="dir"> <!-- text direction -->
                <choice>
                    <value>ltr</value>
                    <value>rtl</value>
                </choice>
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'title' => "sample title", 
    'dir' => "text direction [ltr of rtl]"
)
```

** **

**```array sumTextSections(string $plugin, int $rpp = 20)```**

**Description**  
Returns summary data about text sections of plugin.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
Array like ```array('records' => int $totalSections, 'pages' => int $totalPages)```, where ```totalSections``` - total number of texts in plugin section, ```totalPages``` - total number of pages, calculated as ```totalSections/rpp```. In case of failure returns ```FALSE```.

** **

**```DOMDocument getTextSectionsList(string $plugin, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Description**  
Returns list of text sections of the plugin.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```pageNumber```* - page number, integer from 1 and more.  
*```totalPages```* - total number of pages.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.  
*```orderBy```* - sorting parameters of output of plugin sections. Allowed values of parameters: ```'id'```, ```'name'``` и ```'static'```. Sequence of parameters is defined as array, for example, ```array('id', 'static')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns list of text plugins of the plugin as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugin": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "sections": {
            "type": "array",
            "item": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    },
                    "static": {
                        "type": "integer",
                        "minimum": 0,
                        "maximum": 1
                    },
                    "contains": {
                        "type": "integer",
                        "minimum": 0
                    }
                },
                "required": ["id", "name", "static", "contains"]
            }
        }
    },
    "required": ["plugin", "sections"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="sections">
            <attribute name="plugin"> <!-- plugin name -->
                <ref name="plugFuncName" />
            </attribute>
            <oneOrMore>
                <element name="section"> <!-- section node -->
                    <element name="id"> <!-- section identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="name"> <!-- section name -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                    <element name="static"> <!-- section type. 0 - nonstatic, 1 - static -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                    <element name="contains"> <!-- number of records in section (is counted by name number) -->
                        <data type="nonNegativeInteger" />
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
    <define name="plugFuncName">
        <data type="string">
            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
        </data>
    </define>
</grammar>
```

Array structure has a view like:

```
array(
    "plugin" => plugin_name,
    "sections" => array(
        array(
            "id" => section_identifier,
            "name" => section_name,
            "static" => section_type,
            "contains" => number_of_records
        ),
        array(
            ...
        ),
        ...
    )
)
```

** **

**```array sumTitleSections(string $plugin, int $rpp = 20)```**

**Description**  
Returns summary data about title sections of the plugin.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
Массив вида ```array('records' => int $totalSections, 'pages' => int $totalPages)```, where ```totalSections``` - total number of text in the plugin section, ```totalPages``` - total number of pages, calculated as ```totalSections/rpp```. In case of failure returns ```FALSE```.

** **

**```DOMDocument getTitleSectionsList(string $plugin, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Description**  
Returns list of title sections of the plugin.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```pageNumber```* - page number, integer from 1 and more.  
*```totalPages```* - total number of pages.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.  
*```orderBy```** - sorting parameters of output ofplugin sections. Allowed values of parameters: ```'id'```, ```'name'``` и ```'static'```. Sequence of parameters is defined as array, for example, ```array('id', 'static')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns list of title plugins of the plugin as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugin": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "sections": {
            "type": "array",
            "item": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    },
                    "static": {
                        "type": "integer",
                        "minimum": 0,
                        "maximum": 1
                    },
                    "contains": {
                        "type": "integer",
                        "minimum": 0
                    }
                },
                "required": ["id", "name", "static", "contains"]
            }
        }
    },
    "required": ["plugin", "sections"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="sections">
            <attribute name="plugin"> <!-- plugin name -->
                <ref name="plugFuncName" />
            </attribute>
            <oneOrMore>
                <element name="section"> <!-- section node -->
                    <element name="id"> <!-- section identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="name"> <!-- section name -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                    <element name="static"> <!-- section name. 0 - nonstatic, 1 - static -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                    <element name="contains"> <!-- number of records in section (is counted by name number) -->
                        <data type="nonNegativeInteger" />
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
    <define name="plugFuncName">
        <data type="string">
            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
        </data>
    </define>
</grammar>
```

Array structure has a view like:

```
array(
    "plugin" => plugin_name,
    "sections" => array(
        array(
            "id" => section_identifier,
            "name" => section_name,
            "static" => section_type,
            "contains" => number_of_records
        ),
        array(
            ...
        ),
        ...
    )
)
```

** **

**```array sumTextNames(string $plugin, string $section, int $rpp = 20)```**

**Description**  
Returns summary data about text names by defined plugin and section.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```section```* - section pattern, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
Array like ```array('records' => int $totalNames, 'pages' => int $totalPages)```, where ```totalNames``` - tottal number of names in plugin section, ```totalPages``` - total number of pages, calculated as ```totalNames/rpp```. In case of failure returns ```FALSE```.

** **

**```DOMDocument getTextNamesList(string $plugin, string $section, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = 'id', bool $ascent = FALSE)```**

**Description**  
Returns list of text names by plugin and section.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```pageNumber```* - page number, integer from 1 and more.  
*```totalPages```* - total number of pages.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.  
*```orderBy```* - sorting parameters of output of text names of plugin sections. Allowed values of parameters: ```'id'```, ```'name'```. Sequence of parameters is defined as array, for example, ```array('name', 'id')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns text names as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugin": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "section": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,40}$"
        },
        "texts": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    },
                    "languages": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "pattern": "^[a-z]{2}-[A-Z]{2}$"
                        }
                    }
                },
                "required": ["id", "name", "languages"]
            }
        }
    },
    "required": ["plugin", "section", "texts"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="page">
            <attribute name="plugin"> <!-- plugin name -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <attribute name="section"> <!-- section name -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="text"> <!-- name node -->
                    <element name="id"> <!-- name identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="name"> <!-- name -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                    <element name="languages"> <!-- list of languages in which records exist -->
                        <zeroOrMore>
                            <element name="code"> <!-- language code -->
                                <data type="string">
                                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                </data>
                            </element>
                        </zeroOrMore>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    "plugin" => plugin_name,
    "section" => section_name,
    "texts" => array(
        array(
            "id" => name_identifier,
            "name" => name,
            "languages" => array("la-NG", ...)
        ),
        array(
            ...
        ),
        ...
    )
)
```

** **

**```array sumTitleNames(string $plugin, string $section, int $rpp = 20)```**

**Description**  
Returns summary data about title names by plugin and section.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
Array like ```array('records' => int $totalNames, 'pages' => int $totalPages)```, where ```totalNames``` - total number of names of plugin section, ```totalPages``` - total number of pages, calculated as ```totalNames/rpp```. In case of failure returns ```FALSE```.

** **

**```DOMDocument getTitleNamesList(string $plugin, string $section, int $pageNumber, int $totalPages, int $rpp = 20, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Description**  
Returns list of title names by plugin and section.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```section```* - section name, matches pattern *[a-zA-Z\d_]{3,40}*.  
*```pageNumber```* - page number, integer from 1 and more.  
*```totalPages```* - total number of pages.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.  
*```orderBy```* - sorting parameters of output of title names of plugin sections. Allowed values of parameters: ```'id'```, ```'name'```. Sequence of parameters is defined as array, for example, ```array('name', 'id')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns title names as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugin": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "section": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,40}$"
        },
        "titles": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "name": {
                        "type": "string",
                        "pattern": "^[a-zA-Z0-9_]{3,40}$"
                    },
                    "languages": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "pattern": "^[a-z]{2}-[A-Z]{2}$"
                        }
                    }
                },
                "required": ["id", "name", "languages"]
            }
        }
    },
    "required": ["plugin", "section", "titles"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="page">
            <attribute name="plugin"> <!-- plugin name -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <attribute name="section"> <!-- section name -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="title"> <!-- name node -->
                    <element name="id"> <!-- name identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="name"> <!-- name -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                        </data>
                    </element>
                    <element name="languages"> <!-- list of languages in which records exist -->
                        <zeroOrMore>
                            <element name="code"> <!-- language code -->
                                <data type="string">
                                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                </data>
                            </element>
                        </zeroOrMore>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    "plugin" => plugin_name,
    "section" => section_name,
    "titles" => array(
        array(
            "id" => name_identifier,
            "name" => name,
            "languages" => array("la-NG", ...)
        ),
        array(
            ...
        ),
        ...
    )
)
```

** **

**```void outputFormat(string $output = 'xml')```**

**Description**  
Sets format of output data.

**Parameters**  
*```output```* - can get values *json*, or *xml*.

**[>>> Contents <<<](en.index)**