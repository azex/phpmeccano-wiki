**[>>> Contents <<<](en.index)**

## Module plugins.php ##

### General description ###

Includes class ```Plugins```, that is purposed to install, delete and update plugins of framework.


### Methods of class ```Plugins``` *(class ```Plugins``` extends ```LangMan```)* ###

**```__construct(\mysqli $dbLink)```**

**Description**  
Sets a connection to database.

**Parameters**  
*```dbLink```* - object ```mysqli```.

** **

**```string unpack(string $package)```**

**Description**  
Makes preliminarily unpacking of plugin into interim directory.

**Parameters**  
*```package```* - path to package with plugin.

**Return values**  
Returns short name of unpacked plugin in case of success. Otherwise returns ```FALSE```.

** **

**```bool delUnpacked(string $plugin)```**

**Description**  
Deletes unpacked package of the plugin.

**Parameters**  
*```plugin```* - short name of unpacked plugin, matches pattern *[a-zA-Z\d_]{3,30}*.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```DOMDocument listUnpacked()```**

**Description**  
Returns list of preliminarily unpacked packages.

**Return values**  
In case of success returns list of preliminarily unpacked packages as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "short": {
                "type": "string",
                "pattern": "^[a-zA-Z0-9_]{3,30}$"
            },
            "full": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "version": {
                "type": "string",
                "pattern": "^[0-9]{1,2}\\.[0-9]{1,2}\\.[0-9]{1,2}$"
            },
            "action": {
                "type": "string",
                "pattern": "^(install|reinstall|upgrade|downgrade)$"
            }
        },
        "required": ["short", "full", "version", "action"]
    }
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="unpacked">
            <zeroOrMore>
                <element name="plugin"> <!-- plugin node -->
                    <element name="short"> <!-- plugin name -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                        </data>
                    </element>
                    <element name="full"> <!-- full name of the plugin -->
                        <data type="string">
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="version"> <!-- plugin version -->
                        <data type="string">
                            <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}</param>
                        </data>
                    </element>
                    <element name="action"> <!-- action that can be done -->
                        <choice>
                            <value>install</value>
                            <value>reinstall</value>
                            <value>upgrade</value>
                            <value>downgrade</value>
                        </choice>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    array(
        'short' => 'plugin name',
        'full' => 'full name of the plugin',
        'version' => 'plugin version',
        'action' => 'action that can be done'
    ),
    array(...),
    ...
)
```

** **

**```DOMDocument aboutUnpacked(string $plugin)```**

**Description**  
Returns information about unpacked package.

**Parameters**  
*```plugin```* - name of unpacked plugin, matches pattern *[a-zA-Z\d_]{3,30}*.

**Return values**  
In case of success returns information about unpacked package as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "short": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "full": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "version": {
            "type": "string",
            "pattern": "^[0-9]{1,2}\\.[0-9]{1,2}\\.[0-9]{1,2}$"
        },
        "about": {
            "type": "string",
            "maxLength": 65535
        },
        "credits": {
            "type": "string",
            "maxLength": 65535
        },
        "url": {
            "type": "string",
            "maxLength": 100
        },
        "email": {
            "type": "string",
            "maxLength": 100
        },
        "license": {
            "type": "string",
            "maxLength": 65535
        },
        "depends": {
            "type": "string",
            "maxLength": 65535
        },
        "action": {
            "type": "string",
            "pattern": "^(install|reinstall|upgrade|downgrade)$"
        }
    },
    "required": ["short", "full", "version", "about", "credits", "url", "email", "license", "depends", "action"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="unpacked">
            <element name="short"> <!-- plugin name -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </element>
            <element name="full"> <!-- full name of the plugin -->
                <data type="string">
                    <param name="maxLength">50</param>
                </data>
            </element>
            <element name="version"> <!-- plugin version -->
                <data type="string">
                    <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}</param>
                </data>
            </element>
            <element name="about"> <!-- plugin description -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="credits"> <!-- list of developers of the plugin -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="url"> <!-- web site of the plugin -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="email"> <!-- contact e-mail address -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="license"> <!-- license used by the plugin -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="depends"> <!-- list of dependences required for plugin -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="action"> <!-- action that can be done -->
                <choice>
                    <value>install</value>
                    <value>reinstall</value>
                    <value>upgrade</value>
                    <value>downgrade</value>
                </choice>
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'short' => 'plugin name',
    'full' => 'full name of the plugin',
    'version' => 'plugin version',
    'about' => 'plugin description',
    'credits' => 'developers of the plugin',
    'url' => 'web site of the plugin',
    'email' => 'contact e-mail address',
    'license' => 'license used by the plugin',
    'depends' => 'dependences required for plugin',
    'action' => 'action that can be done'
)
```

** **

**```int pluginData(string $plugin)```**

**Description**  
Returns identifier and version of installed plugin.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.

**Return values**  
Returns array like ```array("id" => (int) $id, "version" => $version)```, where ``id`` - integer identifier of the plugin, ```version``` - plugin version.

** **

**```bool install(string $plugin bool $reset = FALSE, bool $log = TRUE)```**

**Description**  
Makes installation of unpacked plugin.

**Parameters**  
*```plugin```* - name of unpacked plugin, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```reset```* - a flag that shows installation script of the package that all data relevant to plugin are needed to reset by default. Usage of a flag has meaning while re-installation or update of the plugin.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool delInstalled(int $id, bool $keepData = TRUE, bool $log = TRUE)```**

**Description**  
Deletes installed plugin.

**Parameters**  
*```id```* - integer identifier of the unpacked plugin.  
*```keepData```* - a flag that shows removing script of the plugin the all data relevant to plugin are needed to stay and not to delete them. Files of the plugin which are stored inside directory ```MECCANO_DOCUMENTS_DIR``` are not deleted if value is ```TRUE```. These files are deleted if value is ```FALSE```. All records of the log, access policies, texts and titles at all available languages, which are related with plugin, will be removed in any case.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```DOMDocument listInstalled()```**

**Description**  
Returns a whole list of installed plugins.

**Return values**  
In case of success returns a whole list of installed plugins as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "short": {
                "type": "string",
                "pattern": "^[a-zA-Z0-9_]{3,30}$"
            },
            "full": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "version": {
                "type": "string",
                "pattern": "^[0-9]{1,2}\\.[0-9]{1,2}\\.[0-9]{1,2}$"
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            }
        },
        "required": ["short", "full", "version", "time"]
    }
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="installed">
            <zeroOrMore>
                <element name="plugin"> <!-- plugin node -->
                    <element name="short"> <!-- plugin name -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                        </data>
                    </element>
                    <element name="full"> <!-- full name of the plugin -->
                        <data type="string">
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="version"> <!-- plugin version -->
                        <data type="string">
                            <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of the plugin installation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    array(
        "short" => 'plugin name',
        "full" => 'full name of the plugin',
        "version" => 'plugin version',
        "time" => 'date and time of the plugin installation'
    ),
    array(...),
    ...
)
```

** **

**```DOMDocument aboutInstalled(string $plugin)```**

**Description**  
Returns information about installed plugin.

**Parameters```**  
*```plugin```* - short name of installed plugin, matches pattern *[a-zA-Z\d_]{3,30}*.

**Return values**  
In case of success returns information about installed plugin as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "short": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "full": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "version": {
            "type": "string",
            "pattern": "^[0-9]{1,2}\\.[0-9]{1,2}\\.[0-9]{1,2}$"
        },
        "time": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "about": {
            "type": "string",
            "maxLength": 65535
        },
        "credits": {
            "type": "string",
            "maxLength": 65535
        },
        "url": {
            "type": "string",
            "maxLength": 100
        },
        "email": {
            "type": "string",
            "max:ength": 100
        },
        "license": {
            "type": "string",
            "maxLength": 65535
        }
    },
    "required": ["short", "full", "version", "time", "about", "credits", "url", "email", "license"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="installed">
            <element name="short"> <!-- plugin name -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </element>
            <element name="full"> <!-- full name of the plugin -->
                <data type="string">
                    <param name="maxLength">50</param>
                </data>
            </element>
            <element name="version"> <!-- plugin version -->
                <data type="string">
                    <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}</param>
                </data>
            </element>
            <element name="time"> <!-- date and time of the plugin installation -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="about"> <!-- description of the plugin -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="credits"> <!-- developers of the plugin -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="url"> <!-- web site of the plugin -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="email"> <!-- e-mail address for contact -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="license"> <!-- license that is used for plugin -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    "short" => 'plugin name',
    "full" => 'ull name of the plugin',
    "version" => 'plugin version',
    "time" => 'date and time of the plugin installation',
    "about" => 'description of the plugin',
    "credits" => 'developers of the plugin',
    "url" => 'web site of the plugin',
    "email" => 'e-mail address for contact',
    "license" => 'license that is used for plugin'
)
```

** **

**```void outputFormat(string $output = 'xml')```**

**Description**  
Sets format of output data.

**Parameters**  
*```output```* - can get values *json*, or *xml*.

**[>>> Contents <<<](en.index)**