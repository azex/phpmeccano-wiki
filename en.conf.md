**[>>> Contents <<<](en.index)**

# Configuration file conf.php #

### General description ###

File *conf.php* includes constants of database parameters, system paths, default language, parameters to share files and messages, temporary authentication blocking and displaying errors.

### Database parameters ###

**```MECCANO_DBANAME```**

**Description**  
Name of the database administrator.

**Default value**  
```'root'```

** **

**```MECCANO_DBAPASS```**

**Description**  
Password of the database administrator.

**Default value**  
```'MySQLpassw'```

** **

**```MECCANO_DBHOST```**

**Description**  
Database host.

**Default value**  
```'localhost'```

** **

**```MECCANO_DBPORT```**

**Description**  
Port of the database.

**Default value**  
```'3306'```

** **

**```MECCANO_DBNAME```**

**Description**  
Database name.

**Default value**  
```'phpmeccano'```

** **

**```MECCANO_TPREF```**

**Description**  
Prefix of the database tables.

**Default value**  
```'meccano'```

** **

**```MECCANO_DBSTORAGE_ENGINE```**

**Description**  
Prefered engine of the database storage.

**Default value**  
```'MyISAM'```

** **

### System paths ###

**```MECCANO_CONF_FILE```**

**Description**  
The configuration file itself.

**Default value**  
```__FILE__```

** **

**```MECCANO_ROOT_DIR```**

**Description**  
Root folder of the framework.

**Default value**  
```dirname(__FILE__)```

** **

**```MECCANO_CORE_DIR```**

**Description**  
Folder that includes core modules of the framework.

**Default value**  
```MECCANO_ROOT_DIR.'/core'```

** **

**```MECCANO_TMP_DIR```**

**Description**  
Folder that includes temporary files during execution of intermediate actions.

**Default value**  
```MECCANO_ROOT_DIR.'/tmp'```

** **

**```MECCANO_PHP_DIR```**

**Description**  
Folder that includes php-libraries of installed plug-ins.

**Default value**  
```MECCANO_ROOT_DIR.'/phplibs'```

** **

**```MECCANO_JS_DIR```**

**Description**  
Folder that includes javascript-libraries of installed plug-ins.

**Default value**  
```MECCANO_ROOT_DIR.'/jslibs'```

** **

**```MECCANO_CSS_DIR```**

**Description**  
Folder that includes css-libraries of installed plug-ins.

**Default value**  
```MECCANO_ROOT_DIR.'/csslibs'```

** **

**```MECCANO_DOCUMENTS_DIR```**

**Description**  
Folder that includes variable files, related to installed plug-ins, such as images, videos etc.

**Default value**  
```MECCANO_ROOT_DIR.'/documents'```

** **

**```MECCANO_UNPACKED_PLUGINS```**

**Description**  
Folder that includes unpacked packages of plug-ins before installation.

**Default value**  
```MECCANO_ROOT_DIR.'/unpacked'```

** **

**```MECCANO_UNINSTALL```**

**Description**  
Folder that includes scripts to remove installed plug-ins.

**Default value**  
```MECCANO_ROOT_DIR.'/uninstall'```

** **

### Storage of shared files ###

**```MECCANO_SHARED_FILES```**

**Description**  
Folder that keeps shared user files.

**Default value**  
```MECCANO_ROOT_DIR.'/shfiles'```

** **

**```MECCANO_SHARED_FILES```**

**Description**  
Function that generates names of subfolders by which files are distributed.

**Default value**  
```date('Y-m-d')```

** **

### Default language ###

**```MECCANO_DEF_LANG```**

**Description**  
Default system language. To prevent occurrence of unforeseen errors, it is not recommended to change the value after beginning of framework usage.

**Default value**  
```'en-US'```

** **

### Temporary blocking of user authentication to protect from brute-force ###

**```MECCANO_AUTH_LIMIT```**

**Description**  
Limit of password input attempts.

**Default value**  
```5```

** **

**```MECCANO_AUTH_BLOCK_PERIOD```**

**Description**  
Period of temporary blocking of user authentication.

**Default value**  
```'00:05:00'```

** **

### Displaying of errors ###

**```MECCANO_SHOW_ERRORS```**

**Description**  
Manages displaying of errors on the screen.  
If constant gets value ```TRUE```, then information about risen error will be displayed while every call of method setError of class ```ServiceMethods```. If it gets value ```FALSE```, then information about risen error won't be displayed.

**Default value**  
```FALSE```

** **

### Maintenance mode ###

**```MECCANO_SERVICE_PAGES```**

**Description**  
A path to the folder that stores pages of the HTTP-statuses, a stub-page of the maintenance mode and a file with the settings of the maintenance mode.

**Default value**  
```MECCANO_ROOT_DIR.'/servpages'```

** **

**```MECCANO_MNTC_IP```**

**Description**  
A list of IP addresses which aren't affected by the maintenance mode. The addresses must be divided by commas.

**Default value**  
```'127.0.0.1'```

**[>>> Contents <<<](en.index)**