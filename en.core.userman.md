**[>>> Contents <<<](en.index)**

## Module userman.php ##

### General description ###

Includes class ```UserMan``` that is purposed to manage users and user groups.


### Methods of class ```UserMan``` *(class ```UserMan``` extends ```LogMan```)* ###


**```__construct(mysqli $dbLink)```**

**Description**  
Sets a connection to database.

**Parameters**  
*```dbLink```* - object ```mysqli```.

** **

**```int createGroup(string $groupName, string $description, bool $log = TRUE)```**

**Description**  
Creates new user group.

**Parameters**  
*```groupName```* - group name.  
*```description```* - group description.  
*```log```* - a flag to record event in log.  

**Return values**  
Identifier of created group. In case of failure returns ```FALSE```.

** **

**```bool groupStatus(int $groupId, bool $active, bool $log = TRUE)```**

**Description**  
Sets group status.

**Parameters**  
*```groupId```* - group identifier.  
*```active```* - a flag of group status. Use ```TRUE``` to enable group and ```FALSE``` to disable group.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool groupExists(string $groupName)```**

**Description**  
Checks whether exists group with defined name.

**Parameters**  
*```groupName```* - group name.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool moveGroupTo(int $groupId, int $destId, bool $log = TRUE)```**

**Description**  
Moves all users from one group to another.

**Parameters**  
*```groupId```* - identifier of a source group. Must get value more than 1.  
*```destId```* - identifier of a destination group.  
*```log```* - a flag to record event in log.

**Return values**  
Returns number of moved users in case of success. Otherwise returns ```FALSE```.

** **

**```DOMDocument aboutGroup(int $groupId)```**

**Description**  
Returns information about group.

**Parameters**  
*```groupId```* - group identifier.

**Return values**  
In case of success returns group description as object ```DOMDocument```, or as string JSON, otherwise returns ```FALSE```.  
JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "id": {
            "type": "integer",
            "minimum": 1
        },
        "name": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "description": {
            "type": "string",
            "maxLength": 256
        },
        "time": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "active": {
            "type": "integer",
            "minimum": 0,
            "maximum": 1
        },
        "usum": {
            "type": "integer",
            "minimum": 0
        }
    },
    "required": ["id", "name", "description", "time", "active", "usum"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="group">
            <element name="id"> <!-- group identifier -->
                <data type="positiveInteger" />
            </element>
            <element name="name"> <!-- group name -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">50</param>
                </data>
            </element>
            <element name="description"> <!-- group description -->
                <data type="string">
                    <param name="maxLength">256</param>
                </data>
            </element>
            <element name="time"> <!-- date and time of group creation -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="active"> <!-- group status. 1 - enabled, 0 - disabled -->
                <choice>
                    <value>0</value>
                    <value>1</value>
                </choice>
            </element>
            <element name="usum"> <!-- total amount of users in the group -->
                <data type="nonNegativeInteger" />
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'id' => group_identifier,
    'name' => group_name,
    'description' => group_description,
    'time' => time_of_group_creation,
    'active' => group_status,
    'usum' => total_amount_of_users
)
```

** **

**```bool setGroupName(int $groupId, string $groupName, bool $log = TRUE)```**

**Description**  
Sets new name of a group.

**Parameters**  
*```groupId```* - group identifier.  
*```groupName```* - new name of a group.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool setGroupDesc(int $groupId, string $description, bool $log = TRUE)```**

**Description**  
Sets new description of a group.

**Parameters**  
*```groupId```* - group identifier.  
*```description```* - new description of a group.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool delGroup(int $groupId, bool $log = TRUE)```**

**Description**  
Deletes user group together with all access policy of plugin components.

**Parameters**  
*```groupId```* - group identifier. Must get value more than 1.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```array sumGroups(int $rpp = 20)```**

**Description**  
Returns summary data per page about user groups.

**Parameters**  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
Array like ```array('records' => int $totalGroups, 'pages' => int $totalPages)```, where ```totalGroups``` - total number of groups, ```totalPages``` - total number of pages, calculated as ```totalGroups/rpp```. In case of failure returns ```FALSE```.

** **

**```DOMDocument getGroups(int $pageNumber, int $totalGroups, int $rpp = 20, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Description**  
Returns a page with list of user groups.

**Parameters**  
*```pageNumber```* - page number, integer from 1 and more.  
*```totalGroups```* - total number of groups.  
*```rpp```* - desired maximum number of records per page.  
*```orderBy```* - sorting parameters of output of records. Allowed values of parameters: ```'id'```, ```'name'``` , ```'time'``` и ```'active'```. Sequence of parameters is defined as array, for example, ```array('name', 'time')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent*``` - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns a page with list of user groups as object ```DOMDocument```, or as string JSON, otherwise returns ```FALSE```.  
JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "name": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            },
            "active": {
                "type": "integer",
                "minimum": 0,
                "maximum": 1
            }
        },
        "required": ["id", "name", "time", "active"]
    }
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="groups">
            <oneOrMore>
                <element name="group"> <!-- information about group -->
                    <element name="id"> <!-- group identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="name"> <!-- group name -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of group creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="active"> <!-- group status. 1 - enabled, 0 - disabled -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    array(
        'id' => group_identifier,
        'name' => group_name,
        'time' => time_of_group_creation,
        'active' => group_status
    ),
    array(...),
    ...
)
```

** **

*```DOMDocument getAllGroups(array $orderBy = array('id'), bool $ascent = FALSE)```*

**Description**  
Returns a whole list of user groups.

**Parameters**  
*```orderBy```* - sorting parameters of output of records. Allowed values of parameters: ```'id'```, ```'name'``` , ```'time'``` и ```'active'```. Sequence of parameters is defined as array, for example, ```array('name', 'time')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**
In case of success returns a whole list of user groups as object ```DOMDocument```, or as string JSON, otherwise returns ```FALSE```.  
JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "name": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            },
            "active": {
                "type": "integer",
                "minimum": 0,
                "maximum": 1
            }
        },
        "required": ["id", "name", "time", "active"]
    }
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="groups">
            <oneOrMore>
                <element name="group"> <!-- information about group-->
                    <element name="id"> <!-- group identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="name"> <!-- group name -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="time"> <!-- date and time of group creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="active"> <!-- group status. 1 - enabled, 0 - disabled -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    array(
        'id' => group_identifier,
        'name' => group_name,
        'time' => time_of_group_creation,
        'active' => group_status
    ),
    array(...),
    ...
)
```

** **

**```int createUser(string $username, string $password, string $email, int $groupId, bool $active = TRUE, bool $log = TRUE)```**

**Description**  
Creates new user.

**Parameters**  
*```username```* - desired username.  
*```password```* - password.  
*```email```* - e-mail address.  
*```groupId```* - identifier of the group to which new user belongs.  
*```active```* - a flag of user status. Use ```TRUE``` to enable user and ```FALSE``` to disable user.  
*```log```* - a flag to record event in log.

**Return values**  
Identifier of created user. In case of failure returns ```FALSE```.

** **

**```int userExists(string $username)```**

**Description**  
Checks whether exists a user with defined username.

**Parameters**  
*```username```* - checked username.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```int mailExists(string $email)```**

**Description**  
Checks whether exists a user with defined e-mail address.

**Parameters**  
*```email```* - checked e-mail address.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool userStatus(int $userId, bool $active, bool $log = TRUE)```**

**Description**  
Sets user status.

**Parameters**  
*```groupId```* - user identifier.  
*```active```* - a flag of user status. Use TRUE to enable user and ```FALSE``` to disable user.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE.```

** **

**```bool moveUserTo(int $userId, int $destId, bool $log = TRUE)```**

**Description**  
Moves a user from one group to another.

**Parameters**  
*```userId```* - user identifier. Must get value more than 1.  
*```destId```* - identifier of a destination group.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool delUser(int $userId, bool $log = TRUE)```**

**Description**  
Deletes defined user.

**Parameters**  
*```userId```* - user identifier. Must get value more than 1.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```DOMDocument aboutUser(int $userId)```**

**Description**  
Returns information about user.

**Parameters**  
*```userId```* - user identifier.

**Return values**  
In case of success returns information about user as object ```DOMDocument```, or as string JSON, otherwise returns ```FALSE```.  
JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "id": {
            "type": "integer",
            "minimum": 1
        },
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "fillname": {
            "type": "string",
            "maxLength": 255
        },
        "email": {
            "type": "string",
            "pattern": "^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?(?:\\.[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?)*$"
        },
        "time": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "active": {
            "type": "integer",
            "minimum": 0,
            "maximum": 1
        },
        "gid": {
            "type": "integer",
            "minimum": 1
        },
        "group": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        }
    },
    "required": ["id", "username", "fullname", "email", "time", "active", "gid", "group"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="user">
            <element name="id"> <!-- user identifier -->
                <data type="positiveInteger" />
            </element>
            <element name="username"> <!-- username -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </element>
            <element name="fullname"> <!-- full user name -->
                <data type="string">
                    <param name="maxLength">255</param>
                </data>
            </element>
            <element name="email"> <!-- e-mail address of the user -->
                <data type="string">
                    <param name="pattern">[^@]+@[^\.]+\..+</param>
                </data>
            </element>
            <element name="time"> <!-- date and time of user creation -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="active"> <!-- user status. 1 - enabled, 0 - disabled -->
                <choice>
                    <value>0</value>
                    <value>1</value>
                </choice>
            </element>
            <element name="gid"> <!-- identifier of the group to which user belongs -->
                <data type="positiveInteger" />
            </element>
            <element name="group"> <!-- name of the group to which new user belongs -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">50</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    array(
    'id' => user_identifier,
    'username' => username,
    'fullname' => full_user_name,
    'email' => e-mail_address,
    'time' => time_of_user_creation,
    'active' => user_status,
    'gid' => identifier_of_the_user_group,
    'group' => name_of_the_user_group
)
)
```

** **

**```DOMDocument userPasswords(int $userId)```**

**Description**  
Returns a list with information about passwords of a user.

**Parameters**  
*```userId```* - user identifier.

**Return values**  
In case of success returns information about passwords of a user as object ```DOMDocument```, or as string JSON, otherwise returns ```FALSE```.  
JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "passwords": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "description": {
                        "type": "string",
                        "maxLength": 30
                    },
                    "limited": {
                        "type": "integer",
                        "minimum": 0,
                        "maximum": 1
                    }
                },
                "required": ["id", "description", "limited"]
            }
        }
    }
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="security">
            <attribute name="uid"> <!-- user identifier -->
                <data type="positiveInteger" />
            </attribute>
            <oneOrMore>
                <element name="password"> <!-- information about password -->
                    <element name="id"> <!-- password identifier -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="description"> <!-- password description -->
                        <data type="string">
                            <param name="maxLength">30</param>
                        </data>
                    </element>
                    <element name="limited"> <!-- password type. 1 - secondary password (with limited rights), 0 - primary password -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'uid' => user_identifier,
    'passwords' => array(
        array(
            'id' => password_identifier,
            'description' => password_description,
            'limited' => password_type
        ),
        array(...),
        ...
    )
)
```

** **

**```string createPassword(int $userId, string $description, int $length = 8, bool $underline = TRUE, bool $minus = FALSE, bool $special = FALSE, bool $log = TRUE)```**

**Description**  
Automatically creates secondary passwords the user.

**Parameters**  
*```userId```* - user identifier.  
*```description```* - short description of the password.  
*```length```* - desired length of the password. Minimum value - 8, maximum value - 50.  
*```underline```* - a flag of the usage of underline within password. If ```TRUE``` - to use, if ```FALSE``` - not to use.  
*```minus```* - a flag of the usage of minus within password. If ```TRUE``` - to use, if ```FALSE``` - not to use.  
*```special```* - a flag of the usage of special characters within password. If ```TRUE``` - to use, if ```FALSE``` - not to use.  
*```log```* - a flag to record event in log.

**Return values**  
Generated password. In case of failure returns ```FALSE```.

** **

**```int addPassword(int $userId, string $password, string $description='', bool $log = TRUE)```**

**Description**  
Creates secondary password of the user which is defined by the user individually.

**Parameters**  
*```userId```* - user identifier.  
*```password```* - created password.  
*```description```* - short description of the password.  
*```log```* - a flag to record event in log.

**Return values**  
Identifier of created password. In case of failure returns ```FALSE```.

** **

**```bool delPassword(string $passwId, int $userId, bool $log = TRUE)```**

**Description**  
Removes secondary password of the user.

**Parameters**  
*```passwId```* - password identifier.  
*```userId```* - user identifier.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.


**```bool setPassword(string $passwId, int $userId, string $password, bool $log = TRUE)```**

**Description**  
Set new value of the user password.

**Parameters**  
*```passwId```* - password identifier.  
*```userId```* - user identifier.  
*```password```* - new password value.  
*```log```* - a flag to record event in log.

**Return values**
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool setUserName(int $userId, string $username, bool $log = TRUE)```**

**Description**  
Sets new value of the username.

**Parameters**  
*```userId```* - user identifier.  
*```username```* - new username.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool setUserMail(int $userId, string $email, bool $log = TRUE)```**

**Description**  
Sets new value of the user e-mail.

**Parameters***  
*```userId```* - user identifier.  
*```email```* - new e-mail address.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool setFullName(int $userId, string $name, bool $log = TRUE)```**

**Description**  
Sets new value of full name of the user.

**Parameters**  
*```userId```* - user identifier.  
*```name```* - new full name of the user.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool changePassword(string $passwId, int $userId, int $oldPassw, int $newPassw, bool $log = TRUE)```**

**Description**  
Changes an old user password to new.

**Parameters**  
*```passwId```* - password identifier.  
*```userId```* - user identifier.  
*```oldPassw```* - old password.  
*```newPassw```* - new password.  
*```log```* - a flag to record event in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```array sumUsers(int $rpp = 20)```**

**Description**  
Returns summary data per page about users.

**Parameters**  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
Array like ```array('records' => $totalUsers, 'pages' => $totalPages)```, where ```totalUsers``` - total number of users, ```totalPages``` - total number of pages, calculated as ```totalUsers/rpp```. In case of failure returns ```FALSE```.

** **

**```DOMDocument getUsers(int $pageNumber, int $totalUsers, int $rpp = 20, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Description**  
Returns a page with list of users.

**Parameters**
*```pageNumber```* - page number, integer from 1 and more.  
*```totalUsers```* - total number of users.  
*```rpp```* - desired maximum number of records per page.  
*```orderBy```* - sorting parameters of output of records. Allowed values of parameters: ```'id'```, ```'username'```, ```'time'```, ```'fullname'```, ```'email'```, ```'group'```, ```'gid'``` и ```'active'```. Sequence of parameters is defined as array, for example, ```array('group', 'time')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns a page with list of users as object ```DOMDocument```, or as string JSON, otherwise returns ```FALSE```.  
JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "item": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "username": {
                "type": "string",
                "pattern": "^[a-zA-Z0-9_]{3,20}$"
            },
            "fullname": {
                "type": "string",
                "maxLength": 100
            },
            "email": {
                "type": "string",
                "pattern": "^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?(?:\\.[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?)*$"
            },
            "group": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "gid": {
                "type": "integer",
                "minimum": 1
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            },
            "active": {
                "type": "integer",
                "minimum": 0,
                "maximum": 1
            }
        },
        "required": ["id", "username", "fullname", "email", "group", "gid", "time", "active"]
    }
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="users">
            <oneOrMore>
                <element name="user"> <!-- information about user -->
                    <element name="id"> <!-- user identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="username"> <!-- username -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- full user name -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="email"> <!-- user e-mail -->
                        <data type="string">
                            <param name="pattern">[^@]+@[^\.]+\..+</param>
                        </data>
                    </element>
                    <element name="group"> <!-- name of the group to which user belongs -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="gid"> <!-- identifier of the group to which user belongs -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- date and time of user creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="active"> <!-- user status. 1 - enabled, 0 - disabled -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    array(
        id' => user_identifier,
        'username' => username,
        'fullname' => full_user_name,
        'email' => e-mail_address,
        'group' => name_of_the_user_group,
        'gid' => identifier_of_the_user_group,
        'time' => time_of_user_creation,
        'active' => user_status
    ),
    array(...),
    ...
)
```

** **

**```DOMDocument getAllUsers(array $orderBy = array('id'), bool $ascent = FALSE)```**

**Description**  
Returns a whole list of users.

**Parameters**  
*```orderBy```* - sorting parameters of output of records. Allowed values of parameters: ```'id'```, ```'username'```, ```'time'```, ```'fullname'```, ```'email'```, ```'group'```, ```'gid'``` и ```'active'```. Sequence of parameters is defined as array, for example, ```array('group', 'time')```. Incorrectly defined sequence of parameters is set equal ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns a whole list of users as object ```DOMDocument```, or as string JSON, otherwise returns ```FALSE```.  
JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "item": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "username": {
                "type": "string",
                "pattern": "^[a-zA-Z0-9_]{3,20}$"
            },
            "fullname": {
                "type": "string",
                "maxLength": 100
            },
            "email": {
                "type": "string",
                "pattern": "^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?(?:\\.[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?)*$"
            },
            "group": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "gid": {
                "type": "integer",
                "minimum": 1
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            },
            "active": {
                "type": "integer",
                "minimum": 0,
                "maximum": 1
            }
        },
        "required": ["id", "username", "fullname", "email", "group", "gid", "time", "active"]
    }
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="users">
            <oneOrMore>
                <element name="user"> <!-- information about user -->
                    <element name="id"> <!-- user identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="username"> <!-- username -->
                        <data type="string">
                            <param name="minLength">3</param>
                            <param name="maxLength">20</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- full user name -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="email"> <!-- user e-mail -->
                        <data type="string">
                            <param name="pattern">[^@]+@[^\.]+\..+</param>
                        </data>
                    </element>
                    <element name="group"> <!-- name of the group to which user belongs -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="gid"> <!-- identifier of the group to which user belongs -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- date and time of user creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="active"> <!-- user status. 1 - enabled, 0 - disabled -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    array(
        id' => user_identifier,
        'username' => username,
        'fullname' => full_user_name,
        'email' => e-mail_address,
        'group' => name_of_the_user_group,
        'gid' => identifier_of_the_user_group,
        'time' => time_of_user_creation,
        'active' => user_status
    ),
    array(...),
    ...
)
```

** **

**```bool setUserLang(int $userId, string $code = MECCANO_DEF_LANG)```**

**Description**  
Sets preferred language of the user.

**Parameters**  
*```userId```* - user identifier.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool enable2FA(string $passwId, int $userId)```**

**Description**  
Enables two-factor authentication for specified user password.

**Parameters**  
*```passwId```* - password identifier.  
*```userId```* - user identifier.  

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool disable2FA(string $passwId, int $userId)```**

**Description**  
Disables two-factor authentication for specified user password.

**Parameters**  
*```passwId```* - password identifier.  
*```userId```* - user identifier.  

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```void outputFormat(string $output = 'xml')```**

**Description**  
Sets format of output data.

**Parameters**  
*```output```* - can get values *json*, or *xml*.

**[>>> Contents <<<](en.index)**