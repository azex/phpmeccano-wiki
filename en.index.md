![logo.svg](https://bitbucket.org/azexmail/phpmeccano/downloads/logo.svg)  
**[Select language](Home)**

**Contents**

* [phpMeccano web programming framework (version 0.2.0)](en.phpmeccano)
* [Configuration file conf.php](en.conf)
* [Installation](en.webinstaller)
* Core modules
    * [Module \_\_loader\_\_.php](en.core.__loader__)
    * [Module swconst.php](en.core.swconst)
    * [Module unifunctions.php](en.core.unifunctions)
    * [Module extclass.php](en.core.extclass)
    * [Module policy.php](en.core.policy)
    * [Module logman.php](en.core.logman)
    * [Module auth.php](en.core.auth)
    * [Module userman.php](en.core.userman)
    * [Module langman.php](en.core.langman)
    * [Module files.php](en.core.files)
    * [Module plugins.php](en.core.plugins)
    * [Module discuss.php](en.core.discuss)
    * [Module share.php](en.core.share)
    * [Module maintenance.php](en.core.maintenance)
* Installation package of a plug-in (version of specification 0.3)
    * [Structure of the package](en.installation.package)
    * [Auxiliary scripts and description of processes of installation and deletion of the plugin](en.inst.rm.script)
    * [Clarification of the schema of files titles.xml и texts.xml and installation and update of texts and titles at several languages](en.lang.install)
    * [Clarification of the principle of creation and storage of log records](en.log.install)