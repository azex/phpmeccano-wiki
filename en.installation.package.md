**[>>> Contents <<<](en.index)**

# Installation package of a plug-in (version of specification 0.3) #

## Structure of a package ##

Installation package of the plug-in is a simple zip-archive that includes the following folders and files:

**Folders**

* *php* - the folder where are placed php-libraries of the installed plug-in.
* *js* - the folder where are placed javascript-libraries of the installed plug-in.
* *documents* - the folder where are placed different files related to the installed plug-in, such as, pictures, videos and others.
* *css* - the folder where are placed css-libraries of the installed plug-in.

**Files**

* *metainfo.xml* - metadata of the plug-in.
* *depends.xml* - list of dependencies needed for installation of the plug-in.
* *languages.xml* - languages to install.
* *policy.xml* - group policies to access functions of the plug-in.
* *log.xml* - list of events related to the plug-in.
* *texts.xml* - texts to create multilingual interface.
* *titles.xml* - titles to create multilingual interface.
* *inst.php* - auxiliary script to install the plug-in.
* *rm.php* - auxiliary script to remove the plug-in.

## Structure of the files ##

### File *metainfo.xml* ###

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="metainfo">
            <attribute name="version"> <!-- version of package specification -->
                <data type="string">
                    <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}</param>
                </data>
            </attribute>
            <element name="shortname"> <!-- shart name of the plug-in which is used as a keyword -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </element>
            <element name="fullname"> <!-- full name of the plug-in -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">50</param>
                </data>
            </element>
            <element name="version"> <!-- plug-in version -->
                <data type="string">
                    <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}</param>
                </data>
            </element>
            <element name="about"> <!-- short description of the plug-in -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="credits"> <!-- list of plug-in developers -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="url"> <!-- web site of the plug-in -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="email"> <!-- email address to contact with developers -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="license"> <!-- a license that is used for the plug-in -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

### File *depends.xml* ###

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="depends">
            <zeroOrMore>
                <element name="plugin">
                    <attribute name="name"> <!-- short name of the plug-in -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                        </data>
                    </attribute>
                    <attribute name="version"> <!-- version of the plug-in -->
                        <data type="string">
                            <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}</param>
                        </data>
                    </attribute>
                    <attribute name="operator"> <!-- an operator to compare existing version and needed version of the plug-in -->
                        <choice>
                            <value>&gt;=</value>
                            <value>&lt;=</value>
                            <value>&gt;</value>
                            <value>&lt;</value>
                            <value>==</value>
                            <value>!=</value>
                        </choice>
                    </attribute>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

### File *languages.xml* ###

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="languages">
            <zeroOrMore>
                <element name="lang"> <!-- node of the language -->
                    <attribute name="code"> <!-- code of the language (language tag) -->
                        <data type="string">
                            <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                        </data>
                    </attribute>
                    <attribute name="name"> <!-- name of the language -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </attribute>
                    <attribute name="dir"> <!-- text direction that is used in the language -->
                        <choice>
                            <value>ltr</value>
                            <value>rtl</value>
                        </choice>
                    </attribute>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

### File *policy.xml* ###

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="policy">
            <attribute name="plugin"> <!-- short name of the plug-in -->
                <ref name="plugFuncName" />
            </attribute>
            <zeroOrMore>
                <element name="function"> <!-- access policies to functions of the plug-in -->
                    <attribute name="name"> <!-- name of the function -->
                        <ref name="plugFuncName" />
                    </attribute>
                    <attribute name="nonauth"> <!-- default value of the policy for non-authenticated users -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </attribute>
                    <attribute name="auth"> <!-- default value of the policy for groups of users -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </attribute>
                    <oneOrMore>
                        <element name="description"> <!-- node of description of the policy -->
                            <attribute name="code"> <!-- language code of the policy -->
                                <data type="string">
                                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                </data>
                            </attribute>
                            <element name="short">
                                <data type="string"> <!-- short description -->
                                    <param name="minLength">1</param>
                                    <param name="maxLength">128</param>
                                </data>
                            </element>
                            <element name="detailed">
                                <data type="string"> <!-- detailed description -->
                                    <param name="minLength">0</param>
                                    <param name="maxLength">1024</param>
                                </data>
                            </element>
                        </element>
                    </oneOrMore>
                </element>
            </zeroOrMore>
        </element>
    </start>
    <define name="plugFuncName">
        <data type="string">
            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
        </data>
    </define>
</grammar>
```

### File *log.xml* ###

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="log">
            <attribute name="plugin"> <!-- short name of the plug-in -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="event"> <!-- node of the event -->
                    <attribute name="keyword"> <!-- keyword of the event -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                        </data>
                    </attribute>
                    <oneOrMore>
                        <element name="desc"> <!-- description of the event -->
                            <attribute name="code"> <!-- language code of event description -->
                                <data type="string">
                                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                </data>
                            </attribute>
                            <data type="string">
                                <param name="maxLength">128</param>
                            </data>
                        </element>
                    </oneOrMore>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

### File *texts.xml* ###

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="texts">
            <attribute name="plugin"> <!-- short name of the plug-in -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <choice>
                    <element name="section"> <!-- node of the text section -->
                        <attribute name="name"> <!-- name of the section -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- old name of the section -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- type of the section, static -->
                            <value>1</value>
                        </attribute>
                        <oneOrMore>
                            <element name="text"> <!-- node of the text -->
                                <attribute name="name"> <!-- name of the text -->
                                    <data type="string">
                                        <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                    </data>
                                </attribute>
                                <oneOrMore>
                                    <element name="language">
                                        <attribute name="code"> <!-- language code of the text -->
                                            <data type="string">
                                                <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                            </data>
                                        </attribute>
                                        <element name="title"> <!-- title of the text -->
                                            <data type="string">
                                                <param name="minLength">1</param>
                                                <param name="maxLength">128</param>
                                            </data>
                                        </element>
                                        <element name="document"> <!-- the text itself -->
                                            <data type="string">
                                                <param name="minLength">0</param>
                                                <param name="maxLength">65535</param>
                                            </data>
                                        </element>
                                    </element>
                                </oneOrMore>
                            </element>
                        </oneOrMore>
                    </element>
                    <element name="section"> <!-- node of the text section -->
                        <attribute name="name"> <!-- name of the section -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- old name of the section -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- type of the section, non-static -->
                            <value>0</value>
                        </attribute>
                    </element>
                </choice>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

### File *titles.xml* ###

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="titles">
            <attribute name="plugin"> <!-- short name of the plug-in -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <choice>
                    <element name="section"> <!-- node of the title section -->
                        <attribute name="name"> <!-- name of the section -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- old name of the section -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- type of the section, static -->
                            <value>1</value>
                        </attribute>
                        <oneOrMore>
                            <element name="title"> <!-- node of the title -->
                                <attribute name="name"> <!-- name of the title -->
                                    <data type="string">
                                        <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                    </data>
                                </attribute>
                                <oneOrMore>
                                    <element name="language"> <!-- the title itself -->
                                        <attribute name="code"> <!-- language code of the title -->
                                            <data type="string">
                                                <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                            </data>
                                        </attribute>
                                        <data type="string">
                                            <param name="minLength">1</param>
                                            <param name="maxLength">128</param>
                                        </data>
                                    </element>
                                </oneOrMore>
                            </element>
                        </oneOrMore>
                    </element>
                    <element name="section"> <!-- node of the title section -->
                        <attribute name="name"> <!-- name of the section -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- old name of the section -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- type of the section, non-static -->
                            <value>0</value>
                        </attribute>
                    </element>
                </choice>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

### File *inst.php* ###

The structure of the file must match the pattern:

```
#!php

<?php

namespace core;

interface intInstall {

    public function __construct(\mysqli $dblink, $id, $version = '', $reset = FALSE);

    public function errId();

    public function errExp();

    public function preinst();

    public function postinst();
}

class Install implements intInstall {

    private $errid = 0; // error code
    private $errexp = ''; // error description
    private $dbLink; // mysqli object
    private $id; // identifier of the installing plugin
    private $version; // version of the existing plugin
    private $reset; //

    public function __construct(\mysqli $dblink, $id, $version = '', $reset = FALSE) {
        $this->dbLink = $dblink;
        $this->id = $id;
        $this->version = $version;
        $this->reset = $reset;
    }

    private function setError($id, $exp) {
        $this->errid = $id;
        $this->errexp = $exp;
    }

    public function errId() {
        return $this->errid;
    }

    public function errExp() {
        return $this->errexp;
    }

    public function preinst() {
// place here your code
        return TRUE;
    }

    public function postinst() {
// place here your code
        return TRUE;
    }

}
```

### File *rm.php* ###

The structure of the file must match the pattern:

```
#!php

<?php

namespace core;

interface intRemove {

    public function __construct(\mysqli $dblink, $id, $keepData = TRUE);

    public function errId();

    public function errExp();

    public function prerm();

    public function postrm();
}

class Remove implements intRemove {

    private $errid = 0; // error code
    private $errexp = ''; // error description
    private $dbLink; // mysqli object
    private $id; // identifier of the installed plugin
    private $keepData; //

    public function __construct(\mysqli $dblink, $id, $keepData = TRUE) {
        $this->dbLink = $dblink;
        $this->id = $id;
        $this->keepData = $keepData;
    }

    private function setError($id, $exp) {
        $this->errid = $id;
        $this->errexp = $exp;
    }

    public function errId() {
        return $this->errid;
    }

    public function errExp() {
        return $this->errexp;
    }

    public function prerm() {
// place here your code
        return TRUE;
    }

    public function postrm() {
// place here your code
        return TRUE;
    }

}
```

**[>>> Contents <<<](en.index)**