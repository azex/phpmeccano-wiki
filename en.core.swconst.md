**[>>> Contents <<<](en.index)**

# Core modules #

## Module swconst.php ##

### General description ###

Module includes system constants.

### Constants of errors ###

These constants are used to point at errors that arise while program execution.

**```int ERROR_INCORRECT_DATA```**

**Description**  
Incorrect value or incorrect data type.

**Value**  
-1

** **

**```int ERROR_NOT_EXECUTED```**

**Description**  
Query or operation is not executed.

**Value**  
-2

** **

**```int ERROR_RESTRICTED_ACCESS```**

**Description**  
Function has restricted access.

**Value**  
-3

** **

**```int ERROR_SYSTEM_INTERVENTION```**

**Description**  
Executed action is an intervention to system operation and breaks it's structure.

**Value**  
-4

** **

**```int ERROR_NOT_CRITICAL```**

**Description**  
Uncritical error, it is not the reason to interrupt program execution.Некритическая ошибка, не являющейся основанием для прерывания выполнения программы.

**Value**  
-5

** **

**```int ERROR_NOT_FOUND```**

**Description**  
The required item was not found.

**Value**  
-6

** **

**```int ERROR_ALREADY_EXISTS```**

**Description**  
The being created item already exists.

**Value**  
-7

### Authentication keys of session variables ###

**```string AUTH_USERNAME```**

**Description**  
Username.

**Value**  
```'core_auth_uname'```

** **

**```int AUTH_USER_ID```**

**Description**  
User identifier.

**Value**  
```'core_auth_uid'```

** **

**```int AUTH_LIMITED```**

**Description**  
Flag that points type of the current session. Session can be limited and unlimited.

**Value**  
```'core_auth_limited'```

** **

**```string AUTH_LANGUAGE```**

**Description**  
Interface language from user settings.

**Value**  
```'core_auth_lang'```

** **

**```string AUTH_LANGUAGE_DIR```**

**Description**  
The direction in which to display the text.

**Value**  
```'core_auth_lang_dir'```

** **

**```int AUTH_PASSWORD_ID```**

**Description**  
Identifier of the used password.

**Value**  
```'core_auth_pid'```

** **

**```string AUTH_UNIQUE_SESSION_ID```**

**Description**  
Unique session identifier related with a used password.

**Value**  
```'core_auth_usid'```

** **

**```string AUTH_IP```**

**Description**  
IP address of the user which is used while passing of authentication. It is used as control parameter to prevent stealing of the session.

**Value**  
```'core_auth_ip'```

** **

**```string AUTH_USER_AGENT```**

**Description**  
User-agent of the user browser which is used while passing of authentication. It is used as control parameter to prevent stealing of the session.

**Value**  
```'core_auth_uagent'```

** **

**```string AUTH_2FA_SWAP```**

**Description**  
While passing two-factor authentication: a key to a buffer with the session variables, it is equal to password value of the second step of the authentication.

**Value**  
```'core_auth_2fa_swap'```

### Keys of cookie values ###

**```string COOKIE_UNIQUE_SESSION_ID```**

**Description**  
Unique session identifier related to user password. It is used   to pass user authentication without password request.

**Value**  
```'core_auth_usid'```

**[>>> Contents <<<](en.index)**