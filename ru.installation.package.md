**[>>> Содержание <<<](ru.index)**

# Установочный пакет плагина (спецификация версии 0.3) #

## Структура пакета ##

Установочный пакет плагина представляет собой обыкновенный zip-архив, содежащий в себе следующие папки и файлы:

**Папки**

* *php* - папка, в которой размещаются php-библиотеки устанавливаемого плагина.
* *js* - папка, в которой размещаются javascript-библиотеки устанавливаемого плагина.
* *documents* - папка, в которой размещаются различные файлы, относящиеся к установливаемому плагину, такие, как изображения, видео и прочие.
* *css* - папка, в которой размещаются css-библиотеки устанавливаемого плагина.

**Файлы**

* *metainfo.xml* - метаданные плагина.
* *depends.xml* - список с зависимостями, необходимыми для установки плагина.
* *languages.xml* - устанавливаемые языки.
* *policy.xml* - групповые политики доступа к функциям плагина.
* *log.xml* - события журнала событий для плагина
* *texts.xml* - тексты для реализации многоязычности.
* *titles.xml* - заголовки для реализации многоязычности.
* *inst.php* - вспомогательный скрипт установки плагина.
* *rm.php* - вспомогательный скрипт удаления плагина.

## Описание структуры файлов ##

### Файл *metainfo.xml* ###

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="metainfo">
            <attribute name="version"> <!-- версия спецификации пакета -->
                <data type="string">
                    <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}</param>
                </data>
            </attribute>
            <element name="shortname"> <!-- короткое имя плагина, которое используется, как ключевое слово -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </element>
            <element name="fullname"> <!-- полное название плагина -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">50</param>
                </data>
            </element>
            <element name="version"> <!-- версия плагина -->
                <data type="string">
                    <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}</param>
                </data>
            </element>
            <element name="about"> <!-- короткое описание плагина -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="credits"> <!-- перечисление разработчиков плагина -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
            <element name="url"> <!-- веб сайт плагина -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="email"> <!-- контактный адрес электронной почты -->
                <data type="string">
                    <param name="maxLength">100</param>
                </data>
            </element>
            <element name="license"> <!-- лицензия, использумая для плагина -->
                <data type="string">
                    <param name="maxLength">65535</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

### Файл *depends.xml* ###

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="depends">
            <zeroOrMore>
                <element name="plugin">
                    <attribute name="name"> <!-- короткое имя плагина -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                        </data>
                    </attribute>
                    <attribute name="version"> <!-- версия плагина -->
                        <data type="string">
                            <param name="pattern">[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}</param>
                        </data>
                    </attribute>
                    <attribute name="operator"> <!-- оператор для сравнения версии существующего плагина с требуемой версией -->
                        <choice>
                            <value>&gt;=</value>
                            <value>&lt;=</value>
                            <value>&gt;</value>
                            <value>&lt;</value>
                            <value>==</value>
                            <value>!=</value>
                        </choice>
                    </attribute>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

### Файл *languages.xml* ###

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="languages">
            <zeroOrMore>
                <element name="lang"> <!-- нод языка -->
                    <attribute name="code"> <!-- код языка (языковой тег) -->
                        <data type="string">
                            <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                        </data>
                    </attribute>
                    <attribute name="name"> <!-- название языка -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </attribute>
                    <attribute name="dir"> <!-- направление текста в языке -->
                        <choice>
                            <value>ltr</value>
                            <value>rtl</value>
                        </choice>
                    </attribute>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

### Файл *policy.xml* ###

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="policy">
            <attribute name="plugin"> <!-- короткое имя плагина -->
                <ref name="plugFuncName" />
            </attribute>
            <zeroOrMore>
                <element name="function"> <!-- политики доступа к функциям плагина -->
                    <attribute name="name"> <!-- имя функции -->
                        <ref name="plugFuncName" />
                    </attribute>
                    <attribute name="nonauth"> <!-- значение политики по умолчанию для пользователей, не прошедших аутентификацию -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </attribute>
                    <attribute name="auth"> <!-- значение политики по умолчанию для групп пользователей -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </attribute>
                    <oneOrMore>
                        <element name="description"> <!-- Описание политик -->
                            <attribute name="code"> <!-- код языка описания -->
                                <data type="string">
                                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                </data>
                            </attribute>
                            <element name="short">
                                <data type="string"> <!-- короткое описание -->
                                    <param name="minLength">1</param>
                                    <param name="maxLength">128</param>
                                </data>
                            </element>
                            <element name="detailed">
                                <data type="string"> <!-- детальное описание -->
                                    <param name="minLength">0</param>
                                    <param name="maxLength">1024</param>
                                </data>
                            </element>
                        </element>
                    </oneOrMore>
                </element>
            </zeroOrMore>
        </element>
    </start>
    <define name="plugFuncName">
        <data type="string">
            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
        </data>
    </define>
</grammar>
```

### Файл *log.xml* ###

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="log">
            <attribute name="plugin"> <!-- короткое имя плагина -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="event"> <!-- событие -->
                    <attribute name="keyword"> <!-- ключевое слово события -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                        </data>
                    </attribute>
                    <oneOrMore>
                        <element name="desc"> <!-- описание события -->
                            <attribute name="code"> <!-- код языка описания события -->
                                <data type="string">
                                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                </data>
                            </attribute>
                            <data type="string">
                                <param name="maxLength">128</param>
                            </data>
                        </element>
                    </oneOrMore>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

### Файл *texts.xml* ###

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="texts">
            <attribute name="plugin"> <!-- короткое имя плагина -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <choice>
                    <element name="section"> <!-- текстовый раздел -->
                        <attribute name="name"> <!-- имя раздела -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- старое имя раздела -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- тип раздела, статичный -->
                            <value>1</value>
                        </attribute>
                        <oneOrMore>
                            <element name="text"> <!-- текст -->
                                <attribute name="name"> <!-- имя текста -->
                                    <data type="string">
                                        <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                    </data>
                                </attribute>
                                <oneOrMore>
                                    <element name="language">
                                        <attribute name="code"> <!-- код языка текста -->
                                            <data type="string">
                                                <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                            </data>
                                        </attribute>
                                        <element name="title"> <!-- заголовок текста -->
                                            <data type="string">
                                                <param name="minLength">1</param>
                                                <param name="maxLength">128</param>
                                            </data>
                                        </element>
                                        <element name="document"> <!-- сам текст -->
                                            <data type="string">
                                                <param name="minLength">0</param>
                                                <param name="maxLength">65535</param>
                                            </data>
                                        </element>
                                    </element>
                                </oneOrMore>
                            </element>
                        </oneOrMore>
                    </element>
                    <element name="section"> <!-- текстовый раздел -->
                        <attribute name="name"> <!-- имя раздела -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- старое имя раздела -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- тип раздела, нестатичный -->
                            <value>0</value>
                        </attribute>
                    </element>
                </choice>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

### Файл *titles.xml* ###

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="titles">
            <attribute name="plugin"> <!-- короткое имя плагина -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <choice>
                    <element name="section"> <!-- заголовочный раздел -->
                        <attribute name="name"> <!-- имя раздела -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- старое имя раздела -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- тип раздела, статичный -->
                            <value>1</value>
                        </attribute>
                        <oneOrMore>
                            <element name="title"> <!-- заголовок -->
                                <attribute name="name"> <!-- имя заголовка -->
                                    <data type="string">
                                        <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                    </data>
                                </attribute>
                                <oneOrMore>
                                    <element name="language"> <!-- сам заголовок -->
                                        <attribute name="code"> <!-- код языка заголовка -->
                                            <data type="string">
                                                <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                            </data>
                                        </attribute>
                                        <data type="string">
                                            <param name="minLength">1</param>
                                            <param name="maxLength">128</param>
                                        </data>
                                    </element>
                                </oneOrMore>
                            </element>
                        </oneOrMore>
                    </element>
                    <element name="section"> <!-- заголовочный раздел -->
                        <attribute name="name"> <!-- имя раздела -->
                            <data type="string">
                                <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                            </data>
                        </attribute>
                        <optional>
                            <attribute name="oldname"> <!-- старое имя раздела -->
                                <data type="string">
                                    <param name="pattern">[a-zA-Z0-9_]{3,40}</param>
                                </data>
                            </attribute>
                        </optional>
                        <attribute name="static"> <!-- тип раздела, нестатичный -->
                            <value>0</value>
                        </attribute>
                    </element>
                </choice>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

### Файл *inst.php* ###

Структура файла должна соответствовать шаблону:

```
#!php

<?php

namespace core;

interface intInstall {

    public function __construct(\mysqli $dblink, $id, $version = '', $reset = FALSE);

    public function errId();

    public function errExp();

    public function preinst();

    public function postinst();
}

class Install implements intInstall {

    private $errid = 0; // error code
    private $errexp = ''; // error description
    private $dbLink; // mysqli object
    private $id; // identifier of the installing plugin
    private $version; // version of the existing plugin
    private $reset; //

    public function __construct(\mysqli $dblink, $id, $version = '', $reset = FALSE) {
        $this->dbLink = $dblink;
        $this->id = $id;
        $this->version = $version;
        $this->reset = $reset;
    }

    private function setError($id, $exp) {
        $this->errid = $id;
        $this->errexp = $exp;
    }

    public function errId() {
        return $this->errid;
    }

    public function errExp() {
        return $this->errexp;
    }

    public function preinst() {
// разместите здесь свой код
        return TRUE;
    }

    public function postinst() {
// разместите здесь свой код
        return TRUE;
    }

}
```

### Файл *rm.php* ###

Структура файла должна соответствовать шаблону:

```
#!php

<?php

namespace core;

interface intRemove {

    public function __construct(\mysqli $dblink, $id, $keepData = TRUE);

    public function errId();

    public function errExp();

    public function prerm();

    public function postrm();
}

class Remove implements intRemove {

    private $errid = 0; // error code
    private $errexp = ''; // error description
    private $dbLink; // mysqli object
    private $id; // identifier of the installed plugin
    private $keepData; //

    public function __construct(\mysqli $dblink, $id, $keepData = TRUE) {
        $this->dbLink = $dblink;
        $this->id = $id;
        $this->keepData = $keepData;
    }

    private function setError($id, $exp) {
        $this->errid = $id;
        $this->errexp = $exp;
    }

    public function errId() {
        return $this->errid;
    }

    public function errExp() {
        return $this->errexp;
    }

    public function prerm() {
// разместите здесь свой код
        return TRUE;
    }

    public function postrm() {
// разместите здесь свой код
        return TRUE;
    }

}
```

**[>>> Содержание <<<](ru.index)**