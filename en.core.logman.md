**[>>> Contents <<<](en.index)**

## Module logman.php ##

### General description ###

Includes class ```LogMan``` that is purposed to log events and to manage records.

### Methods of class ```LogMan``` *(class ```LogMan``` extends ```Policy```)* ###

**```__construct(mysqli $dbLink)```**

**Description**  
Sets a connection to database.

**Parameters**  
*```dbLink```* - object ```mysqli```.

** **

**```bool installEvents(\DOMDocument $events, bool $validate = TRUE)```**

**Description**  
Installs a set of plugin events.

**Parameters**  
*```events```* - object ```DOMDocument```, includes a set of plugin events and them descriptions in different languages.

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="log">
            <attribute name="plugin"> <!-- short plugin name -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="event"> <!-- event -->
                    <attribute name="keyword"> <!-- keyword of event -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                        </data>
                    </attribute>
                    <oneOrMore>
                        <element name="desc"> <!-- description of event -->
                            <attribute name="code"> <!-- language code of description -->
                                <data type="string">
                                    <param name="pattern">[a-z]{2}-[A-Z]{2}</param>
                                </data>
                            </attribute>
                            <data type="string">
                                <param name="maxLength">128</param>
                            </data>
                        </element>
                    </oneOrMore>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

*```validate```* - the structure of *DOM* is checked beforehand if value is ```TRUE```. The structure is not checked if value is ```FALSE```.

**Return values**  
Returns ```TRUE``` in case of successful installation. Otherwise returns ```FALSE```.

** **

**```delLogEvents(string $plugin)```**

**Description**  
Deletes all records and events of the plugin.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool clearLog()```**

**Description**  
Clears event log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.


**```array sumLogAllPlugins(int $rpp = 20)```**

**Description**  
Returns summary data per page about log to display information over all plugins.

**Parameters**  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
Array like ```array('records' => int $totalRecs, 'pages' => int $totalPages)```, where ```totalRecs``` - total number of records in the log, ```totalPages``` - total number of pages, calculated as ```totalRecs/rpp```. In case of failure returns ```FALSE```.

** **

**```DOMDocument getPageAllPlugins(int $pageNumber, int $totalPages, int $rpp = 20, string $code = MECCANO_DEF_LANG, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Description**  
Returns page from event log over all plugins.

**Parameters**  
*```pageNumber```* - page number, integer from 1 and more.  
*```totalPages```* - total number of pages.  
*```rpp```* - desired maximum number of records per page.  
*```code```* - language code, matches pattern ```[a-z]{2}-[A-Z]{2}```.  
*```orderBy```* - sorting parameters of output of event log records. Allowed values of parameters: ```'id'```, ```'user'```, ```'event'``` и ```'time'```. Sequence of parameters is defined as array, for example, ```array('event', 'user')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns page from event log as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            },
            "event": {
                "type": "string"
            },
            "user": {
                "type": "string",
                "pattern": "^([a-zA-Z0-9_]{3,20}|M)$"
            }
        },
        "required": ["id", "time", "event", "user"]
    }
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="log">
            <zeroOrMore>
                <element name="record"> <!-- information about event -->
                    <element name="id"> <!-- record identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- date and time of event recording -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="event"> <!-- description of event -->
                        <data type="string" />
                    </element>
                    <element name="user"> <!-- username from name of whom a record was made; if M - non-authenticated user -->
                        <data type="string">
                            <param name="pattern">([a-zA-Z0-9_]{3,20}|M)</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    array(
        'id' => record_identifier,
        'time' => 'date and time of event recording',
        'event' => 'description of event',
        'user' => 'username from name of whom a record was made; if M - non-authenticated user'
        ),
    array(...),
    ...
)
```

** **

**```DOMDocument getLogAllPlugins(string $code = MECCANO_DEF_LANG, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Description**  
Returns all log records over all plugins.

**Parameters**  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - sorting parameters of output of event log records. Allowed values of parameters: ```'id'```, ```'user'```, ```'event'``` и ```'time'```. Sequence of parameters is defined as array, for example, ```array('event', 'user')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success return log records as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            },
            "event": {
                "type": "string"
            },
            "user": {
                "type": "string",
                "pattern": "^([a-zA-Z0-9_]{3,20}|M)$"
            }
        },
        "required": ["id", "time", "event", "user"]
    }
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="log">
            <zeroOrMore>
                <element name="record"> <!-- information about event -->
                    <element name="id"> <!-- record identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- date and time of event recording -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="event"> <!-- description of event -->
                        <data type="string" />
                    </element>
                    <element name="user"> <!-- username from name of whom a record was made; if M - non-authenticated user -->
                        <data type="string">
                            <param name="pattern">([a-zA-Z0-9_]{3,20}|M)</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    array(
        'id' => record_identifier,
        'time' => 'date and time of event recording',
        'event' => 'description of event',
        'user' => 'username from name of whom a record was made; if M - non-authenticated user'
        ),
    array(...),
    ...
)
```

** **

**```array sumLogByPlugin(string $plugin, int $rpp = 20)```**

**Description**  
Returns summary data per page about log to display information by defined plugin.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```rpp```* - desired maximum number of records per page. Must have value from 1 or more. If value is less than 1, it is set equal to 1.

**Return values**  
Array like ```array('records' => int $totalRecs, 'pages' => int $totalPages)```, where ```totalRecs``` - total number of records in the log, ```totalPages``` - total number of pages, calculated as ```totalRecs/rpp```. In case of failure returns ```FALSE```.

**  **

**```DOMDocument getPageByPlugin(string $plugin, int $pageNumber, int $totalPages, int $rpp = 20, string $code = MECCANO_DEF_LANG, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Description**  
Returns page from event log by defined plugin.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```pageNumber```* - page number, integer from 1 and more.  
*```totalPages```* - total number of pages.  
*```rpp```* - desired maximum number of records per page.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - sorting parameters of output of event log records. Allowed values of parameters: ```'id'```, ```'user'```, ```'event'``` и ```'time'```. Sequence of parameters is defined as array, for example, ```array('event', 'user')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**  
In case of success returns page from event log as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugin": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "records": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    },
                    "event": {
                        "type": "string"
                    },
                    "user": {
                        "type": "string",
                        "pattern": "^([a-zA-Z0-9_]{3,20}|M)$"
                    }
                },
                "required": ["id", "time", "event", "user"]
            }
        }
    },
    "required": ["plugin", "records"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="log">
            <attribute name="plugin"> <!-- plugin name -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="record"> <!-- information about event -->
                    <element name="id"> <!-- record identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- date and time of event recording -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="event"> <!-- description of event -->
                        <data type="string" />
                    </element>
                    <element name="user"> <!-- username from name of whom a record was made; if M - non-authenticated user -->
                        <data type="string">
                            <param name="pattern">([a-zA-Z0-9_]{3,20}|M)</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'plugin' => 'plugin name',
    'records' => array(
        array(
            'id' => record_identifier,
            'time' => 'date and time of event recording',
            'event' => 'description of event',
            'user' => 'username from name of whom a record was made; if M - non-authenticated user'
        ),
        array(...),
        ...
    )
)
```

** **

**```DOMDocument getLogByPlugin(string $plugin, string $code = MECCANO_DEF_LANG, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Description**  
Returns all log records by defined plugins.

**Parameters**  
*```plugin```* - plugin name, matches pattern *[a-zA-Z\d_]{3,30}*.  
*```code```* - language code, matches pattern *[a-z]{2}-[A-Z]{2}*.  
*```orderBy```* - sorting parameters of output of event log records. Allowed values of parameters: ```'id'```, ```'user'```, ```'event'``` и ```'time'```. Sequence of parameters is defined as array, for example, ```array('event', 'user')```. Incorrectly defined sequence of parameters is set equal to ```array('id')```.  
*```ascent```* - sorting direction. Descending sort is applied if value is ```FALSE```. Ascending sort is applied if value is ```TRUE```.

**Return values**
In case of success return log records as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "plugin": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,30}$"
        },
        "records": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "time": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    },
                    "event": {
                        "type": "string"
                    },
                    "user": {
                        "type": "string",
                        "pattern": "^([a-zA-Z0-9_]{3,20}|M)$"
                    }
                },
                "required": ["id", "time", "event", "user"]
            }
        }
    },
    "required": ["plugin", "records"]
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="log">
            <attribute name="plugin"> <!-- plugin name -->
                <data type="string">
                    <param name="pattern">[a-zA-Z0-9_]{3,30}</param>
                </data>
            </attribute>
            <zeroOrMore>
                <element name="record"> <!-- informmation about event -->
                    <element name="id"> <!-- record identifier -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- date and time of event recording -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="event"> <!-- description of event -->
                        <data type="string" />
                    </element>
                    <element name="user"> <!-- username from name of whom a record was made; if M - non-authenticated user -->
                        <data type="string">
                            <param name="pattern">([a-zA-Z0-9_]{3,20}|M)</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array(
    'plugin' => 'plugin name',
    'records' => array(
        array(
            'id' => record_identifier,
            'time' => 'date and time of event recording',
            'event' => 'description of event',
            'user' => 'username from name of whom a record was made; if M - non-authenticated user'
        ),
        array(...),
        ...
    )
)
```

** **

**```void outputFormat(string $output = 'xml')```**

**Description**  
Sets format of output data.

**Parameters**  
*```output```* - can get values *json*, or *xml*.

**[>>> Contents <<<](en.index)**