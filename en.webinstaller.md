**[>>> Contents <<<](en.index)**

# Installation #

Make sure that framework components are placed into the web-accessible directory. Then edit file *conf.php* and set the database parameters:

*```MECCANO_DBSTORAGE_ENGINE```* - database storage engine. Available values are ```"MyISAM"``` и ```"InnoDB"```;  
*```MECCANO_DBANAME```* - name of the database administrator;  
*```MECCANO_DBAPASS```* - password of the database administrator;  
*```MECCANO_DBHOST```* - database host;  
*```MECCANO_DBPORT```* - database port;  
*```MECCANO_DBNAME```* - name of the database;  
*```MECCANO_TPREF```* - prefix of the database tables.

Also you may edit system paths at your opinion. Make sure that web server has read/write access to files and directories.

By editing value of ```MECCANO_DEF_LANG``` you can set default language. Initially available values are ```"en-US"``` (English) and ```"ru-RU"``` (Russian).

*Refer to the [documentation](en.conf) to get more info.*

Save changes.	

Now open web browser and go to address ```http://host_name/install/``` to run web installer.

![webinstaller01.png](https://bitbucket.org/azexmail/phpmeccano/downloads/en.webinstaller01v0_2_0.png)

All values in the right block will be marked by green color if parameters in file *conf.php* are set correctly. If any value is marked by pink color, it means that this parameter is set incorrectly. In case of system paths it can indicate to absence of read/write rights.

Fill all fields of the form in the left block and push "*Run installation*".

![webinstaller02.png](https://bitbucket.org/azexmail/phpmeccano/downloads/en.webinstaller02v0_2_0.png)

Upon completion of all steps there have to appear notification about successful installation.

![webinstaller03.png](https://bitbucket.org/azexmail/phpmeccano/downloads/en.webinstaller03v0_2_0.png)

Push the dustbin icon to remove installer.

![webinstaller04.png](https://bitbucket.org/azexmail/phpmeccano/downloads/en.webinstaller04v0_2_0.png)

Installation is completed.

**[>>> Contents <<<](en.index)**