**[>>> Содержание <<<](ru.index)**

## Модуль userman.php ##

### Общее описание ###

Содержит класс ```UserMan```, предназначенный для управления пользователями и группами пользователей.

### Методы класса ```UserMan``` *(class ```UserMan``` extends ```LogMan```)* ###

**```__construct(mysqli $dbLink)```**

**Описание**  
Устанавливает связь с базой данных.

**Принимаемые значения**  
*```dbLink```* - объект ```mysqli```.

** **

**```int createGroup(string $groupName, string $description, bool $log = TRUE)```**

**Описание**  
Создает новую группу пользователей.

**Принимаемые значения**  
*```groupName```* - имя группы.  
*```description```* - описание группы.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
Идентификатор созданной группы. В случае возникновения ошибки возвращает ```FALSE```.

** **

**```bool groupStatus(int $groupId, bool $active, bool $log = TRUE)```**

**Описание**  
Задает статус группы.

**Принимаемые значения**  
*```groupId```* - идентификатор группы.  
*```active```* - флаг статуса группы. Для активации группы ```TRUE```, для диактивации ```FALSE```.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool groupExists(string $groupName)```**

**Описание**  
Проверяет существует ли группа с заданным именем.

**Принимаемые значения**  
*```groupName```* - имя группы.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool moveGroupTo(int $groupId, int $destId, bool $log = TRUE)```**

**Описание**  
Переносит всех пользователей из одной группы в другую группу.

**Принимаемые значения**  
*```groupId```* - идентификатор исходной группы. Должен принимать значение больше 1.  
*```destId```* - идентификатор группы назначения.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает колличество перемещенных пользователей, иначе ```FALSE```.

** **

**```DOMDocument aboutGroup(int $groupId)```**

**Описание**  
Возвращает информацию о группе.

**Принимаемые значения**  
*```groupId```* - идентификатор группы.

**Возвращаемые значения**  
В случае успеха возвращает описание группы в виде объекта ```DOMDocument```, либо строки в формате JSON, иначе возвращает ```FALSE```.  
Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "id": {
            "type": "integer",
            "minimum": 1
        },
        "name": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "description": {
            "type": "string",
            "maxLength": 256
        },
        "time": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "active": {
            "type": "integer",
            "minimum": 0,
            "maximum": 1
        },
        "usum": {
            "type": "integer",
            "minimum": 0
        }
    },
    "required": ["id", "name", "description", "time", "active", "usum"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="group">
            <element name="id"> <!-- идентификатор группы -->
                <data type="positiveInteger" />
            </element>
            <element name="name"> <!-- имя группы -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">50</param>
                </data>
            </element>
            <element name="description"> <!-- описание группы -->
                <data type="string">
                    <param name="maxLength">256</param>
                </data>
            </element>
            <element name="time"> <!-- время создания группы -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="active"> <!-- статус группы. 1 - доступна, 0 - не доступна -->
                <choice>
                    <value>0</value>
                    <value>1</value>
                </choice>
            </element>
            <element name="usum"> <!-- общее количество пользователей в группе -->
                <data type="nonNegativeInteger" />
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'id' => идентификатор_группы,
    'name' => имя_группы,
    'description' => описание_группы,
    'time' => время_создания_группы,
    'active' => статус_группы,
    'usum' => количество_пользователей_в_группе
)
```

** **

**```bool setGroupName(int $groupId, string $groupName, bool $log = TRUE)```**

**Описание**  
Задает новое имя группы.

**Принимаемые значения**  
*```groupId```* - идентификатор группы.  
*```groupName```* - новое имя группы.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool setGroupDesc(int $groupId, string $description, bool $log = TRUE)```**

**Описание**  
Задает новое описание группы.

**Принимаемые значения**  
*```groupId```* - идентификатор группы.  
*```description```* - новое описание группы.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool delGroup(int $groupId, bool $log = TRUE)```**

**Описание**  
Удаляет группу пользователей вместе со всеми политиками доступа к компонентам плагинов.

**Принимаемые значения**  
*```groupId```* - идентификатор группы. Должен принимать значение больше 1.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```array sumGroups(int $rpp = 20)```**

**Описание**  
Возвращает обобщенные данные о списке групп пользователей для вывода данных постранично.

**Принимаемые значения**  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
Массив вида ```array('records' => int $totalGroups, 'pages' => int $totalPages)```, где ```totalGroups``` - общее количество групп ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalGroups/gpp```. В случае неудачи возвращает ```FALSE```.

** **

**```DOMDocument getGroups(int $pageNumber, int $totalGroups, int $rpp = 20, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает страницу со списком групп пользователей.

**Принимаемые значения**  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalGroups```* - общее количество групп.  
*```rpp```* - желаемое максимальное количество записей на странице.  
*```orderBy```* - параметры сортировки вывода записей. Допустимые значения параметров: ```'id'```, ```'name'``` , ```'time'``` и ```'active'```. Последовательность параметров задаётся как массив, например, ```array('name', 'time')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает список групп в виде объекта ```DOMDocument```, либо строки в формате JSON, иначе возвращает ```FALSE```.  
Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "name": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            },
            "active": {
                "type": "integer",
                "minimum": 0,
                "maximum": 1
            }
        },
        "required": ["id", "name", "time", "active"]
    }
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="groups">
            <oneOrMore>
                <element name="group"> <!-- информация о группе -->
                    <element name="id"> <!-- идентификатор группы -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="name"> <!-- имя группы -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания группы -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="active"> <!-- статус группы. 1 - доступна, 0 - не доступна -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    array(
        'id' => идентификатор_группы,
        'name' => имя_группы,
        'time' => время_создания_группы,
        'active' => статус_группы
    ),
    array(...),
    ...
)
```

** **

**```DOMDocument getAllGroups(array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает список со всеми группами пользователей.

**Принимаемые значения**  
*```orderBy```* - параметры сортировки вывода записей. Допустимые значения параметров: ```'id'```, ```'name'```, ```'time'``` и ```'active'```. Последовательность параметров задаётся как массив, например, ```array('name', 'time')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает список со всеми группами в виде объекта ```DOMDocument```, либо строки в формате JSON, иначе возвращает ```FALSE```.  
Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "name": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            },
            "active": {
                "type": "integer",
                "minimum": 0,
                "maximum": 1
            }
        },
        "required": ["id", "name", "time", "active"]
    }
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="groups">
            <oneOrMore>
                <element name="group"> <!-- информация о группе -->
                    <element name="id"> <!-- идентификатор группы -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="name"> <!-- имя группы -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="time"> <!-- время создания группы -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="active"> <!-- статус группы. 1 - доступна, 0 - не доступна -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    array(
        'id' => идентификатор_группы,
        'name' => имя_группы,
        'time' => время_создания_группы,
        'active' => статус_группы
    ),
    array(...),
    ...
)
```

** **

**```int createUser(string $username, string $password, string $email, int $groupId, bool $active = TRUE, bool $log = TRUE)```**

**Описание**  
Создает нового пользователя.

**Принимаемые значения**  
*```username```* - желаемое имя пользователя.  
*```password```* - пароль.  
*```email```* - адрес электронной почты.  
*```groupId```* - идентификатор группы, к которой будет относится новый пользователь.  
*```active```* - флаг статуса пользователя. Для активации пользователя ```TRUE```, для диактивации ```FALSE```.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
Идентификатор созданного пользователя. В случае возникновения ошибки возвращает ```FALSE```.

** **

**```int userExists(string $username)```**

**Описание**  
Проверяет существует ли пользователь с заданным именем.

**Принимаемые значения**  
*```username```* - проверяемое имя пользователей.

**Возвращаемые значения**  
При положытельном результате возвращает идентификатор пользователя. Иначе возвращает ```FALSE```.

** **

**```int mailExists(string $email)```**

**Описание**  
Проверяет существует ли пользователь с заданным адресом электронной почты.

**Принимаемые значения**  
*```email```* - проверяемый адрес электронной почты.

**Возвращаемые значения**  
При положытельном результате возвращает идентификатор пользователя. Иначе возвращает ```FALSE```.

** **

**```bool userStatus(int $userId, bool $active, bool $log = TRUE)```**  

**Описание**  
Задает статус пользователя.

**Принимаемые значения**  
*```groupId```* - идентификатор пользователя.  
*```active```* - флаг статуса пользователя. Для активации пользователя ```TRUE```, для диактивации ```FALSE```.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool moveUserTo(int $userId, int $destId, bool $log = TRUE)```**

**Описание**  
Переносит пользователя из одной группы в другую группу.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя. Не может принимать значение 1, так как это значение соответствует системному пользователю.  
*```destId```* - идентификатор группы назначения.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool delUser(int $userId, bool $log = TRUE)```**

**Описание**  
Удаляет заданного пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя. Не может принимать значение 1, так как это значение соответствует системному пользователю.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```DOMDocument aboutUser(int $userId)```**

**Описание**  
Возвращает информацию о пользователе.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя.

**Возвращаемые значения**  
В случае успеха возвращает информацию о пользователе в виде объекта ```DOMDocument```, либо строки в формате JSON, иначе возвращает ```FALSE```.  
Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "id": {
            "type": "integer",
            "minimum": 1
        },
        "username": {
            "type": "string",
            "pattern": "^[a-zA-Z0-9_]{3,20}$"
        },
        "fillname": {
            "type": "string",
            "maxLength": 255
        },
        "email": {
            "type": "string",
            "pattern": "^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?(?:\\.[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?)*$"
        },
        "time": {
            "type": "string",
            "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
        },
        "active": {
            "type": "integer",
            "minimum": 0,
            "maximum": 1
        },
        "gid": {
            "type": "integer",
            "minimum": 1
        },
        "group": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        }
    },
    "required": ["id", "username", "fullname", "email", "time", "active", "gid", "group"]
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="user">
            <element name="id"> <!-- идентификатор пользователя -->
                <data type="positiveInteger" />
            </element>
            <element name="username"> <!-- имя пользователя -->
                <data type="string">
                    <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                </data>
            </element>
            <element name="fullname"> <!-- полное имя пользователя -->
                <data type="string">
                    <param name="maxLength">255</param>
                </data>
            </element>
            <element name="email"> <!-- адрес электронной почты пользователя -->
                <data type="string">
                    <param name="pattern">[^@]+@[^\.]+\..+</param>
                </data>
            </element>
            <element name="time"> <!-- время создания учётной записи пользователя -->
                <data type="string">
                    <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                </data>
            </element>
            <element name="active"> <!-- статус пользователя. 1 - доступен, 0 - не доступен -->
                <choice>
                    <value>0</value>
                    <value>1</value>
                </choice>
            </element>
            <element name="gid"> <!-- идентификатор группы, к которой принадлежит пользователь -->
                <data type="positiveInteger" />
            </element>
            <element name="group"> <!-- имя группы, к которой принадлежит пользователь -->
                <data type="string">
                    <param name="minLength">1</param>
                    <param name="maxLength">50</param>
                </data>
            </element>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'id' => идентификатор_пользователя,
    'username' => имя_пользователя,
    'fullname' => полное_имя_пользователя,
    'email' => адрес_электронной_почты,
    'time' => время_создания_пользователя,
    'active' => статус_пользователя,
    'gid' => идентификатор_группы_пользователя,
    'group' => имя_группы_пользователя
)
```

** **

**```DOMDocument userPasswords(int $userId)```**

**Описание**  
Возвращает список с информацией о паролях пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя.

**Возвращаемые значения**  
В случае успеха возвращает список с информацией о паролях пользователя в виде объекта ```DOMDocument```, либо строки в формате JSON, иначе возвращает ```FALSE```.  
Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "passwords": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "description": {
                        "type": "string",
                        "maxLength": 30
                    },
                    "limited": {
                        "type": "integer",
                        "minimum": 0,
                        "maximum": 1
                    }
                },
                "required": ["id", "description", "limited"]
            }
        }
    }
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="security">
            <attribute name="uid"> <!-- идентификатор пользователя -->
                <data type="positiveInteger" />
            </attribute>
            <oneOrMore>
                <element name="password"> <!-- инфомация о пароле -->
                    <element name="id"> <!-- идентификатор пароля -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="description"> <!-- описание пароля -->
                        <data type="string">
                            <param name="maxLength">30</param>
                        </data>
                    </element>
                    <element name="limited"> <!-- тип пароля. 1 - с ограниченными правами, 0 - основной пароль -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    'uid' => идентификатор_пользователя,
    'passwords' => array(
        array(
            'id' => идентификатор_пароля,
            'description' => описание_пароля,
            'limited' => тип_пароля
        ),
        array(...),
        ...
    )
)
```

** **

**```string createPassword(int $userId, string $description, int $length = 8, bool $underline = TRUE, bool $minus = FALSE, bool $special = FALSE, bool $log = TRUE)```**

**Описание**  
Автоматически создает вторичные пароли пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя.  
*```description```* - краткое описание пароля.  
*```length```* - желаемое количество символов в пароле. Минимальное значение - 8, максимальное - 50.  
*```underline```* - флаг использования в пароле символа нижней черты. Если ```TRUE``` - использовать, если ```FALSE``` - не использовать.  
*```minus```* - флаг использования в пароле знака "минус". Если ```TRUE``` - использовать, если ```FALSE``` - не использовать.  
*```special```* - флаг использования в пароле специальных символов. Если ```TRUE``` - использовать, если ```FALSE``` - не использовать.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
Строка с созданным паролем. При возникновении ошибки возвращает ```FALSE```.

** **

**```int addPassword(int $userId, string $password, string $description='', bool $log = TRUE)```**

**Описание**  
Создает вторичные пароли пользователя (пароли с ограниченными правами).

**Принимаемые значения**  
*```userId```* - идентификатор пользователя.  
*```password```* - добавляемый пароль.  
*```description```* - краткое описание пароля.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
Идентификатор созданного пароля. В случае возникновения ошибки возвращает ```FALSE```.

** **

**```bool delPassword(string $passwId, int $userId, bool $log = TRUE)```**

**Описание**  
Удаляет вторичный пароль пользователя.

**Принимаемые значения**  
*```passwId```* - идентификатор пароля.  
*```userId```* - идентификатор пользователя.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool setPassword(string $passwId, int $userId, string $password, bool $log = TRUE)```**

**Описание**  
Задает новое значение пароля пользователя.

**Принимаемые значения**  
*```passwId```* - идентификатор пароля.  
*```userId```* - идентификатор пользователя.  
*```password```* - новый пароль.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool setUserName(int $userId, string $username, bool $log = TRUE)```**

**Описание**  
Задает новое значение имени пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя.  
*```username```* - новое имя пользователя.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool setUserMail(int $userId, string $email, bool $log = TRUE)```**

**Описание**  
Задает новое значение адреса электронной почты пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя.  
*```email```* - новый адрес электронной почты.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool setFullName(int $userId, string $name, bool $log = TRUE)```**

**Описание**  
Задает новое значение полного имени пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя.  
*```name```* - новое полное имя пользователя.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool changePassword(string $passwId, int $userId, int $oldPassw, int $newPassw, bool $log = TRUE)```**

**Описание**  
Меняет старый пароль пользователя на новый.

**Принимаемые значения**  
*```passwId```* - идентификатор пароля.  
*```userId```* - идентификатор пользователя.  
*```oldPassw```* - старый пароль.  
*```newPassw```* - новый пароль.  
*```log```* - флаг создания записи в журнале событий.

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```array sumUsers(int $rpp = 20)```**

**Описание**  
Возвращает обобщенные данные о списке пользователей для вывода данных постранично.

**Принимаемые значения**  
*```rpp```* - желаемое максимальное количество записей на странице. Число от 1 и выше. Если значение меньше 1, то оно приравнивается к 1.

**Возвращаемые значения**  
Массив вида ```array('records' => $totalUsers, 'pages' => $totalPages)```, где ```totalUsers``` - общее количество пользователей, ```totalPages``` - общее количество страниц, рассчитанное из соотношения ```totalUsers/rpp```. В случае неудачи возвращает ```FALSE```.

** **

**```DOMDocument getUsers(int $pageNumber, int $totalUsers, int $rpp = 20, array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает страницу со списком пользователей.

**Принимаемые значения**  
*```pageNumber```* - номер страницы, число от 1 и выше.  
*```totalUsers```* - общее количество пользователей.  
*```rpp```* - желаемое максимальное количество записей на странице.  
*```orderBy```* - параметры сортировки вывода записей. Допустимые значения параметров: ```'id'```, ```'username'```, ```'time'```, ```'fullname'```, ```'email'```, ```'group'```, ```'gid'``` и ```'active'```. Последовательность параметров задаётся как массив, например, ```array('group', 'time')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает страницу со списком пользователей в виде объекта ```DOMDocument```, либо строки в формате JSON, иначе возвращает ```FALSE```.  
Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "item": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "username": {
                "type": "string",
                "pattern": "^[a-zA-Z0-9_]{3,20}$"
            },
            "fullname": {
                "type": "string",
                "maxLength": 100
            },
            "email": {
                "type": "string",
                "pattern": "^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?(?:\\.[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?)*$"
            },
            "group": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "gid": {
                "type": "integer",
                "minimum": 1
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            },
            "active": {
                "type": "integer",
                "minimum": 0,
                "maximum": 1
            }
        },
        "required": ["id", "username", "fullname", "email", "group", "gid", "time", "active"]
    }
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="users">
            <oneOrMore>
                <element name="user"> <!-- информация о пользователе -->
                    <element name="id"> <!-- идентификатор пользователя -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="pattern">[a-zA-Z\d_]{3,20}</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- полное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="email"> <!-- адрес электронной почты пользователя -->
                        <data type="string">
                            <param name="pattern">[^@]+@[^\.]+\..+</param>
                        </data>
                    </element>
                    <element name="group"> <!-- имя группы, к которой принадлежит пользователь -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="gid"> <!-- идентификатор группы, к которой принадлежит пользователь -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- время создания учётной записи пользователя -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="active"> <!-- статус пользователя. 1 - доступен, 0 - не доступен -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    array(
        id' => идентификатор_пользователя,
        'username' => имя_пользователя,
        'fullname' => полное_имя_пользователя,
        'email' => адрес_электронной_почты,
        'group' => имя_группы_пользователя,
        'gid' => идентификатор_группы_пользователя,
        'time' => время_создания_пользователя,
        'active' => статус_пользователя
    ),
    array(...),
    ...
)
```

** **

**```DOMDocument getAllUsers(array $orderBy = array('id'), bool $ascent = FALSE)```**

**Описание**  
Возвращает список со всеми пользователями.

**Принимаемые значения**  
*```orderBy```* - параметры сортировки вывода записей. Допустимые значения параметров: ```'id'```, ```'username'```, ```'time'```, ```'fullname'```, ```'email'```, ```'group'```, ```'gid'``` и ```'active'```. Последовательность параметров задаётся как массив, например, ```array('group', 'time')```. Некорректно заданная последовательность параметров приравнивается к ```array('id')```.  
*```ascent```* - направление сортировки. При значении ```FALSE```, применяется сортировка по убывающей. При значении ```TRUE``` применяется сортировка по возрастающей.

**Возвращаемые значения**  
В случае успеха возвращает список со всеми пользователями в виде объекта ```DOMDocument```, либо строки в формате JSON, иначе возвращает ```FALSE```.  
Структура JSON описывается следующей схемой JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "array",
    "item": {
        "type": "object",
        "properties": {
            "id": {
                "type": "integer",
                "minimum": 1
            },
            "username": {
                "type": "string",
                "pattern": "^[a-zA-Z0-9_]{3,20}$"
            },
            "fullname": {
                "type": "string",
                "maxLength": 100
            },
            "email": {
                "type": "string",
                "pattern": "^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?(?:\\.[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?)*$"
            },
            "group": {
                "type": "string",
                "minLength": 1,
                "maxLength": 50
            },
            "gid": {
                "type": "integer",
                "minimum": 1
            },
            "time": {
                "type": "string",
                "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
            },
            "active": {
                "type": "integer",
                "minimum": 0,
                "maximum": 1
            }
        },
        "required": ["id", "username", "fullname", "email", "group", "gid", "time", "active"]
    }
}
```

Структура XML описывается следующей схемой RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="users">
            <oneOrMore>
                <element name="user"> <!-- информация о пользователе -->
                    <element name="id"> <!-- идентификатор пользователя -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="username"> <!-- имя пользователя -->
                        <data type="string">
                            <param name="minLength">3</param>
                            <param name="maxLength">20</param>
                        </data>
                    </element>
                    <element name="fullname"> <!-- полное имя пользователя -->
                        <data type="string">
                            <param name="maxLength">100</param>
                        </data>
                    </element>
                    <element name="email"> <!-- адрес электронной почты пользователя -->
                        <data type="string">
                            <param name="pattern">[^@]+@[^\.]+\..+</param>
                        </data>
                    </element>
                    <element name="group"> <!-- имя группы, к которой принадлежит пользователь -->
                        <data type="string">
                            <param name="minLength">1</param>
                            <param name="maxLength">50</param>
                        </data>
                    </element>
                    <element name="gid"> <!-- идентификатор группы, к которой принадлежит пользователь -->
                        <data type="positiveInteger" />
                    </element>
                    <element name="time"> <!-- время создания учётной записи пользователя -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="active"> <!-- статус пользователя. 1 - доступен, 0 - не доступен -->
                        <choice>
                            <value>0</value>
                            <value>1</value>
                        </choice>
                    </element>
                </element>
            </oneOrMore>
        </element>
    </start>
</grammar>
```

Структура массива имеет вид подобный:

```
array(
    array(
        id' => идентификатор_пользователя,
        'username' => имя_пользователя,
        'fullname' => полное_имя_пользователя,
        'email' => адрес_электронной_почты,
        'group' => имя_группы_пользователя,
        'gid' => идентификатор_группы_пользователя,
        'time' => время_создания_пользователя,
        'active' => статус_пользователя
    ),
    array(...),
    ...
)
```

** **

**```bool setUserLang(int $userId, string $code = MECCANO_DEF_LANG)```**

**Описание**  
Задает предпочитаемый язык для пользователя.

**Принимаемые значения**  
*```userId```* - идентификатор пользователя.  
*```code```* - код задаваемого языка, соответствующий шаблону *[a-z]{2}-[A-Z]{2}*.  

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool enable2FA(string $passwId, int $userId)```**

**Описание**  
Включает двухфакторную аутентификацию для указанного пароля пользователя.

**Принимаемые значения**  
*```passwId```* - идентификатор пароля.  
*```userId```* - идентификатор пользователя.  

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```bool disable2FA(string $passwId, int $userId)```**

**Описание**  
Выключает двухфакторную аутентификацию для указанного пароля пользователя.

**Принимаемые значения**  
*```passwId```* - идентификатор пароля.  
*```userId```* - идентификатор пользователя.  

**Возвращаемые значения**  
В случае успеха возвращает ```TRUE```, иначе ```FALSE```.

** **

**```void outputFormat(string $output = 'xml')```**

**Описание**  
Устанавливает формат выводимых данных.

**Принимаемые значения**  
*```output```* - может принимать значения *json*, или *xml*.

**[>>> Содержание <<<](ru.index)**