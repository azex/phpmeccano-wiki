**[>>> Contents <<<](en.index)**

## Module auth.php ##

### General description ###

Includes class ```Auth``` that is purposed to manage user authentication.


### Methods of class ```Auth``` *(class ```Auth``` extends ```LogMan```)* ###


**```__construct(mysqli $dbLink)```**

**Description**  
Sets a connection to database.

**Parameters**  
*```dbLink```* - object ```mysqli```.

** **

**```bool userLogin(string $username, string $password, bool $useCookie = TRUE, string $cookieTime = 'month', bool $log = TRUE, bool $blockBrute = FALSE, $cleanSessions = TRUE)```**

**Description**  
It can executes process of the ordinary authentication or the first step of the two-factor authentication of the user. The type of the used authentication depends on the settings of the specific used password.

**Parameters**  
*```username```* - username, a string of 3-20 characters length. Allowed letters of English ABC upper and lower case, numbers and underline symbol. Or e-mail address of the user.  
*```password```* - password, a string of 8-50 characters length. Allowed letters of English ABC upper and lower case, numbers and special characters.  
*```log```* - a flag to record event of authentication in log.  
*```useCookie```* - a flag to create cookie for the next time authentication.  
*```cookieTime```* - a parameter that sets storage time of cookie. Allowed values ```'hour'```, ```'day'```, ```'week'```, ```'two-weeks'```, ```'mounth'```, ```'half-year'``` и ```'year'```. If parameter is set incorrectly, then cookie won't be stored.  
*```blockBrute```* - a flag to temporarily block authentication of user by password after some attempts of input of incorrect passwords to prevent password brute force. ```TRUE``` - protection is used, ```FALSE``` - protection is not used.  
*```cleanSessions```* - a flag to clean all expired sessions of the user. ```TRUE``` - all expired sessions of the user are deleted, ```FALSE``` - do nothing, expired sessions are kept in the database.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

**Created session variables**  
In case of success execution of the ordinary authentication the following session variables are created:

*```'core_auth_last_time'```* - a time of last authentication pass;  
*```'core_auth_last_ip'```* - IP of last authentication;  
*```'core_auth_uname'```* - username;  
*```'core_auth_userid'```* - user identifier in the database;  
*```'core_auth_limited'```* - a flag of the authentication's type;  
*```'core_auth_usi'```* - uniqie session identifier in the database;  
*```'core_auth_password'```* - password identifier in the database;  
*```'core_auth_ip'```* - user IP in the moment of authentication pass;  
*```'core_auth_uagent'```* - user-agent of browser in the moment of authentication pass;

In case of success execution of the two-factor authentication the following session variable is created:
*```'core_auth_2fa_swap'```* - a buffer with the session variables.

** **

**```bool login2FA(string $code)```**

**Description**  
It executes process of two-factor user authentication.

**Parameters**  
*```code```* - 8-number code of the second step of the two-factor user authentication.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

**Created session variables**  
In case of success the following session variables are created:

*```'core_auth_last_time'```* - a time of last authentication pass;  
*```'core_auth_last_ip'```* - IP of last authentication;  
*```'core_auth_uname'```* - username;  
*```'core_auth_userid'```* - user identifier in the database;  
*```'core_auth_limited'```* - a flag of the authentication's type;  
*```'core_auth_usi'```* - uniqie session identifier in the database;  
*```'core_auth_password'```* - password identifier in the database;  
*```'core_auth_ip'```* - user IP in the moment of authentication pass;  
*```'core_auth_uagent'```* - user-agent of browser in the moment of authentication pass;  

** **

**```bool isSession()```**

**Description**  
Checks the state of user session.

**Return values**  
```TRUE``` if session is valid. ```FALSE``` if session is invalid, stolen, group or user are deactivated.

** **

**```bool userLogout()```**

**Description**  
Closes active user session.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool getSession(bool $log = TRUE)```**

**Description**  
Passes user authentication using earlier saved cookie.

**Parameters**  
*```log```* - a flag to record event of authentication in log.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

**Created session variables**  
In case of success the following session variables are created:

*```'core_auth_last_time'```* - a time of last authentication pass  
*```'core_auth_last_ip'```* - IP of last authentication;  
*```'core_auth_uname'```* - username;  
*```'core_auth_userid'```* - user identifier in the database;  
*```'core_auth_limited'```* - a flag of the authentication's type;  
*```'core_auth_usi'```* - uniqie session identifier in the database;  
*```'core_auth_password'```* - password identifier in the database;  
*```'core_auth_ip'```* - user IP in the moment of authentication pass;  
*```'core_auth_uagent'```* - user-agent of browser in the moment of authentication pass.  

** **

**```DOMDocument userSessions(int $userId)```**

**Description**  
Returns list with data of actual sessions and deletes expired sessions of the user.

**Parameters**  
*```userId```* - user identifier.

**Return values**  
In case of success returns list with data of the user sessions as object ```DOMDocument```, as string JSON, or as array, otherwise returns ```FALSE```. 

JSON structure is described by the following schema JSON Schema:

```
#!json

{
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": {
        "uid": {
            "type": "integer",
            "minimum": 1
        },
        "sessions": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "usi": {
                        "type": "string",
                        "pattern": "^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$"
                    },
                    "ip": {
                        "type": "string",
                        "maxLength": 45
                    },
                    "useragent": {
                        "type": "string",
                        "maxLength": 512
                    },
                    "created": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    },
                    "endtime": {
                        "type": "string",
                        "pattern": "^[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}$"
                    },
                    "info": {
                        "type": "string",
                        "maxLength": 30
                    }
                },
                "required": ["usi", "ip", "useragent", "created", "endtime", "info"]
            }
        }
    }
}
```

XML structure is described by the following schema RELAX NG:

```
#!xml

<?xml version="1.0"?>

<grammar datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes" xmlns="http://relaxng.org/ns/structure/1.0" >
    <start>
        <element name="list">
            <attribute name="uid"> <!-- identifier of the user who created the session -->
                <data type="positiveInteger" />
            </attribute>
            <zeroOrMore>
                <element name="session"> <!-- block of the session data -->
                    <element name="usi"> <!-- unique session identifier -->
                        <data type="string">
                            <param name="pattern">[a-z\d]{8}-[a-z\d]{4}-4[a-z\d]{3}-[a-z\d]{4}-[a-z\d]{12}</param>
                        </data>
                    </element>
                    <element name="ip"> <!-- IP address from which the session was created -->
                        <data type="string">
                            <param name="maxLength">45</param>
                        </data>
                    </element>
                    <element name="useragent"> <!-- user-agent of the browser from which the session was created by the user -->
                        <data type="string">
                            <param name="maxLength">512</param>
                        </data>
                    </element>
                    <element name="created"> <!-- time of the session creation -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="endtime"> <!-- end time of the session -->
                        <data type="string">
                            <param name="pattern">[0-9]{4}(-[0-9]{2}){2} [0-2][0-9](:[0-5][0-9]){2}</param>
                        </data>
                    </element>
                    <element name="info"> <!-- description of the password which was used to created the session -->
                        <data type="string">
                            <param name="maxLength">30</param>
                        </data>
                    </element>
                </element>
            </zeroOrMore>
        </element>
    </start>
</grammar>
```

Array structure has a view like:

```
array("uid" => user_identifier,
    "sessions" => array(
        array("usi" => session_identifier,
            "ip" => IP_address,
            "useragent" => browser_useragent,
            "created" => session_created,
            "endtime" => session_end_time,
            "info" => used_password_description),
        array(...),
        ...
    )
)
```

** **

**```bool destroyAllSessions(int $userId)```**

**Description**  
Deletes all existing sessions of the specified user.

**Return values**  
*```userId```* - user identifier.

**Reurn values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool destroySession(int $userId, string $sesId)```**

**Описание**  
Deletes specified session of the user.

**Принимаемые значения**  
*```userId```* - user identifier.  
*```sesId```* - session identifier (GUID).

**Возвращаемые значения**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

**[>>> Contents <<<](en.index)**