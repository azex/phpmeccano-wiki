**[>>> Contents <<<](en.index)**

## Module files.php ##

### General description ###

Includes class ```Files``` that is purposed to execute operations of copying, moving and removing. Available execution of actions over single files and single symbolic links, and also over directories (then it is generally called as item) that include many files, symbolic links and subdirectories.


### Methods of class ```Files``` ###

**```int errId()```**

**Description**  
Returns code of last occurred errors.

**Return values**  
Integer that matches to constants of view ```ERROR_*```. Value 0 means absence of error.

** **

**```string errExp()```**

**Description**  
Returns description of last occurred errors.

**Return values**  
A string with description of error. Empty string means absence of error.

** **

**```bool copy(string $sourcePath, string $destPath, bool $mergeDirs = FALSE, bool $rewriteFiles = FALSE, bool $skipExistentFiles = FALSE, bool $skipNotReadable = FALSE, bool $skipWriteProtected = FALSE, bool $skipConflicts = FALSE, bool $removeConflictFiles = FALSE)```**

**Description**  
Makes a copy of source item to specified place.

**Parameters**  
*```sourcePath```* - a path to source item.  
*```destPath```* - a path to destination item.  
*```mergeDirs```* - a flag to merge directories. It can get values ```TRUE``` or ```FALSE```.  
*```rewriteFiles```* - a flag to rewrite files while merging. It can get values ```TRUE``` or ```FALSE```.  
*```skipExistentFiles```* - a flag to skip existing files while merging. It can get values ```TRUE``` or ```FALSE```. This argument is ignored if rewriting of files is enabled.  
*```skipNotReadable```* - a flag to skip not readable files and directories while merging. It can get values ```TRUE``` or ```FALSE```.  
*```skipWriteProtected```* - a flag to skip write-protected files and directories while merging. It can get values ```TRUE``` or ```FALSE```.  
*```skipConflicts```* - a flag to skip conflicts while merging. The following things are pertinent to conflicts: to replace an existing file (or symbolic link pointing to file) by a directory; to replace an existing directory by a file (or symbolic link). It can get values ```TRUE``` or ```FALSE```.  
*```removeConflictFiles```* - a flag to remove files and directories while merging. Allows to remove directories which should be replaced by files; and to remove files which should be replaced by directories. It can get values ```TRUE``` or ```FALSE```.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool move(string $sourcePath, string $destPath, bool $mergeDirs = FALSE, bool $replaceFiles = FALSE, bool $skipExistentFiles = FALSE, bool $skipNotReadable = FALSE, bool $skipWriteProtected = FALSE, bool $skipConflicts = FALSE, bool $removeConflictFiles = FALSE)```**

**Description**  
Moves a source item to specified place.

**Parameters**  
*```sourcePath```* - a path to source item.  
*```destPath```** - a path to destination item.  
*```mergeDirs```* - a flag to merge directories. It can get values ```TRUE``` or ```FALSE```.  
*```replaceFiles```* - a flag to replace files while merging. It can get values ```TRUE``` or ```FALSE```.  
*```skipExistentFiles```* - a flag to skip existing files while merging. It can get values ```TRUE``` or ```FALSE```. This argument is ignored if replacing of files is enabled.  
*```skipNotReadable```* - a flag to skip not readable files and directories while merging. It can get values ```TRUE``` or ```FALSE```.  
*```skipWriteProtected```* - a flag to skip write-protected files and directories while merging. It can get values ```TRUE``` or ```FALSE```.  
*```skipConflicts```* - a flag to skip conflicts while merging. The following things are pertinent to conflicts: to replace an existing file (or symbolic link pointing to file) by a directory; to replace an existing directory by a file (or symbolic link); to remove directory that is not empty. It can get values ```TRUE``` or ```FALSE```.  
*```removeConflictFiles```* - a flag to remove files and directories while merging. Allows to remove directories which should be replaced by files; and to remove files which should be replaced by directories. It can get values ```TRUE``` or ```FALSE```.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

** **

**```bool remove(string $sourcePath, bool $skipNotReadable = FALSE, bool $skipWriteProtected = FALSE, bool $skipConflicts = FALSE)```**

**Description**  
Removes specified item.

**Parameters**  
*```sourcePath```* - a path to being removed item.  
*```skipNotReadable```* - a flag to skip not readable directories. It can get values ```TRUE``` or ```FALSE```.  
*```skipWriteProtected```* - a flag to skip write-protected directories. It can get values ```TRUE``` or ```FALSE```.  
*```skipConflicts```* - a flag to skip conflicts. The conflict is an attempt to remove directory that is not empty. It can get values ```TRUE``` or ```FALSE```.

**Return values**  
Returns ```TRUE``` in case of success. Otherwise returns ```FALSE```.

**[>>> Contents <<<](en.index)**